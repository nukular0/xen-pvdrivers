#ifndef _VSPI_IF_H_
#define _VSPI_IF_H_

#define VSPI_MAX_PAGES_PER_TRANSFER		32

typedef enum { 	CMD_SPI_TRANSFER_ONE_MSG 		= 0,
				CMD_SPI_SETUP 					= 1, 
				CMD_SPI_CLEANUP 				= 2
} 
vspi_command_t;


/*
 * subset of struct spi_device
 * only the relevant data that needs to be passed to the backend
 */
struct spi_device_data{
	u32			max_speed_hz;
	int			cs_gpio;	
	u16			mode;
	s16			bus_num;
	u8			chip_select;
	u8			bits_per_word;
	char		modalias[SPI_NAME_SIZE];
};
	
	
/*
 * information about one spi_transfer for the backend
 * used to create a struct spi_transfer as part of creating a 
 * struct spi_message
 * parameters are the same as in struct spi_transfer, except:
 * fe_buf:	pointer to page shared with the backend
 * 			data from transfers tx_buf will be copied there
 * 			the backend will map this page via grefs and when the 
 * 			transfer is complete, we will copy rx data from here to 
 * 			rx_buf (if it exists)
 * be_buf:	used by the backend to store the pointer to the location 
 * 			where it mapped fe_buf to
 * grefs:	reference(s) to page(s) for tx/rx data for this transfer
 * tx_buf:	
 * rx_buf:	used only to determine if data has to be copied to buf
 * 			on creation of the vspi_transfer (tx_buf != NULL) and to 
 * 			copy data from buf to rx_buf (rx_buf != NULL)
 * 			The backend does not have access to the memory pointed to by
 * 			these pointers		
 * spi_transfer:
 * 			pointer to the spi_transfer that the backend created for
 * 			this vspi_transfer. The backend will use this to copy the
 * 			rx data back to buf and to free the memory it allocated 
 * 			for the transfer	
 */
struct vspi_transfer{
	unsigned 			len;
	unsigned			cs_change;
	unsigned			tx_nbits;
	unsigned			rx_nbits;
	const void			*tx_buf;
	void 				*rx_buf;
	u8					bits_per_word;
	u16					delay_usecs;
	u32					speed_hz;
	void 				*fe_buf;
	void 				*be_buf;
	u16 				num_grefs;
	grant_ref_t 		grefs[VSPI_MAX_PAGES_PER_TRANSFER];
	struct spi_transfer *spi_transfer;
};
	
/*
 * information about one spi_message
 * will be used by the backend to create a struct spi_message
 * frame_length is the number of bytes in all tranfers in this spi_message
 * num_transfers is the number of transfers in this spi_message
 * transfers is an array of vspi_transfers used to create the 
 * struct spi_transfers for the struct spi_message
 * transfers will be num_transfers * sizeof(struct vspi_transfer) 
 * bytes in size.
 * spi_message_info will be created in a free page when creating a 
 * request for CMD_SPI_TRANSFER_ONE_MSG, so there should be enough space
 * for that
 */
struct spi_message_info{
	unsigned 	frame_length;
	int		 	num_transfers;
	struct vspi_transfer transfers[1];
};
	
/*
 * Request type for the shared ring.
 * cmd:			command e.g. CMD_SPI_WRITE
 * 
 * spi_dev_data: 
 * 				relevant data of struct spi_device that was passed
 * 				to the master (e.g. in setup()). 
 * 				Relevant for CMD_SPI_SETUP, CMD_SPI_CLEANUP
 * 
 * spi_message_info:
 * 				reference to a page that stores information about 
 * 				the spi_message to be tranfered
 * 				relevant for CMD_SPI_TRANSFER_ONE_MSG
 */
struct vspi_request {
	vspi_command_t 				cmd;
	struct spi_device_data 		spi_dev_data;
	grant_ref_t 				info_gref;
};
typedef struct vspi_request vspi_request_t;

/*
 * Response type for the shared ring.
 */
struct vspi_response {
	int ret;
};
typedef struct vspi_response vspi_response_t;

DEFINE_RING_TYPES(vspi, struct vspi_request, struct vspi_response);

#endif
