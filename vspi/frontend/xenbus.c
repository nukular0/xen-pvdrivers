#include "vspifront.h"
#include "spi_master.h"

struct xbdev_list xbdevs;

/*
 * For those transfers of the message that had a rx_buf we have to copy 
 * the rx data to that buffer.
 * Then we have to free the memory and grant references allocated for this
 * message when creating the request for the message (see spi_master.c
 * vspi_request_from_msg())
 * Then tell the spi subsystem that we are done with the current message
 */
void finish_message(unsigned long dev_id){
	struct spi_master 			*master;
	struct vspifront_dev 		*dev;
	struct spi_message_info 	*info;
	struct vspi_transfer 		*transfer;
	int 						i_trans, i_gref;
	int 						ret;
	
	VSPI_DEBUG("finish_message()\n");
	
	dev = (struct vspifront_dev*)dev_id;
	master = dev->spi_master_data->master;
	
	info = dev->current_message_info;
			
	for(i_trans = 0; i_trans < info->num_transfers; i_trans++){
		transfer = &info->transfers[i_trans];
		
		// Copy memory to the actual rx buffer if it exists
		if(transfer->rx_buf){
			memcpy(transfer->rx_buf, transfer->fe_buf, transfer->len);
		}
		
		// end access to all grefs for this transfer
		for(i_gref = 0; i_gref < transfer->num_grefs; i_gref++){
			ret = gnttab_end_foreign_access_ref(transfer->grefs[i_gref], 0);
			gnttab_free_grant_reference(transfer->grefs[i_gref]);
		}		
		// free memory that was allocated as buffer
		free_pages_exact(transfer->fe_buf, transfer->len);
	}
	
	// free gref and memory for spi_message_info 
	ret = gnttab_end_foreign_access_ref(dev->current_request->info_gref, 0);
	if(!ret){
		VSPI_ERR("gnttab_end_foreign_access_ref() (still in use?\n");
		BUG();
	}
	gnttab_free_grant_reference(dev->current_request->info_gref);
	
	free_page((unsigned long)info);
	
	// free memory for request and set current_request to NULL
	// so we can perform a new transfer
	kfree(dev->current_request);
	dev->current_request = NULL;
	
	master->cur_msg->status = 0;
	
	// finalize message to tell the spi subsystem that 
	// we are ready to receive the next one
	VSPI_DEBUG("finished_message()\n");
	spi_finalize_current_message(master);
}

int vspifront_send_request(struct vspifront_dev* dev, vspi_request_t req){
	RING_IDX i;
	vspi_request_t *_req;
	int notify;
	int _ret;
	
	if(dev->state == XenbusStateConnected){
		i = dev->ring.req_prod_pvt;
		_req = RING_GET_REQUEST(&dev->ring, i);
		memcpy(_req, &req, sizeof(req));
		dev->ring.req_prod_pvt = i + 1;

		wmb();
		RING_PUSH_REQUESTS_AND_CHECK_NOTIFY(&dev->ring, notify);
		if(notify) 
		{
			VSPI_DEBUG("sending request: cmd %d\n", req.cmd);
			notify_remote_via_irq(dev->comm_evtchn_irq);    
			VSPI_DEBUG("notified\n");
		}
		down(&dev->sem);
	}
	else{
		VSPI_DEBUG("error: not connected");
		return -ENOTCONN;
	}
	_ret = dev->last_response.ret;
	VSPI_DEBUG("got response: %d\n", _ret);
	dev->last_response.ret = INVALID_RESPONSE;
	return _ret;
}

/*
 * vspifront_msg_completed_interrupt
 * Triggered by the backend once a message has been transfered
 * Schedules the tasklet to do the actual work of copying memory to 
 * message rx_bufs and freeing grefs and request/message memory
 */
static irqreturn_t vspifront_msg_completed_interrupt(int irq, void *dev_id){
	struct vspifront_dev *dev = (struct vspifront_dev*)dev_id;
	VSPI_DEBUG("vspifront_msg_completed_interrupt()\n");	
	
	if(!dev){
		VSPI_ERR("no dev\n");
		return IRQ_HANDLED;
	}
	
	//tasklet_schedule(&dev->finish_msg_tasklet);
	//~ up(&dev->finish_msg_sem);
	wake_up_process(dev->finish_msg_task);
	VSPI_DEBUG("vspifront_msg_completed_interrupt up'ed sema\n");	
	
	return IRQ_HANDLED;
}

static irqreturn_t vspifront_comm_interrupt(int irq, void *dev_id)
{
	RING_IDX rp, cons;
	vspi_response_t *rsp;
	int nr_consumed, more;
	struct vspifront_dev *dev = (struct vspifront_dev*) dev_id;

moretodo:
	rp = dev->ring.sring->rsp_prod;
    rmb(); /* Ensure we see queued responses up to 'rp'. */
    cons = dev->ring.rsp_cons;
   
	while ((cons != rp))
    {
		rsp = RING_GET_RESPONSE(&dev->ring, cons);
		nr_consumed++;
		dev->last_response = *rsp;
		dev->ring.rsp_cons = ++cons;   
        if (dev->ring.rsp_cons != cons)
            /* We reentered, we must not continue here */
            break;
			
	}
	
	RING_FINAL_CHECK_FOR_RESPONSES(&dev->ring, more);
    if (more) goto moretodo;    
	
	up(&dev->sem);   
	
	return IRQ_HANDLED;
}

static int publish_xenbus(struct vspifront_dev* dev) {
   struct xenbus_transaction xbt;
   int err;
   /* Write the grant reference and event channel to xenstore */
again:
   if((err = xenbus_transaction_start(&xbt))) {
      VSPI_ERR("Unable to start xenbus transaction, error was %d\n", err);
      
      return -1;
   }

   if((err = xenbus_printf(xbt, dev->nodename, "ring-ref", "%u", (unsigned int) dev->ring_ref))) {
      VSPI_ERR("Unable to write %s/ring-ref, error was %d\n", dev->nodename, err);
      goto abort_transaction;
   }

   if((err = xenbus_printf(xbt, dev->nodename, "event-channel", "%u", (unsigned int) dev->comm_evtchn))) {
      VSPI_ERR("Unable to write %s/event-channel, error was %d\n", dev->nodename, err);
      goto abort_transaction;
   }
   
   if((err = xenbus_printf(xbt, dev->nodename, "msg-event-channel", "%u", (unsigned int) dev->msg_completed_evtchn))) {
      VSPI_ERR("Unable to write %s/msg-event-channel, error was %d\n", dev->nodename, err);
      goto abort_transaction;
   }

   if((err = xenbus_transaction_end(xbt, 0))) {
      VSPI_ERR("Unable to complete xenbus transaction, error was %d\n", err);
      if(err == -EAGAIN){
		goto again;
	  }
      return -1;
   }
  
   return 0;
abort_transaction:
   xenbus_transaction_end(xbt, 1);
   return -1;
}


static int vspifront_connect(struct xenbus_device *xbdev)
{
	int err ;
	struct vspi_sring *sring;
	struct vspifront_dev* dev;
   
	dev = dev_get_drvdata(&xbdev->dev);
	
	err = 0;
   
	/* Create shared page/ring */
	dev->ring_page = get_zeroed_page(GFP_KERNEL);
	sring = (struct vspi_sring *)dev->ring_page;
	if(sring == NULL) {
	  VSPI_ERR("Unable to allocate page for shared memory\n");
	  goto error;	
	}

	/* Initialize shared ring in shared page */
	SHARED_RING_INIT(sring);
	FRONT_RING_INIT(&dev->ring, sring, XEN_PAGE_SIZE);
	
	err = xenbus_grant_ring(xbdev, sring, 1, &dev->ring_ref);
	if(err < 0){
		VSPI_ERR("error granting access to ring: %d\n", err);
		goto error_postalloc;
	}
	
	VSPI_DEBUG("grant ref is %lu\n", (unsigned long) dev->ring_ref);

	/* Create event channel for communication with backend */
	err = xenbus_alloc_evtchn(xbdev, &dev->comm_evtchn);
	if(err < 0) {
	  VSPI_ERR("Unable to allocate comm_event channel\n");
	  goto error_postmap;
	}
	VSPI_DEBUG("comm_event channel is %lu\n", (unsigned long) dev->comm_evtchn);
	
	err = bind_evtchn_to_irqhandler(dev->comm_evtchn,
					vspifront_comm_interrupt,
					0, "vspifront", dev);
	if (err < 0){
		VSPI_ERR("Could not bind evtchn to irq handler (error %d)\n", err);
		goto error_postbind;
	}
	dev->comm_evtchn_irq = err;
	VSPI_DEBUG("comm_event comm_evtchn_irq is %lu\n", (unsigned long) dev->comm_evtchn_irq);
	
	/* Create event channel for notification about completed message */
	err = xenbus_alloc_evtchn(xbdev, &dev->msg_completed_evtchn);
	if(err < 0) {
	  VSPI_ERR("Unable to allocate msg_completed_evtchn\n");
	  goto error_postmap_msg;
	}
	VSPI_DEBUG("msg_completed_evtchn channel is %lu\n", (unsigned long) dev->msg_completed_evtchn);
	
	err = bind_evtchn_to_irqhandler(dev->msg_completed_evtchn,
					vspifront_msg_completed_interrupt,
					0, "vspifront", dev);
	if (err < 0){
		VSPI_ERR("Could not bind evtchn to irq handler (error %d)\n", err);
		goto error_postbind_msg;
	}
	dev->msg_completed_evtchn_irq = err;
	VSPI_DEBUG("comm_event msg_completed_evtchn_irq is %lu\n", (unsigned long) dev->msg_completed_evtchn_irq);
		
	
	/* Write the entries to xenstore */
	if(publish_xenbus(dev)) {
	  goto error_postevtchn;
	}
	

	return 0;
	
error_postevtchn:
		unbind_from_irqhandler(dev->msg_completed_evtchn_irq, dev);
error_postbind_msg:
		xenbus_free_evtchn(xbdev, dev->msg_completed_evtchn);
		dev->msg_completed_evtchn = 0;
error_postmap_msg:
		unbind_from_irqhandler(dev->comm_evtchn_irq, dev);
error_postbind:
		xenbus_free_evtchn(xbdev, dev->comm_evtchn);
		dev->comm_evtchn = 0;
error_postmap:
		gnttab_end_foreign_access_ref(dev->ring_ref, 0);
		gnttab_free_grant_reference(dev->ring_ref);
error_postalloc:
		free_page((unsigned long)sring);
error:
   return err;
}

void free_vspifront(struct xenbus_device *xbdev){
	struct vspifront_dev *dev;
	int i;
	
	dev = dev_get_drvdata(&xbdev->dev);
	if(!dev) 
		return;
	
	if(dev->nodename){
		VSPI_DEBUG("freeing nodename\n");
		kfree(dev->nodename);
	}
	
	if(dev->bepath){
		VSPI_DEBUG("freeing bepath\n");		
		kfree(dev->bepath);
	}
	
	for(i = 0; i < dev->spi_master_data->num_chipselect; i++){
		dev->spi_master_data->spi_devices[i] = NULL;
	}
	kfree(dev->spi_master_data->spi_devices);
	
	if(dev->spi_master_data){
		VSPI_DEBUG("freeing spi_master_data\n");
		kfree(dev->spi_master_data);
	}
	
	// free communication event channel
	if(dev->comm_evtchn){
		VSPI_DEBUG("unbind_from_irqhandler (irq %d)\n", dev->comm_evtchn_irq);
		unbind_from_irqhandler(dev->comm_evtchn_irq, dev);
		dev->comm_evtchn = 0;
	}
	
	// free message completed event channel
	if(dev->msg_completed_evtchn){
		VSPI_DEBUG("unbind_from_irqhandler (irq %d)\n", dev->msg_completed_evtchn_irq);
		unbind_from_irqhandler(dev->msg_completed_evtchn_irq, dev);
		dev->msg_completed_evtchn = 0;
	}
	
	// free shared ring and memory
	if(dev->ring_ref){
		VSPI_DEBUG("gnttab_free_grant_reference (ring_ref %d)\n", dev->ring_ref);
		//~ gnttab_free_grant_reference(dev->ring_ref);
		VSPI_DEBUG("gnttab_end_foreign_access (ring_ref %d)\n", dev->ring_ref);
		gnttab_end_foreign_access(dev->ring_ref, 0, dev->ring_page);
		dev->ring_ref = 0;
		dev->ring_page = 0;
	}
	
	// if there is still an open request, finish it and free it's memory
	if(dev->current_request){
		VSPI_DEBUG("stopping current_request\n");
		finish_message((unsigned long)dev);
	}
	
	
	
	// free device memory
	kfree(dev);
	
	vspifront_set_state(xbdev, XenbusStateClosed);	
}

void shutdown_vspifront(struct xenbus_device *xbdev){
	
	struct vspifront_dev *dev;
	int err;
	if(xbdev->state != XenbusStateConnected) 
		return;
		
	dev = dev_get_drvdata(&xbdev->dev);
	
	if(!dev) 
		return;
	
	
	vspifront_set_state(xbdev, XenbusStateClosing);	
	
	VSPI_DEBUG("stopping finish_msg_task thread...\n");
	kthread_stop(dev->finish_msg_task);
	VSPI_DEBUG("upping semaphore (%p)\n", &dev->finish_msg_sem);
	wake_up_process(dev->finish_msg_task);
	//~ up(&dev->finish_msg_sem);
	
	// clean up xenstore
	VSPI_DEBUG("cleaning up xenstore\n");
	err = xenbus_rm(XBT_NIL, dev->nodename, "ring-ref");
	if(err){
		VSPI_ERR("removing ring-ref (%d)\n", err);
	}
	err = xenbus_rm(XBT_NIL, dev->nodename, "event-channel");
	if(err){
		VSPI_ERR("removing event-channel (%d)\n", err);
	}
	
	err = xenbus_rm(XBT_NIL, dev->nodename, "msg-event-channel");
	if(err){
		VSPI_ERR("removing msg-event-channel (%d)\n", err);
	}
	
	vspi_master_exit(dev);
	
	VSPI_DEBUG("freeing vspifront dev\n");
	free_vspifront(xbdev);
	
}

struct vspifront_dev* init_vspifront(struct xenbus_device *xbdev, const char* _nodename)
{
	struct vspifront_dev *dev;
	unsigned int val;
	const char* nodename;
	int err;
	char tmpstr[512];

	VSPI_DEBUG("============= Init VSPI Front ================\n");

	dev = kzalloc(sizeof(*dev), GFP_KERNEL);
	
	if(!dev){
		VSPI_ERR("allocating memory for dev\n");
		return NULL;
	}

	dev->spi_master_data = kzalloc(sizeof(*dev->spi_master_data), GFP_KERNEL);

	if(!dev->spi_master_data){
		VSPI_ERR("allocating memory for dev->spi_master_data\n");
		goto error;
	}

	VSPI_DEBUG("vspifront_dev at %p (%p)\n", dev, &*dev);
	VSPI_DEBUG("spi_master_data at %p\n", &dev->spi_master_data);
	
	dev->spi_master_data->vspidev = dev;

	dev->last_response.ret = INVALID_RESPONSE;
	dev->xbdev = xbdev;

	/* Init semaphore */
	sema_init(&dev->sem, 0);
	sema_init(&dev->finish_msg_sem, 0);
	VSPI_DEBUG("finish_msg_sem at %p\n", &dev->finish_msg_sem);
	
	/* Set node name */
	nodename = _nodename ? _nodename : "device/vspi/0";
	dev->nodename = kstrdup(nodename, GFP_KERNEL);
  

	/* Get backend domid */
	err = xenbus_scanf(XBT_NIL, dev->nodename, "backend-id", "%u", &val);
	if(err != 1) {
	  VSPI_ERR("Unable to read backend-id during vspifront initialization! error = %d\n", err);
	  goto error;
	}
	dev->bedomid = val;
	VSPI_LOG("backend dom-id is %d\n", val);

	/* Get backend xenstore path */
	err = xenbus_scanf(XBT_NIL, dev->nodename, "backend", "%s", tmpstr);
	if(err != 1) {
	  VSPI_ERR("Unable to read backend during vspifront initialization! error = %d\n", err);
	  goto error;
	}
	dev->bepath = kstrdup((const char*)tmpstr, GFP_KERNEL);
	VSPI_LOG("backend path is %s\n", dev->bepath);

	/* Get spi master parameter:  bus_num*/
	err = xenbus_scanf(XBT_NIL, dev->nodename, "busnum", "%u", &val);
	if(err != 1) {
	  VSPI_ERR("Unable to read busnum during vspifront initialization! error = %d\n", err);
	  goto error;
	}
	dev->spi_master_data->bus_num = val;
	VSPI_LOG("spi busnum: %u\n", dev->spi_master_data->bus_num);
	
	/* Get spi master parameter:  max_speed_hz*/
	err = xenbus_scanf(XBT_NIL, dev->nodename, "max_speed_hz", "%u", &val);
	if(err != 1) {
	  VSPI_ERR("Unable to read max_speed_hz during vspifront initialization! error = %d\n", err);
	  goto error;
	}
	dev->spi_master_data->max_speed_hz = val;
	VSPI_LOG("spi max_speed_hz: %u\n", dev->spi_master_data->max_speed_hz);
	
	/* Get spi master parameter:  max_speed_hz*/
	err = xenbus_scanf(XBT_NIL, dev->nodename, "num_cs", "%u", &val);
	if(err != 1) {
	  VSPI_ERR("Unable to read num_cs during vspifront initialization! error = %d\n", err);
	  goto error;
	}
	dev->spi_master_data->num_chipselect = val;
	VSPI_LOG("spi num_cs: %u\n", dev->spi_master_data->num_chipselect);
		
	
	return dev;

error:
   shutdown_vspifront(xbdev);
   return NULL;
}

// The function is called on activation of the device
static int vspifront_probe(struct xenbus_device *xbdev,
              const struct xenbus_device_id *id)
{
	struct vspifront_dev* dev;
	struct xbdev_list *xblist_item;
	int ret = 0;
	
	VSPI_DEBUG("vspifront_probe called: %s\n", xbdev->nodename);
	
	// Create vspi device
	dev = init_vspifront(xbdev, xbdev->nodename);
	if(!dev) 
		goto error;
	
	ret = vspi_master_init(dev);
	if(ret){
		VSPI_ERR("init spi master failed: %d\n", ret);
		goto error_postinit;
	}
		
	xblist_item = (struct xbdev_list *)kzalloc(sizeof(struct xbdev_list), GFP_KERNEL);
	if(!xblist_item){
		VSPI_ERR("Could not allocate memory for xblist_item\n");
		goto error_postinit;
	}
	xblist_item->xbdev = xbdev;
	list_add(&xblist_item->list, &xbdevs.list);
	
	
	VSPI_DEBUG("xbdev->dev at %p\n", &xbdev->dev);
	dev_set_drvdata(&xbdev->dev, dev);
	
	//~ tasklet_init(&dev->finish_msg_tasklet, finish_message,
		     //~ (unsigned long)dev);
	dev->finish_msg_task = kthread_run(finish_message_thread, dev, "vspifront-finish-msg-thread");
	
	vspifront_set_state(xbdev, XenbusStateInitialising);
		
	return 0;
	
error_postinit:
	shutdown_vspifront(xbdev);
error:
	dev_set_drvdata(&xbdev->dev, NULL);
	return ret;
}

void vspifront_set_state(struct xenbus_device *xbdev, 
									enum xenbus_state state)
{
	struct vspifront_dev* dev;
	if(!xbdev) 
		return;
	
	dev = dev_get_drvdata(&xbdev->dev);
	if(!dev) 
		return;
	
	dev->state = state;
	xenbus_switch_state(xbdev, state);						
}


// The function is called on a state change of the backend driver
static void vspiback_changed(struct xenbus_device *xbdev,
			    enum xenbus_state backend_state)
{
	struct vspifront_dev* dev;
	int ret, i;
	dev = dev_get_drvdata(&xbdev->dev);
	
	VSPI_DEBUG("backend changed: %s -> %s\n", xbdev->otherend, xenbus_strstate(backend_state));
	
	switch (backend_state)
	{
		case XenbusStateInitialising:
			vspifront_set_state(xbdev, XenbusStateInitialising);
			break;
		case XenbusStateInitialised:
		case XenbusStateReconfiguring:
		case XenbusStateReconfigured:
		case XenbusStateUnknown:
			break;

		case XenbusStateInitWait:
			if (xbdev->state != XenbusStateInitialising)
				break;
			if ((ret = vspifront_connect(xbdev)) != 0){
				VSPI_ERR("vspifront_connect failed: %d\n", ret);
				break;
			}

			vspifront_set_state(xbdev, XenbusStateConnected);
			VSPI_DEBUG("connected: %s\n", (dev->state == XenbusStateConnected) ? "OK" : "Failed");
			break;

		case XenbusStateConnected:
			VSPI_DEBUG("Other side says it is connected as well.\n");
			if(dev->resume){
				for(i = 0; i < dev->spi_master_data->num_chipselect; i++){
					if(dev->spi_master_data->spi_devices[i])
						vspi_master_setup(dev->spi_master_data->spi_devices[i]);
				}
				dev->resume = 0;
			}
			break;

		case XenbusStateClosed:
			if (xbdev->state == XenbusStateClosed)
				break;
			/* Missed the backend's CLOSING state -- fallthrough */
		case XenbusStateClosing:
			if (xbdev->state != XenbusStateConnected)
				break;
			shutdown_vspifront(xbdev);
			xenbus_frontend_closed(xbdev);
			vspifront_set_state(xbdev, XenbusStateInitialising);
	}
}

static int vspifront_resume(struct xenbus_device *xbdev)
{
	struct vspifront_dev* dev;
	VSPI_LOG("vspifront_resume()");
	dev = dev_get_drvdata(&xbdev->dev);
	
	if(!dev){
		VSPI_LOG("no device!");
		return -ENODEV;
	}
	//~ VSPI_LOG("spi_master: %p\n", dev->spi_master_data->master);
	
	//~ if(!dev->spi_device){
		//~ VSPI_LOG("no spi_device!");
		//~ return 0;
	//~ }
	
	//~ VSPI_LOG("spi_device: %s\n", dev->spi_device->modalias);
	//~ VSPI_LOG("max_speed_hz: %d\n", dev->spi_device->max_speed_hz);
	//~ VSPI_LOG("chip_select: %d\n", dev->spi_device->chip_select);
	
	dev->resume = 1;
	
	return 0;
}
//~ static int vspifront_suspend(struct xenbus_device *xbdev)
//~ {
	//~ struct vspifront_dev* dev;
	//~ VSPI_LOG("vspifront_suspend()");
	//~ dev = dev_get_drvdata(&xbdev->dev);
	
	//~ return 0;
//~ }

int vspifront_remove(struct xenbus_device *xbdev)
{
	VSPI_LOG("vspifront_remove: %s\n", xbdev->nodename);
	
	shutdown_vspifront(xbdev);
	return 0;
}

static const struct xenbus_device_id vspifront_ids[] = {
	{ "vspi" },
	{ "" }
};

static struct xenbus_driver vspifront_driver = {
	.ids = vspifront_ids,
	.probe = vspifront_probe,
	.remove = vspifront_remove,
	.otherend_changed = vspiback_changed,
	.resume = vspifront_resume,
	//~ .suspend = vspifront_suspend,
};

int xenvspi_xenbus_register()
{
	return xenbus_register_frontend(&vspifront_driver);
}

void xenvspi_xenbus_unregister()
{
	xenbus_unregister_driver(&vspifront_driver);
}
