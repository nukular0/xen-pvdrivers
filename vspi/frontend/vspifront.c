#include "vspifront.h"
#include <linux/string.h>
#include <linux/irq.h>
#include <linux/list_sort.h>
#include "spi_master.h"



static int __init vspifront_init(void)
{
	int ret;
	printk(KERN_NOTICE "vspi: init\n");
	if (!xen_domain()){	
		printk(KERN_NOTICE "vspi: no xen domain\n");
		return -ENODEV;
	}

	INIT_LIST_HEAD(&xbdevs.list);

	ret = xenvspi_xenbus_register();
	if(ret)
		VSPI_ERR("xenvspi_xenbus_register(): %d", ret);


	return ret;
}
module_init(vspifront_init);


static void __exit vspifront_exit(void)
{
	struct xbdev_list *item;
	struct list_head *pos, *q;
	VSPI_DEBUG("vspi: exit\n");
	
	// shutdown all xenbus devices when module is removed
	list_for_each_safe(pos, q, &xbdevs.list){
		item = list_entry(pos, struct xbdev_list, list);
		VSPI_DEBUG("shutting down %s\n", item->xbdev->nodename);
		vspifront_remove(item->xbdev);
		list_del(pos);
		kfree(item);
	}
	xenvspi_xenbus_unregister();
}
module_exit(vspifront_exit);

MODULE_AUTHOR("David Tondorf");
MODULE_DESCRIPTION("Xen virtual spi device frontend");
MODULE_LICENSE("GPL");
MODULE_ALIAS(DRIVER_NAME);
