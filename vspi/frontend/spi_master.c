#include "spi_master.h"
#include "vspifront.h"

//~ static void vspi_master_release(struct device *dev);

static int vspi_request_from_msg(struct spi_message *msg,  struct vspifront_dev *dev);

int finish_message_thread(void* data)
{
	struct vspifront_dev 	*dev;
		
	dev = (struct vspifront_dev*)data;
	
	if(!dev){
		VSPI_ERR("finish_message_thread: no device\n");
		return -ENODEV;
	}
	set_current_state(TASK_INTERRUPTIBLE);
    while (!kthread_should_stop()) {
		set_current_state(TASK_INTERRUPTIBLE);
		schedule();
		
		if(kthread_should_stop())
			break;
		
		finish_message((unsigned long)dev);

    }
	VSPI_DEBUG("finish_message_thread terminating\n");
    set_current_state(TASK_RUNNING);

	return 0;
}

/*
 * Create a new CMD_SPI_TRANSFER_ONE_MSG request in devs current_request
 * dev->current_request must be NULL to create a new request.
 * return 0 on success or negative errno on error.
 */
static int vspi_request_from_msg(struct spi_message *msg,  struct vspifront_dev *dev)
{
	struct list_head 		*pos;
	struct spi_transfer 	*transfer;
	struct vspi_transfer 	*vtransfer;
	struct xenbus_device 	*xbdev;
	struct spi_message_info *info;
	int 					err;
	int 					i = 0;
	
	if(!dev || !dev->xbdev){
		return -ENODEV;
	}
	
	xbdev = dev->xbdev;
	
	if(dev->current_request){
		VSPI_DEBUG("vspi_request_from_msg: current_request not NULL!\n");
		return -EBUSY;
	}
	
	dev->current_request = kzalloc(sizeof(vspi_request_t), GFP_KERNEL);
	if(!dev->current_request){
		VSPI_DEBUG("vspi_request_from_msg: error allocating memory for new request!\n");
		return -ENOMEM;
	}
	
	dev->current_request->cmd = CMD_SPI_TRANSFER_ONE_MSG;
	
	/* allocate a page for spi_message_info */
	//~ alloc_pages_exact(sizeof(struct spi_message_info), GFP_KERNEL);
	dev->current_message_info = (struct spi_message_info*)get_zeroed_page(GFP_KERNEL);
	
	if(!dev->current_message_info){
		VSPI_ERR("no memory for spi_message_info\n");
		return -ENOMEM;
	}
		
	/* Grant access to info page */
	err = xenbus_grant_ring(xbdev, dev->current_message_info, 1, &dev->current_request->info_gref);
	if(err < 0){
		VSPI_ERR("error granting access to spi_message_info: %d\n", err);
		goto error_post_ring_alloc;
	}

	dev->current_message_info->frame_length = msg->frame_length;
	dev->current_message_info->num_transfers = 0;
	/* for each spi_transfer in msg:
	 * 	allocate and share page for buffer for each transfer 
	 * 	create a vspi_transfer and add to spi_message_info.transfers
	 * 	increase spi_message_info.num_transfers
	 */	 
	list_for_each(pos, &msg->transfers){
		transfer = list_entry(pos, struct spi_transfer, transfer_list);
		
		//~ VSPI_DEBUG("transfer %d: %d Bytes", i++, transfer->len);
		
		info = dev->current_message_info;
		
		vtransfer = info->transfers + info->num_transfers;
		
		vtransfer->fe_buf = alloc_pages_exact(transfer->len, GFP_KERNEL);
		if(!vtransfer->fe_buf){
			VSPI_ERR("no memory for vtransfer->fe_buf\n");
			 /*
			  * If we failed while allocating memory for
			  * the first transfer, we have not allocated any
			  * memory for transfers yet, so just unmap and free 
			  * the spi_message_info page.
			  * Otherwise we have allocated and mapped memory for
			  * at least one transfer.
			  */
			if(dev->current_message_info->num_transfers == 0){
				goto error_post_info_map;
			}
			else{
				goto error_post_vtrans_alloc;				
			}
		}
		
		vtransfer->num_grefs = (transfer->len/PAGE_SIZE) + 1;
		
		err = xenbus_grant_ring(xbdev, vtransfer->fe_buf, vtransfer->num_grefs, vtransfer->grefs);
		if(err < 0){
			VSPI_ERR("error granting access to vtransfer->fe_buf: %d\n", err);
			if(dev->current_message_info->num_transfers == 0){
				goto error_post_vtrans_alloc;
			}
			else{
				goto error_post_vtrans_map;
			}
		}
		VSPI_DEBUG("created %d grefs: gref[0]: %d\n", vtransfer->num_grefs, vtransfer->grefs[0]);
		
		
		vtransfer->len = transfer->len;
		vtransfer->cs_change = transfer->cs_change;
		vtransfer->tx_nbits = transfer->tx_nbits;
		vtransfer->rx_nbits = transfer->rx_nbits;
		vtransfer->bits_per_word = transfer->bits_per_word;
		vtransfer->delay_usecs = transfer->delay_usecs;
		vtransfer->speed_hz = transfer->speed_hz;
		vtransfer->rx_buf = transfer->rx_buf;
		vtransfer->tx_buf = transfer->tx_buf;
		if(transfer->tx_buf){
			memcpy(vtransfer->fe_buf, transfer->tx_buf, transfer->len);
		}

		info->num_transfers++;
	}
	
	return 0;
	
error_post_vtrans_map:
	vtransfer = &(dev->current_message_info->transfers[dev->current_message_info->num_transfers]);
	for(i = 0; i < vtransfer->num_grefs; i++){
		gnttab_end_foreign_access_ref(vtransfer->grefs[i], 0);		
	}
error_post_vtrans_alloc:
	free_pages_exact(vtransfer->fe_buf, vtransfer->len);
	if(--dev->current_message_info->num_transfers >= 0)
		goto error_post_vtrans_map;
error_post_info_map:
	gnttab_end_foreign_access_ref(dev->current_request->info_gref, 0);		
error_post_ring_alloc:
	free_page((unsigned long)dev->current_message_info);
	return err;
}

static int vspi_master_transfer_one_message(struct spi_master *master,
					   struct spi_message *msg)
{

	struct spi_master_data *master_data;
	struct vspifront_dev *dev;
	vspi_request_t req;
	int ret;
	
	
	VSPI_DEBUG("vspi_master_transfer_one_message(): %d (%d) Byte\n", msg->frame_length, msg->actual_length);
	
	master_data = spi_master_get_devdata(master);

	if(!master_data){
		VSPI_ERR("no master_data\n");
		return -ENODEV;
	}
	
	dev = master_data->vspidev;
	if(!dev){
		VSPI_ERR("no dev\n");
		return -ENODEV;
	}

	vspi_request_from_msg(msg, dev);


	if(!dev->current_request){
		VSPI_ERR("no current request\n");
	}

	memcpy(&req, dev->current_request, sizeof(req));

	ret = vspifront_send_request(dev, req);
	if(ret)
		finish_message((unsigned long)dev);
	
	return ret;
}


int vspi_master_setup(struct spi_device *spi)
{
	struct spi_master_data *master_data;
	struct vspifront_dev *dev;
	vspi_request_t req;
	int ret;
	VSPI_DEBUG("setup()\n");

	if(!spi){
		VSPI_DEBUG("spi is NULL\n");
		return -EINVAL;
	}

	
	master_data = spi_master_get_devdata(spi->master);

	VSPI_DEBUG("master data at %p\n", master_data);

	if(!master_data){
		VSPI_ERR("no master_data\n");
		return -ENODEV;
	}
	dev = master_data->vspidev;
	if(!dev){
		VSPI_ERR("no dev\n");
		return -ENODEV;
	}
	
	/* Create SETUP request */
	req.cmd = CMD_SPI_SETUP;
	req.spi_dev_data.max_speed_hz = spi->max_speed_hz;
	req.spi_dev_data.chip_select = spi->chip_select;
	req.spi_dev_data.bits_per_word = spi->bits_per_word;
	req.spi_dev_data.mode = spi->mode;
	req.spi_dev_data.cs_gpio = spi->cs_gpio;
	req.spi_dev_data.bus_num = master_data->bus_num;
	strncpy(req.spi_dev_data.modalias, spi->modalias, SPI_NAME_SIZE);
	
	ret = vspifront_send_request(dev, req);
	
	if(ret)
		return ret;
		
	master_data->spi_devices[spi->chip_select] = spi;
	
	return 0;
}

static void vspi_master_cleanup(struct spi_device *spi)
{
	struct spi_master_data *master_data;
	vspi_request_t req;
	struct vspifront_dev *dev;
	VSPI_DEBUG("cleanup\n");

	if(!spi){
		VSPI_ERR("spi is NULL\n");
	}
	
	master_data = spi_master_get_devdata(spi->master);

	if(!master_data){
		VSPI_ERR("no master_data\n");
		return;
	}
	VSPI_DEBUG("master_data at %p\n", master_data);
	
	dev = master_data->vspidev;
	if(!dev){
		VSPI_ERR("no dev\n");
		return;
	}
		
	
	/* Create CLEANUP request */
	req.cmd = CMD_SPI_CLEANUP;
	req.spi_dev_data.max_speed_hz = spi->max_speed_hz;
	req.spi_dev_data.chip_select = spi->chip_select;
	req.spi_dev_data.bits_per_word = spi->bits_per_word;
	req.spi_dev_data.mode = spi->mode;
	req.spi_dev_data.cs_gpio = spi->cs_gpio;
	req.spi_dev_data.bus_num = master_data->bus_num;
	strncpy(req.spi_dev_data.modalias, spi->modalias, SPI_NAME_SIZE);
	
	vspifront_send_request(dev, req);
	
	master_data->spi_devices[spi->chip_select] = NULL;
}


int vspi_master_probe(struct platform_device *pdev)
{
	struct device *dev = &pdev->dev;
	struct spi_master_data **m_data, *master_data;
	struct spi_master *master;
	int status;

	VSPI_DEBUG("vspi_master_probe\n");
	
	
	m_data = dev_get_platdata(&pdev->dev);
	if(!m_data){
		dev_err(&pdev->dev, "m_data is null\n");
		return -ENODATA;
	}
	VSPI_DEBUG("m_data at %p\n", m_data);
	
	master_data = *m_data;
	
	if(!master_data){
		dev_err(&pdev->dev, "master_data is null\n");
		return -ENODATA;
	}
	
	VSPI_DEBUG("master_data at %p\n", master_data);
	
	VSPI_DEBUG("max_speed_hz: %d\n", master_data->max_speed_hz);
	VSPI_DEBUG("bus_num: %d\n", master_data->bus_num);
	VSPI_DEBUG("num_chipselect: %d\n", master_data->num_chipselect);
	
	master = spi_alloc_master(dev, sizeof(struct platform_device*));
	if (!master) {
		dev_err(&pdev->dev, "cannot alloc spi_master\n");
		return -ENOMEM;
	}

	master->dev.of_node = pdev->dev.of_node;
	/* the spi->mode bits understood by this driver: */
	master->mode_bits = SPI_CPOL | SPI_CPHA | SPI_CS_HIGH | SPI_LOOP;

	master->bus_num = master_data->bus_num;
	master->num_chipselect = master_data->num_chipselect;
	master->max_speed_hz = master_data->max_speed_hz;
	
	master->cleanup = vspi_master_cleanup;
	master->setup = vspi_master_setup;
	master->transfer_one_message = vspi_master_transfer_one_message;
	
	master_data->master = master;

	spi_master_set_devdata(master, master_data);
	VSPI_DEBUG("spi_master_set_devdata: %p\n", &pdev);
	

	/* Register with the SPI framework */
	status = devm_spi_register_master(&pdev->dev, master);
	if (status != 0) {
		dev_err(&pdev->dev, "problem registering spi master\n");
		goto error_postalloc;
	}
	
	master_data->spi_devices = kzalloc(master->num_chipselect * sizeof(struct spi_device*), GFP_KERNEL);
	if(!master_data->spi_devices){
		status = -ENOMEM;
		goto error_postalloc;
	}
	
	return status;
	
error_postalloc:
	spi_master_put(master);
	return status;

}

static int vspi_master_remove(struct platform_device *pdev)
{
	VSPI_DEBUG("vspi_master_remove\n");
	return 0;
}

static void vspi_master_shutdown(struct platform_device *pdev)
{
	int status = 0;
	VSPI_DEBUG("vspi_master_shutdown\n");
	if ((status = vspi_master_remove(pdev)) != 0)
		dev_err(&pdev->dev, "shutdown failed with %d\n", status);
}



//~ static void vspi_master_release(struct device *dev){
	//~ printk("vspi_release()");
//~ }

//~ static struct platform_device spi_master_device = {
        //~ .name           = DRIVER_NAME,
        //~ .id             = -1,
		//~ .dev = {
			//~ .release = vspi_master_release,
		//~ }
//~ };


static struct platform_driver spi_master_driver = {
	.driver = {
		.name	= DRIVER_NAME,
	},
	.probe = vspi_master_probe,
	.remove = vspi_master_remove,
	.shutdown = vspi_master_shutdown,
};

int vspi_master_init(struct vspifront_dev *dev)
{
	int ret;
	struct platform_device_info pi;
	
	VSPI_DEBUG("vspi_master_init\n");
	ret = platform_driver_register(&spi_master_driver);
	
	if(ret){
		VSPI_ERR("registering platform deriver failed: %d\n", ret);
		return ret;	
	}
	
	VSPI_DEBUG("max_speed_hz: %d\n", dev->spi_master_data->max_speed_hz);
	VSPI_DEBUG("bus_num: %d\n", dev->spi_master_data->bus_num);
	VSPI_DEBUG("num_chipselect: %d\n", dev->spi_master_data->num_chipselect);
	
	memset(&pi, 0, sizeof(pi));
	pi.fwnode = dev->xbdev->dev.fwnode;
	pi.parent = &dev->xbdev->dev;
	pi.name = DRIVER_NAME;
	pi.id = -1;
	// platform_device_register_full will copy pi.data so we have to
	// store the address of the pointer to master data, so 
	// we will still have access to the correct master data later
	pi.data = &dev->spi_master_data;
	pi.size_data = sizeof(struct spi_master_data*);

	dev->spi_master_data->pdev = platform_device_register_full(&pi);
	
	if (IS_ERR(dev->spi_master_data->pdev)) {
		VSPI_ERR("registering platform device failed: %ld\n", PTR_ERR(dev->spi_master_data->pdev));
		platform_driver_unregister(&spi_master_driver);
		return PTR_ERR(dev->spi_master_data->pdev);
	}
	
	VSPI_DEBUG("platform_device at %p\n", dev->spi_master_data->pdev);
	//~ VSPI_DEBUG("setting data of dev at %p to %p\n", &dev->spi_master_data->pdev->dev, dev);
	//~ dev_set_drvdata(&dev->spi_master_data->pdev->dev, dev);

	return ret;
	
	
}

void vspi_master_exit(struct vspifront_dev *dev)
{
	VSPI_DEBUG("vspi_master_exit\n");
	
	platform_device_unregister(dev->spi_master_data->pdev);
	platform_driver_unregister(&spi_master_driver);
	
}

