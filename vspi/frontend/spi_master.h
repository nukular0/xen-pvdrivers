#ifndef _SPI_MASTER_TEST_
#define _SPI_MASTER_TEST_

#include <linux/init.h>
#include <linux/device.h>
#include <linux/errno.h>
#include <linux/err.h>
#include <linux/interrupt.h>
#include <linux/platform_device.h>
#include <linux/interrupt.h> 

#include "vspifront.h"

int vspi_master_init(struct vspifront_dev *dev);
void vspi_master_exit(struct vspifront_dev *dev);

extern int vspi_master_setup(struct spi_device *spi);

#endif
