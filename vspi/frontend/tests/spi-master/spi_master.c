#include <linux/init.h>
#include <linux/module.h>
#include <linux/device.h>
#include <linux/errno.h>
#include <linux/err.h>
#include <linux/interrupt.h>
#include <linux/kernel.h>
#include <linux/platform_device.h>
#include <linux/spi/spi.h>
#include <linux/delay.h>
#include <linux/interrupt.h> 
#include <linux/list.h>

#include "spi_master.h"

#define DRIVER_NAME "spi-master-test"


MODULE_AUTHOR("David Tondorf");
MODULE_DESCRIPTION("SPI Master Test Module");
MODULE_LICENSE("GPL");
MODULE_ALIAS("platform:" DRIVER_NAME);

#define PRINT_DEBUG
#ifdef PRINT_DEBUG
#define DEBUG(fmt,...) printk("spi-master:Debug("__FILE__":%d) " fmt, __LINE__, ##__VA_ARGS__)
#define DEBUG_MORE(fmt,...) printk(fmt, ##__VA_ARGS__)
#else
#define DEBUG(fmt,...)
#endif
#define LOG(fmt,...) printk(KERN_NOTICE "spi-master:Info " fmt, ##__VA_ARGS__)
#define ERR(fmt,...) printk(KERN_ERR "spi-master:Error " fmt, ##__VA_ARGS__)



static int spi_test_spi_transfer_one_message(struct spi_master *master,
					   struct spi_message *msg)
{
	
	struct spi_transfer *transfer;
	struct list_head *pos;
	int i = 0;
	DEBUG("spi_test_spi_transfer_one_message(): %d (%d) Byte\n", msg->frame_length, msg->actual_length);
	
	list_for_each(pos, &msg->transfers){
		transfer = list_entry(pos, struct spi_transfer, transfer_list);
		DEBUG("transfer %d: %d Bytes", i++, transfer->len);
	}
	spi_finalize_current_message(master);
	return 0;
}

static int spi_test_spi_unprepare_transfer(struct spi_master *master)
{
	DEBUG("spi_test_spi_unprepare_transfer\n");
	//~ struct driver_data *drv_data = spi_master_get_devdata(master);

	return 0;
}


static int setup(struct spi_device *spi)
{
	DEBUG("setup()\n");

	if(!spi){
		DEBUG("spi is NULL\n");
		return -EINVAL;
	}
	
	DEBUG("spi->max_speed_hz: %d\n", spi->max_speed_hz);
	DEBUG("spi->chip_select: %d\n", spi->chip_select);
	DEBUG("spi->bits_per_word: %d\n", spi->bits_per_word);
	DEBUG("spi->mode: %d\n", spi->mode);
	DEBUG("spi->irq: %d\n", spi->irq);
	DEBUG("spi->modalias: %s\n", spi->modalias);
	DEBUG("spi->cs_gpio: %d\n", spi->cs_gpio);

	return 0;
}

static void cleanup(struct spi_device *spi)
{
	//~ struct driver_data *drv_data = spi_master_get_devdata(spi->master);

	DEBUG("cleanup\n");

	if(!spi){
		DEBUG("spi is NULL\n");
	}
	
	DEBUG("spi->max_speed_hz: %d\n", spi->max_speed_hz);
	DEBUG("spi->chip_select: %d\n", spi->chip_select);
	DEBUG("spi->bits_per_word: %d\n", spi->bits_per_word);
	DEBUG("spi->mode: %d\n", spi->mode);
	DEBUG("spi->irq: %d\n", spi->irq);
	DEBUG("spi->modalias: %s\n", spi->modalias);
	DEBUG("spi->cs_gpio: %d\n", spi->cs_gpio);
}


static inline struct spi_test_spi_master *
spi_test_spi_init_pdata(struct platform_device *pdev)
{
	struct spi_test_spi_master *pdata;
	DEBUG("spi_test_spi_init_pdata\n");
	pdata = devm_kzalloc(&pdev->dev, sizeof(*pdata), GFP_KERNEL);
	if (!pdata)
		return NULL;
	pdata->num_chipselect = 1;
	
	return pdata;
}


static int spi_test_spi_probe(struct platform_device *pdev)
{
	struct device *dev = &pdev->dev;
	struct spi_test_spi_master *platform_info;
	struct spi_master *master;
	struct driver_data *drv_data;
	int status;

	DEBUG("spi_test_spi_probe\n");
	
	platform_info = dev_get_platdata(dev);
	if (!platform_info) {
		platform_info = spi_test_spi_init_pdata(pdev);
		if (!platform_info) {
			dev_err(&pdev->dev, "missing platform data\n");
			return -ENODEV;
		}
	}


	master = spi_alloc_master(dev, sizeof(struct driver_data));
	if (!master) {
		dev_err(&pdev->dev, "cannot alloc spi_master\n");
		return -ENOMEM;
	}
	drv_data = spi_master_get_devdata(master);
	drv_data->master = master;
	drv_data->master_info = platform_info;
	drv_data->pdev = pdev;

	master->dev.of_node = pdev->dev.of_node;
	/* the spi->mode bits understood by this driver: */
	master->mode_bits = SPI_CPOL | SPI_CPHA | SPI_CS_HIGH | SPI_LOOP;

	master->bus_num = 1;
	//~ master->dma_alignment = DMA_ALIGNMENT;
	master->cleanup = cleanup;
	master->setup = setup;
	master->transfer_one_message = spi_test_spi_transfer_one_message;
	master->unprepare_transfer_hardware = spi_test_spi_unprepare_transfer;
	//master->flags = SPI_MASTER_MUST_RX | SPI_MASTER_MUST_TX;

	master->max_speed_hz = 25e6;

	master->num_chipselect = 3;


	/* Register with the SPI framework */
	platform_set_drvdata(pdev, drv_data);
	status = devm_spi_register_master(&pdev->dev, master);
	if (status != 0) {
		dev_err(&pdev->dev, "problem registering spi master\n");
	}

	return status;

	return status;
}

static int spi_test_spi_remove(struct platform_device *pdev)
{
	DEBUG("spi_test_spi_remove\n");
	return 0;
}

static void spi_test_spi_shutdown(struct platform_device *pdev)
{
	int status = 0;
	DEBUG("spi_test_spi_shutdown\n");
	if ((status = spi_test_spi_remove(pdev)) != 0)
		dev_err(&pdev->dev, "shutdown failed with %d\n", status);
}

static void spi_test_release(struct device *dev);

static void spi_test_release(struct device *dev){
	printk("spi_test_release()");
}

static struct platform_device spi_test_device = {
        .name           = DRIVER_NAME,
        .id             = -1,
		.dev = {
			.release = spi_test_release,
		}
};


static struct platform_driver driver = {
	.driver = {
		.name	= DRIVER_NAME,
	},
	.probe = spi_test_spi_probe,
	.remove = spi_test_spi_remove,
	.shutdown = spi_test_spi_shutdown,
};

static int __init spi_test_spi_init(void)
{
	int ret;
	DEBUG("spi_test_spi_init\n");
	ret = platform_driver_register(&driver);
	DEBUG("platform_driver_register: %d\n", ret);
	
	if(ret)
		return ret;	
	
	//~ pdev = platform_device_alloc(DRIVER_NAME, -1);
	//~ if(!pdev)
		//~ return -ENOMEM;
		
	//~ ret = platform_device_add(pdev);
	ret = platform_device_register(&spi_test_device);
	DEBUG("platform_device_register: %d\n", ret);
	
	return ret;
}
subsys_initcall(spi_test_spi_init);

static void __exit spi_test_spi_exit(void)
{
	DEBUG("spi_test_spi_exit\n");
	platform_device_unregister(&spi_test_device);
	platform_driver_unregister(&driver);
	
}
module_exit(spi_test_spi_exit);
