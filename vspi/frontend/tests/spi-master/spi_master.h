#ifndef _SPI_MASTER_TEST_
#define _SPI_MASTER_TEST_

struct spi_test_spi_master {
	u16 num_chipselect;
	u8 enable_dma;
};

struct spi_test_spi_chip {
	u8 tx_threshold;
	u8 tx_hi_threshold;
	u8 rx_threshold;
	u8 dma_burst_size;
	u32 timeout;
	u8 enable_loopback;
	int gpio_cs;
	void (*cs_control)(u32 command);
};

struct driver_data {
	/* Driver model hookup */
	struct platform_device *pdev;


	/* SPI framework hookup */
	struct spi_master *master;

	struct spi_test_spi_master *master_info;

	/* Message Transfer pump */
	struct tasklet_struct pump_transfers;

	/* Current message transfer state info */
	struct spi_transfer *cur_transfer;
	size_t len;
	void *tx;
	void *tx_end;
	void *rx;
	void *rx_end;
	u8 n_bytes;
	int (*write)(struct driver_data *drv_data);
	int (*read)(struct driver_data *drv_data);
	irqreturn_t (*transfer_handler)(struct driver_data *drv_data);
	void (*cs_control)(u32 command);

	/* GPIOs for chip selects */
	struct gpio_desc **cs_gpiods;
};

struct chip_data {
	u32 cr1;
	u32 dds_rate;
	u32 timeout;
	u8 n_bytes;
	u32 dma_burst_size;
	u32 threshold;
	u32 dma_threshold;
	u16 lpss_rx_threshold;
	u16 lpss_tx_threshold;
	u8 enable_dma;
	union {
		int gpio_cs;
		unsigned int frm;
	};
	int gpio_cs_inverted;
	int (*write)(struct driver_data *drv_data);
	int (*read)(struct driver_data *drv_data);
	void (*cs_control)(u32 command);
};



#endif
