#include <linux/init.h>
#include <linux/module.h>
#include <linux/spi/spi.h>
 
#define MY_BUS_NUM 1


char *b;
struct spi_transfer	t = {
		.len		= 11,
		.speed_hz	= 1e6,
	};
struct spi_message	m;

static struct spi_device *dev1;
 

void vspi_msg_complete(void* context)
{
	printk("vspi_msg_complete()\n");
}

static int __init spi_init(void)
{
	char buf[11] = {'H','A','L','L','O',' ','W','E','L','T', 0};
    DECLARE_COMPLETION_ONSTACK(done);
    int ret;
    struct spi_master *master;
    
    
    //Register information about your slave device:
    struct spi_board_info spi_device_info = {
        .modalias = "dev1",
        .max_speed_hz = 1e6, //speed your device (slave) can handle
        .bus_num = MY_BUS_NUM,
        .chip_select = 0,
        .mode = 0,
    };
     
    //~ struct spi_board_info spi_device_info2 = {
        //~ .modalias = "my-device-driver-name",
        //~ .max_speed_hz = 1e6, //speed your device (slave) can handle
        //~ .bus_num = MY_BUS_NUM,
        //~ .chip_select = 1,
        //~ .mode = 1,
    //~ };
    /*To send data we have to know what spi port/pins should be used. This information 
      can be found in the device-tree. */
    master = spi_busnum_to_master( spi_device_info.bus_num );
    if( !master ){
        printk("MASTER not found.\n");
            return -ENODEV;
    }
     
    // create a new slave device, given the master and device info
    dev1 = spi_new_device( master, &spi_device_info );
    //~ spi_device2 = spi_new_device( master, &spi_device_info2 );
 
 
    if( !dev1) {
        printk("FAILED to create slave.\n");
        return -ENODEV;
    }
     
 
	b = kzalloc(11, GFP_KERNEL);
	if(!b){
		printk("no mem!\n"); 
		return -ENOMEM;
	}
	
	memcpy(b, "Hallo Welt", 11);
		
	//~ spi_message_init(&m);

	//~ m.complete = vspi_msg_complete;
	//~ m.context = &done;
	//~ t.tx_buf = b;
 
	//~ spi_message_add_tail(&t, &m);
    
    ret = spi_write(dev1, b, 11);
    printk("spi_write: %d\n", ret);
	
	//~ ret = spi_async(dev1, &m);
    //~ printk("spi_async: %d\n", ret);
 
   
    
    return 0;
}
 
 
static void __exit spi_exit(void)
{
	printk("myspidev: exit\n"); 
    if( dev1 ){
        //~ spi_write(spi_device, &ch, sizeof(ch));
        spi_unregister_device( dev1 );
        //~ spi_unregister_device( spi_device2 );
    }
    if(b)
		kfree(b);
}
 
module_init(spi_init);
module_exit(spi_exit);
 
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Piktas Zuikis <[email protected]>");
MODULE_DESCRIPTION("SPI module example");
