#ifndef _XEN_VSPIFRONT_H
#define _XEN_VSPIFRONT_H

#include <linux/module.h>  /* Needed by all modules */
#include <linux/kernel.h>  /* Needed for KERN_ALERT */

#include <xen/xen.h>       /* We are doing something with Xen */
#include <xen/xenbus.h>
#include <xen/events.h>
#include <xen/interface/event_channel.h>
#include <xen/interface/grant_table.h>
#include <xen/interface/io/ring.h>
#include <xen/page.h>
#include <linux/errno.h>
#include <linux/list.h>
#include <linux/semaphore.h>
#include <linux/platform_device.h>
#include <xen/grant_table.h>
#include <linux/spi/spi.h>
#include <linux/kthread.h>
#include <linux/sched.h>
#include "../vspiif.h"


#define DRIVER_NAME "xen-frontend:vspi"

#define INVALID_RESPONSE			-9999

#define nVSPI_PRINT_DEBUG
#ifdef VSPI_PRINT_DEBUG
#define VSPI_DEBUG(fmt,...) printk("vspi:Debug("__FILE__":%d) " fmt, __LINE__, ##__VA_ARGS__)
#define VSPI_DEBUG_MORE(fmt,...) printk(fmt, ##__VA_ARGS__)
#else
#define VSPI_DEBUG(fmt,...)
#endif
#define VSPI_LOG(fmt,...) printk(KERN_NOTICE "vspi:Info " fmt, ##__VA_ARGS__)
#define VSPI_ERR(fmt,...) printk(KERN_ERR "vspi:Error("__FILE__":%d) " fmt, __LINE__, ##__VA_ARGS__)



struct xbdev_list{
	struct list_head list;
	struct xenbus_device *xbdev;
};

extern struct xbdev_list xbdevs;


	
struct spi_master_data{
	struct platform_device 	*pdev;
	struct vspifront_dev	*vspidev;
	struct spi_master 		*master;
	u32 					max_speed_hz;
	s16 					bus_num;
	u16 					num_chipselect;
	struct spi_device 		**spi_devices;
};


struct vspifront_dev {
	domid_t 				bedomid;
	char* 					nodename;
	char* 					bepath;
	enum xenbus_state 		state;
	struct xenbus_device 	*xbdev;
	
	grant_ref_t 			ring_ref;
	struct 					vspi_front_ring ring;
	unsigned long 			ring_page;
	
	evtchn_port_t 			comm_evtchn;
	unsigned int 			comm_evtchn_irq;		
	
	evtchn_port_t 			msg_completed_evtchn;
	unsigned int 			msg_completed_evtchn_irq;		
	
	struct semaphore 		sem; // Semaphore used for waiting for responses from backend

	struct spi_master_data 	*spi_master_data;

	vspi_response_t 		last_response;
	vspi_request_t			*current_request;
	
	/*
	 * reference to a page that stores information about 
	 * the spi_message to be tranfered
	 */
	struct spi_message_info *current_message_info;
	
	struct task_struct 		*finish_msg_task;
	struct semaphore 		finish_msg_sem; // Semaphore used for waiting for responses from backend
	
	int 					resume;
};


/*Initialize frontend */
struct vspifront_dev* init_vspifront(struct xenbus_device *xbdev, const char* nodename);
/*Shutdown frontend */
void shutdown_vspifront(struct xenbus_device *xbdev);
void free_vspifront(struct xenbus_device *xbdev);

int vspifront_remove(struct xenbus_device *xbdev);

int xenvspi_xenbus_register(void);
void xenvspi_xenbus_unregister(void);

void vspifront_set_state(struct xenbus_device *xbdev, 
									enum xenbus_state state);


/*
 * Executed by tasklet after vspifront_msg_completed_interrupt
 */
void finish_message(unsigned long);

int finish_message_thread(void* data);

//static irqreturn_t vspifront_comm_interrupt(int irq, void *dev_id);

int vspifront_send_request(struct vspifront_dev* dev, vspi_request_t req);


#endif /* _XEN_VSPIFRONT_H */
