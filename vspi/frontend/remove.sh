#!/bin/sh
mod="xen-vspifront.ko"
domid=$(sudo xenstore-read domid)
state=$(sudo xenstore-read /local/domain/$domid/device/vspi/0/state)
echo "Old state is $state"
echo "Removing module..."
rm_result=$(sudo rmmod $mod)
echo "remove: $rm_result"
sudo xenstore-write /local/domain/$domid/device/vspi/0/state 1
state=$(sudo xenstore-read /local/domain/$domid/device/vspi/0/state)
echo "New state is $state"
