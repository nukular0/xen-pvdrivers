#include "common.h"


void get_and_handle_requests(struct vspidev *dev)
{
	int 				moretodo;
	RING_IDX 			rq, cons;
	vspi_request_t 		*req;

	moretodo = 1;
	
	if(!dev || !dev->ring.sring)
		return;
	
	VSPI_DEBUG("get_and_handle_requests()\n");
	while(moretodo){
		rq = dev->ring.sring->req_prod;
		rmb(); /* Ensure we see queued responses up to 'rp'. */
		cons = dev->ring.req_cons;
	   
		while ((cons != rq))
		{
			req = RING_GET_REQUEST(&dev->ring, cons);
			vspi_handle_request(dev, req);
			dev->ring.req_cons = ++cons;
			if (dev->ring.req_cons != cons)
				/* We reentered, we must not continue here */
				break;
		}	
		RING_FINAL_CHECK_FOR_REQUESTS(&dev->ring, moretodo);
	}
	
	dev->request_available = 0;
}

int get_requests_thread(void *data)
{
	struct vspidev 		*dev;
		
	dev = (struct vspidev*)data;;
	if(!dev){
		VSPI_ERR("get_requests_thread: no device\n");
		return -ENODEV;
	}
	
	
	VSPI_DEBUG("get_requests_thread started\n");
	sched_setscheduler(dev->get_requests_task, SCHED_FIFO, &(struct sched_param){.sched_priority = 49});
	VSPI_DEBUG("new priority: %d\n",dev->get_requests_task->prio);
    
    while (!kthread_should_stop()) {

		VSPI_DEBUG("going to sleep\n");
		wait_event_interruptible(dev->requests_thread_wakeup_event, (dev->request_available || kthread_should_stop()));
        if(kthread_should_stop()) 
			break;
		
		VSPI_DEBUG("getting requests\n");
		get_and_handle_requests(dev);
    }
	VSPI_DEBUG("get_requests_thread done\n");
    

    return 0;
	

}



irqreturn_t vspiback_comm_interrupt_handler(int irq, void *dev_id)
{
	struct backend_info *be;
	struct vspidev *dev;
	
	
	VSPI_DEBUG("vspiback_comm_interrupt_handler()\n");
	be = (struct backend_info*)dev_id;
	
	if(!be){
		VSPI_ERR("vspiback_interrupt_handler: be is NULL\n");
		return IRQ_HANDLED;
	}
	
	dev = be->vspi;
	
	VSPI_DEBUG("waking up requests task\n");
	//tasklet_hi_schedule(&dev->get_requests_tasklet);
	//~ wake_up_process(dev->get_requests_task);
	dev->request_available = 1;
	wake_up(&dev->requests_thread_wakeup_event);

	return IRQ_HANDLED;
}

void vspi_send_response(struct vspidev *dev, vspi_response_t *rsp){
	RING_IDX i;
	vspi_response_t *_rsp;
	int notify;
	
	i = dev->ring.rsp_prod_pvt;
	_rsp = RING_GET_RESPONSE(&dev->ring, i);
	_rsp->ret = rsp->ret;
	dev->ring.rsp_prod_pvt = i + 1;

	wmb();
	RING_PUSH_RESPONSES_AND_CHECK_NOTIFY(&dev->ring, notify);
	if(notify) 
	{
		notify_remote_via_irq(dev->comm_irq);
	}
	else{
		//~ VSPI_DEBUG("not notified\n");
	}
}


struct vspidev *vspidev_alloc(domid_t fe_domid)
{
	struct vspidev *dev;
	
	dev = kzalloc(sizeof(struct vspidev), GFP_KERNEL);
	if(!dev)
		goto error;
	
	dev->fe_domid = fe_domid;
	
	mutex_init(&dev->current_message_mutex);
	sema_init(&dev->requests_semaphore, 0);
	init_waitqueue_head(&dev->requests_thread_wakeup_event);
	
	
	return dev;

error:
	kfree(dev);
	return ERR_PTR(-ENOMEM);
}

void vspidev_free(struct vspidev *dev)
{
	// free device memory
	kfree(dev);
}
