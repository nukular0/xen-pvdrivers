#include "common.h"
#include <linux/time.h>
/*
 * Will be called on completen of the spi_message tranfer
 * context is a pointer to the vspidev that send the message
 */
void vspi_msg_complete(void* context)
{
	struct vspidev 				*dev;
	struct spi_transfer			*xfer, *xfers;
	struct vspi_transfer		*vxfer;
	int							i;
	
	VSPI_DEBUG("vspi_msg_complete\n");
	
	dev = (struct vspidev*)context;
	mutex_lock(&dev->current_message_mutex);
	if(!dev || !dev->current_message){
		return;
	}
	
	xfers = dev->current_xfers;
	
	for(i = 0; i < dev->current_message_info->num_transfers; i++){
		vxfer = &dev->current_message_info->transfers[i];
		xfer = vxfer->spi_transfer;
		
		if(xfer->rx_buf){
			memcpy(vxfer->be_buf, xfer->rx_buf, xfer->len);
		}
		
		wmb();
		xenbus_unmap_ring_vfree(dev->xbdev, vxfer->be_buf);
		
		
		if(xfer->tx_buf)
			kfree(xfer->tx_buf);
		else
			kfree(xfer->rx_buf);
	}
	

	kfree(dev->current_xfers);
	dev->current_xfers = NULL;
	
	xenbus_unmap_ring_vfree(dev->xbdev, dev->current_message_info);
	dev->current_message_info = NULL;
	
	kfree(dev->current_message);
	dev->current_message = NULL;
	
	// then notify the frontend
	mutex_unlock(&dev->current_message_mutex);
	notify_remote_via_irq(dev->msg_completed_evtchn_irq);
	VSPI_DEBUG("vspi_msg_complete done\n");
}

static int vspi_make_msg_from_request(struct vspidev *dev, vspi_request_t *req)
{
	struct spi_transfer			*xfers, *xfer;
	struct vspi_transfer		*vxfer;
	int 						i;
	void 						*buf, *cur_msg_addr;
	int 						err;
	
	buf = NULL;
	VSPI_DEBUG("vspi_make_msg_from_request(): dev %p req %p xbdev %p\n", dev, req, dev->xbdev);
	if(!mutex_trylock(&dev->current_message_mutex)){
		VSPI_ERR("mutex_trylock failed\n");
		return -EBUSY;
	}
	
	VSPI_DEBUG("allocating memory\n");
	dev->current_message = kzalloc(sizeof(struct spi_message), GFP_KERNEL);
	if(!dev->current_message){
		VSPI_ERR("can not allocate memory for current_message\n");
		err = -ENOMEM;
		goto err;
	}
		
	if(!req){
		VSPI_ERR("no request\n");
		err = -ENODATA;
		goto err;
	}
	if(!req->info_gref){
		VSPI_ERR("!req->info_gref\n");
		err = -ENODATA;
		goto err;
	}

	if(!&dev->current_message_info){
		VSPI_ERR("!&dev->current_message_info\n");
		err = -ENODATA;
		goto err;
	}
	

	VSPI_DEBUG("xenbus_map_ring_valloc memory\n");
	// map page with information about transfers
	err = xenbus_map_ring_valloc(dev->xbdev, &req->info_gref,1, &cur_msg_addr);
	if(err){
		VSPI_ERR("Error while trying to map shared page: %d\n", err);
		dev->current_message_info = NULL;
		goto err_postmsg;
	}
	dev->current_message_info = (struct spi_message_info*)cur_msg_addr;

	VSPI_DEBUG("allocating memoryfor current_xfers\n");
	dev->current_xfers = kzalloc(dev->current_message_info->num_transfers * sizeof(struct spi_transfer), GFP_KERNEL);
	if(!dev->current_xfers){
		VSPI_ERR("error allocating memory for dev->current_xfers\n");
		err = -ENOMEM;
		goto err_postmap_info;
	}
	xfers = dev->current_xfers;
	
	for(i = 0; i < dev->current_message_info->num_transfers; i++){
		
		VSPI_DEBUG("transfer %d\n", i);
		vxfer = &dev->current_message_info->transfers[i];
		xfer = &xfers[i];
		
		// map gref from vspi_transfer
		VSPI_DEBUG("Mapping %d grefs...\n", vxfer->num_grefs);
		err = xenbus_map_ring_valloc(dev->xbdev, vxfer->grefs, vxfer->num_grefs, &vxfer->be_buf);
		if(err){
			VSPI_ERR("error mapping memory for transfer %d: %d\n", i, err);
			goto err_postmap_xfer;
		}
			
		if(vxfer->tx_buf){
			xfer->tx_buf = kzalloc(vxfer->len, GFP_KERNEL);
			if(!xfer->tx_buf){
				err = -ENOMEM;
				goto err_postmap_xfer;
			}
			if(vxfer->rx_buf)
				xfer->rx_buf = (void*)xfer->tx_buf;
			
			memcpy((void*)xfer->tx_buf, vxfer->be_buf, vxfer->len);
			
		}
		else{
			xfer->rx_buf = kzalloc(vxfer->len, GFP_KERNEL);
			if(!xfer->rx_buf){
				err = -ENOMEM;
				goto err_postmap_xfer;
			}
		}
		
		
		xfer->len = vxfer->len;
		xfer->cs_change = vxfer->cs_change;
		xfer->tx_nbits = vxfer->tx_nbits;
		xfer->rx_nbits = vxfer->rx_nbits;
		xfer->bits_per_word = vxfer->bits_per_word;
		xfer->delay_usecs = vxfer->delay_usecs;
		xfer->speed_hz = vxfer->speed_hz;	
		
		vxfer->spi_transfer = xfer;
		
		//~ VSPI_DEBUG("xfer->len: %d\n", xfer->len);
		//~ VSPI_DEBUG("xfer->cs_change: %d\n", xfer->cs_change);
		//~ VSPI_DEBUG("xfer->tx_nbits: %d\n", xfer->tx_nbits);
		//~ VSPI_DEBUG("xfer->rx_nbits: %d\n", xfer->rx_nbits);
		//~ VSPI_DEBUG("xfer->bits_per_word: %d\n", xfer->bits_per_word);
		//~ VSPI_DEBUG("xfer->delay_usecs: %d\n", xfer->delay_usecs);
		//~ VSPI_DEBUG("xfer->speed_hz: %d\n", xfer->speed_hz);
		//~ VSPI_DEBUG("xfer->rx_buf: %s\n", (char*)xfer->rx_buf);
		//~ VSPI_DEBUG("xfer->tx_buf: %s\n", (char*)xfer->tx_buf);
	} 
	
	VSPI_DEBUG("spi_message_init_with_transfers\n");
	spi_message_init_with_transfers(dev->current_message, xfers, dev->current_message_info->num_transfers);
	
	dev->current_message->complete = vspi_msg_complete;
	dev->current_message->context = (void*)dev;
	
	
	
	mutex_unlock(&dev->current_message_mutex);
	return 0;
	
err_postmap_xfer:
	for(; i > 0; i--){
		xfer = &xfers[i-1];
		if(xfer->tx_buf)
			buf = (void*)xfer->tx_buf;
		else
			buf = xfer->rx_buf;
		
		xenbus_unmap_ring_vfree(dev->xbdev, buf);		
	}
	kfree(xfers);
	dev->current_xfers = NULL;
err_postmap_info:
		xenbus_unmap_ring_vfree(dev->xbdev, (void*)dev->current_message_info);	
		dev->current_message_info = NULL;	
err_postmsg:
	kfree(dev->current_message);
	dev->current_message = NULL;
err:
	mutex_unlock(&dev->current_message_mutex);
	return err;
}

static int vspi_handle_transfer_request(struct vspidev *dev, vspi_request_t *req)
{
	int ret;
	struct spi_transfer *xfer;
#ifdef VSPI_PRINT_DEBUG
	struct timespec t1, t2;
#endif	

	VSPI_DEBUG("vspi_handle_transfer_request()\n");
	if(!dev || !dev->spi_device)
		return -ENODEV;
		
	if(dev->current_message_info){
		VSPI_ERR("dev->current_message_info exists!\n");
		return -EBUSY;
	}
	
	// this should not happen since we can only get a new message once
	// the current message is complete
	if(dev->current_message)
		return -EBUSY;
	
	// will create a spi message in dev->current_message
	ret = vspi_make_msg_from_request(dev, req);
	
	if(ret){
		VSPI_ERR("error creating spi_message: %d\n", ret);
		return ret;
	}
	
	VSPI_DEBUG("spi message created:\n");
	
	list_for_each_entry(xfer, &dev->current_message->transfers, transfer_list){
		VSPI_DEBUG("xfer->len: %d\n", xfer->len);
		VSPI_DEBUG("xfer->cs_change: %d\n", xfer->cs_change);
		VSPI_DEBUG("xfer->tx_nbits: %d\n", xfer->tx_nbits);
		VSPI_DEBUG("xfer->rx_nbits: %d\n", xfer->rx_nbits);
		VSPI_DEBUG("xfer->bits_per_word: %d\n", xfer->bits_per_word);
		VSPI_DEBUG("xfer->delay_usecs: %d\n", xfer->delay_usecs);
		VSPI_DEBUG("xfer->speed_hz: %d\n", xfer->speed_hz);
		//~ VSPI_DEBUG("xfer->rx_buf: %s\n", (const char*)xfer->rx_buf);
		//~ VSPI_DEBUG("xfer->tx_buf: %s\n", (const char*)xfer->tx_buf);
	}
	
	//~ ret = spi_write(dev->spi_device, "Test", 4);
	
#ifdef VSPI_PRINT_DEBUG
	getnstimeofday(&t1);
#endif
	ret = spi_async(dev->spi_device, dev->current_message);
#ifdef VSPI_PRINT_DEBUG
	getnstimeofday(&t2);
	VSPI_DEBUG("spi_async(): %d after %ldns\n", ret,t2.tv_nsec - t1.tv_nsec);
#endif
	return ret;
	
}


static int vspi_handle_setup_request(struct vspidev *dev, vspi_request_t *req)
{
	struct spi_board_info 	*spi_device_info;
	struct spi_device_data	*devdata;
	
	VSPI_DEBUG("vspi_handle_setup_request()\n");
	devdata = &req->spi_dev_data;
	
	/*
	 * If we do not have a spi device yet, create a new one and register
	 * it with the spi subsystem
	 */ 
	if(!dev->spi_device){
		dev->spi_master = spi_busnum_to_master(devdata->bus_num);
		if(!dev->spi_master){
			printk("MASTER not found.\n");
            return -ENODEV;
		}
		
		
		spi_device_info = kzalloc(sizeof(struct spi_board_info), GFP_KERNEL);
		
		spi_device_info->bus_num = devdata->bus_num;
		spi_device_info->max_speed_hz = devdata->max_speed_hz;
		spi_device_info->chip_select = devdata->chip_select;
		spi_device_info->mode = devdata->mode;
		strlcpy(spi_device_info->modalias, devdata->modalias, SPI_NAME_SIZE);
		
		
		VSPI_DEBUG("creating new spi_device\n");
		VSPI_DEBUG("bus_num: %d\n", spi_device_info->bus_num);
		VSPI_DEBUG("max_speed_hz: %d\n", spi_device_info->max_speed_hz);
		VSPI_DEBUG("chip_select: %d\n", spi_device_info->chip_select);
		VSPI_DEBUG("mode: %d\n", spi_device_info->mode);
		VSPI_DEBUG("modalias: %s\n", spi_device_info->modalias);
		
		dev->spi_device = spi_new_device(dev->spi_master, spi_device_info);
		
		if( !dev->spi_device ) {
			printk("FAILED to create slave.\n");
			return -ENODEV;
		}
		kfree(spi_device_info);
		VSPI_DEBUG("spi_device created\n");
	}
	/* 
	 * we already have a spi_device and just need to change it's parameters
	 */
	else{
		VSPI_DEBUG("spi_device exists...updating\n");
		dev->spi_device->max_speed_hz = devdata->max_speed_hz;
		dev->spi_device->chip_select = devdata->chip_select;
		dev->spi_device->mode = devdata->mode;
		strlcpy(dev->spi_device->modalias, devdata->modalias, SPI_NAME_SIZE);
		return spi_setup(dev->spi_device);
	}
	
	return 0;
}
static int vspi_handle_cleanup_request(struct vspidev *dev)
{
	VSPI_DEBUG("vspi_handle_cleanup_request\n");
	if(!dev){
		VSPI_ERR("no vspidev\n");
		return -ENODEV;
	}
	
	if(dev->spi_device){
		spi_unregister_device(dev->spi_device);
		dev->spi_device = NULL;
	}
	
	return 0;
}



void vspi_handle_request(struct vspidev *dev, vspi_request_t *req)
{
	vspi_response_t rsp;
	VSPI_DEBUG("vspi_handle_request: %d\n", req->cmd);

	
	switch(req->cmd){
		case CMD_SPI_TRANSFER_ONE_MSG:
			rsp.ret = vspi_handle_transfer_request(dev, req);
			break;
		case CMD_SPI_SETUP: 
			rsp.ret = vspi_handle_setup_request(dev, req);
			break;
		case CMD_SPI_CLEANUP: 
			rsp.ret = vspi_handle_cleanup_request(dev);
			break;
		default:
			rsp.ret = -EINVAL;
	}
	
	vspi_send_response(dev, &rsp);
}

// On loading this kernel module, we register as a frontend driver
static int __init vspi_init(void)
{
	printk(KERN_NOTICE "vspi: init\n");
	if (!xen_domain()){	
		return -ENODEV;
	}
	
	return xenvspi_xenbus_init();
}
module_init(vspi_init);

// ...and on unload we unregister
static void __exit vspi_exit(void)
{
	
	printk(KERN_ALERT "vspi: exit\n");
	xenvspi_xenbus_fini();	
}
module_exit(vspi_exit);

MODULE_LICENSE("GPL");
MODULE_ALIAS("xen-backend:vspi");
