/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation; or, when distributed
 * separately from the Linux kernel or incorporated into other
 * software packages, subject to the following license:
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this source file (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy, modify,
 * merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
#ifndef __XEN_VSPIBACK__COMMON_H__
#define __XEN_VSPIBACK__COMMON_H__

#include <linux/module.h>  /* Needed by all modules */
#include <linux/kernel.h>  /* Needed for KERN_ALERT */

#include <xen/xen.h>       /* We are doing something with Xen */
#include <xen/xenbus.h>
#include <xen/events.h>
#include <xen/interface/event_channel.h>
#include <xen/interface/grant_table.h>
#include <xen/interface/io/ring.h>
#include <xen/page.h>
#include <linux/errno.h>
#include <linux/list.h>
#include <linux/workqueue.h>
#include <linux/init.h>
#include <linux/spi/spi.h>
#include <linux/mutex.h>
#include <linux/semaphore.h>
#include <linux/kthread.h>
#include <linux/sched.h>
#include "../vspiif.h"

#define nVSPI_PRINT_DEBUG
#ifdef VSPI_PRINT_DEBUG
#define VSPI_DEBUG(fmt,...) printk("vspi:Debug("__FILE__":%d) " fmt, __LINE__, ##__VA_ARGS__)
#define VSPI_DEBUG_MORE(fmt,...) printk(fmt, ##__VA_ARGS__)
#else
#define VSPI_DEBUG(fmt,...)
#endif

#define VSPI_LOG(fmt,...) printk(KERN_NOTICE "vspi:Info " fmt, ##__VA_ARGS__)
#define VSPI_ERR(fmt,...) printk(KERN_ERR "vspi:Error("__FILE__":%d) " fmt, __LINE__, ##__VA_ARGS__)


struct backend_info {
	struct xenbus_device *dev;
	struct vspidev *vspi;

	/* This is the state that will be reflected in xenstore
	 */
	enum xenbus_state state;
	enum xenbus_state frontend_state;
};


struct vspidev {
	domid_t 				fe_domid;
	unsigned int 			handle;
	struct xenbus_device 	*xbdev;

	/* event channel for communication / commands from front end */
	evtchn_port_t 			comm_evtchn; 	
	/* interrupt number for events on comm_evtchn */
	unsigned int 			comm_irq;		

	/* Shared ring */
	struct vspi_back_ring 	ring; 
		
	struct spi_device 		*spi_device;
	struct spi_master 		*spi_master;
	
	evtchn_port_t 			msg_completed_evtchn;
	unsigned int 			msg_completed_evtchn_irq;
	
	/* 
	 * Pointer to the spi_message we are currently processing
	 * We can not get more than one message at once, because
	 * the frontend can only send a new spi_message once we notify it about
	 * completion of the current message.
	 */
	struct spi_message		*current_message;
	struct spi_message_info	*current_message_info;
	struct spi_transfer		*current_xfers;
	
	// protects the creation and removal of current_message
	struct mutex			current_message_mutex;
	
	//~ struct tasklet_struct 	get_requests_tasklet;
	
	struct task_struct 		*get_requests_task;
	struct semaphore 		requests_semaphore;
	wait_queue_head_t 		requests_thread_wakeup_event;
	int 					request_available;
	
};


irqreturn_t vspiback_comm_interrupt_handler(int irq, void *dev_id);

int xenvspi_xenbus_init(void);
void xenvspi_xenbus_fini(void);

struct vspidev *vspidev_alloc(domid_t fe_domid);
void vspidev_free(struct vspidev *vspi);
void vspi_msg_complete(void* context);

int get_requests_thread(void *data);

void vspi_handle_request(struct vspidev *dev, vspi_request_t *req);
void vspi_send_response(struct vspidev *dev, vspi_response_t *rsp);


#endif /* __XEN_VSPIBACK__COMMON_H__ */
