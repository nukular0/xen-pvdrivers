#include "common.h"
#include <linux/string.h>


static void backend_connect(struct backend_info *be);
static void backend_disconnect(struct backend_info *be);
static void set_backend_state(struct backend_info *be,
			      enum xenbus_state state);
static int backend_create_vspidev(struct backend_info *be);


/* 
 * dummy irq handler for event channels we do not expect to
 * receive any events on, like msg_completed_evtchn
 */
static irqreturn_t vspi_dummy_handler(int irq, void *dev_id){
	return IRQ_HANDLED;
}

static int backend_create_vspidev(struct backend_info *be)
{
	int 					err;
	struct vspidev 			*vspi;
	struct xenbus_device 	*dev = be->dev;
	
	if (be->vspi != NULL)
		return 0;

	vspi = vspidev_alloc(dev->otherend_id);
	if (IS_ERR(vspi)) {
		err = PTR_ERR(vspi);
		xenbus_dev_fatal(dev, err, "creating interface ");
		return err;
	}
	vspi->xbdev = dev;
	be->vspi = vspi;
	
	kobject_uevent(&dev->dev.kobj, KOBJ_ONLINE);
	return 0;
}	

// This is where we set up path watchers and event channels
static void backend_connect(struct backend_info *be)
{
	unsigned int 			val;
	grant_ref_t 			ring_ref;
	unsigned int 			evtchn;
	int 					err;
	void					*ring_addr;
	struct xenbus_device 	*xbdev = be->dev;
	struct vspi_sring 		*shared;
	struct vspidev 			*dev = be->vspi;
	char 					threadname[100];
	
	if(!xbdev || !dev){
		VSPI_LOG("backend_connect error");
		return;
	}
	
	VSPI_LOG("Connecting the backend now\n");
	
	VSPI_DEBUG("Fetching ring-ref\n");
	/* Fetch the grant reference (ring-ref from fe-path) */
	err = xenbus_scanf(XBT_NIL, xbdev->otherend,
			   "ring-ref", "%u", &val);
	if (err < 0){
		VSPI_LOG("No ring-ref in %s\n", xbdev->otherend);
		goto done; /* The frontend does not have a control ring */
	}
	ring_ref = val;
	VSPI_DEBUG("ring ref is %u\n", ring_ref);

	/* Map shared page */
	err = xenbus_map_ring_valloc(xbdev, &ring_ref,1, &ring_addr);
	if(err != 0){
		VSPI_LOG("Error while trying to map shared page: %d\n", err);
		goto fail;
	}
	shared = (struct vspi_sring*)ring_addr;

	/* Initialize shared ring */
    BACK_RING_INIT(&dev->ring, shared, XEN_PAGE_SIZE);
	VSPI_LOG("Shared ring mapped successfully!\n");

	VSPI_DEBUG("fetching event-channel\n");
	/* Fetch event channel for main communication*/
	err = xenbus_scanf(XBT_NIL, xbdev->otherend,
			   "event-channel", "%u", &val);
	if (err < 0) {
		VSPI_LOG("No event-channel in %s\n", xbdev->otherend);
		xenbus_dev_fatal(xbdev, err,
				 "reading %s/event-channel",
				 xbdev->otherend);
		goto fail;
	}

	evtchn = val;
	dev->comm_evtchn = evtchn;
	VSPI_DEBUG("evtchn is %u\n", dev->comm_evtchn);
	
	
	/* bind event channel */
	err = bind_interdomain_evtchn_to_irqhandler(
			// id of FE domU, evtchn, interrupt handler,          flags
			xbdev->otherend_id, evtchn, vspiback_comm_interrupt_handler, 0,
			// xbdevice name,  xbdevice-id (void*), will be passed to handler
			xbdev->devicetype, be);
	VSPI_DEBUG("bind result: %d\n", err);
	
	if(err < 0){
		goto fail;
	}
	dev->comm_irq = err;
		
		
	/* Fetch event channel for notification about completed messages */
	err = xenbus_scanf(XBT_NIL, xbdev->otherend,
			   "msg-event-channel", "%u", &val);
	if (err < 0) {
		VSPI_LOG("No msg-event-channel in %s\n", xbdev->otherend);
		xenbus_dev_fatal(xbdev, err,
				 "reading %s/msg-event-channel",
				 xbdev->otherend);
		goto fail;
	}
	dev->msg_completed_evtchn = val;
	
	/* bind event channel for msg completion */
	err = bind_interdomain_evtchn_to_irqhandler(
			// id of FE domU, evtchn, interrupt handler,          			flags
			xbdev->otherend_id, dev->msg_completed_evtchn, vspi_dummy_handler, 0,
			// xbdevice name,  xbdevice-id (void*), will be passed to handler
			xbdev->devicetype, NULL);
	VSPI_DEBUG("bind result: %d\n", err);
	
	if(err < 0){
		goto fail;
	}
	dev->msg_completed_evtchn_irq = err;
	
	snprintf(threadname, 100, "vspi-req-handler-%s", xbdev->otherend);
	dev->get_requests_task = kthread_run(get_requests_thread, dev, threadname);
	VSPI_DEBUG("dev->get_requests_task: %p\n", dev->get_requests_task);
	if(!dev->get_requests_task){
		VSPI_ERR("could not start thread dev->get_requests_task\n");
		goto fail;
	}
done:
	return;

fail:
	backend_disconnect(be);		
	return;
	
}

// This will destroy event channel handlers
static void backend_disconnect(struct backend_info *be)
{
	int ret;
	struct vspidev *dev;
	pr_info("Disconnecting the backend now\n");
	VSPI_LOG("Unmapping shared page and event channel\n");
	
	dev = be->vspi;
	if(!dev)
		return;
	
	if(dev->get_requests_task){
		kthread_stop(dev->get_requests_task);
		dev->get_requests_task = NULL;
	//~ wake_up_process(dev->get_requests_task);
	}
	else{
		VSPI_ERR("dev->get_requests_task is NULL!\n"); 
	}
	
	if(dev->spi_device)
		spi_unregister_device( dev->spi_device );
			
	if(dev->comm_irq){
		unbind_from_irqhandler(dev->comm_irq, be);
		dev->comm_irq = 0;
	}
	if(dev->msg_completed_evtchn_irq){
		unbind_from_irqhandler(dev->msg_completed_evtchn_irq, 0);
		dev->msg_completed_evtchn_irq = 0;
	}
	
	VSPI_DEBUG("Unbound from IRQ handler");
	
	if(dev->ring.sring){
		ret = xenbus_unmap_ring_vfree(be->dev, (void*)dev->ring.sring);
		VSPI_DEBUG("Unmap shared page result: %d\n", ret);		
	}
}

static inline void backend_switch_state(struct backend_info *be,
					enum xenbus_state state)
{
	struct xenbus_device *dev = be->dev;

	VSPI_DEBUG("%s -> %s\n", dev->nodename, xenbus_strstate(state));
	be->state = state;

	xenbus_switch_state(dev, state);
}

// We try to switch to the next state from a previous one
static void set_backend_state(struct backend_info *be,
			      enum xenbus_state state)
{
	while (be->state != state) {
		switch (be->state) {
		case XenbusStateInitialising:
			switch (state) {
			case XenbusStateInitWait:
			case XenbusStateConnected:
			case XenbusStateClosing:
				backend_switch_state(be, XenbusStateInitWait);
				break;
			case XenbusStateClosed:
				backend_switch_state(be, XenbusStateClosed);
				break;
			default:
				BUG();
			}
			break;
		case XenbusStateClosed:
			switch (state) {
			case XenbusStateInitWait:
			case XenbusStateConnected:
				backend_switch_state(be, XenbusStateInitWait);
				break;
			case XenbusStateClosing:
				backend_switch_state(be, XenbusStateClosing);
				break;
			default:
				BUG();
			}
			break;
		case XenbusStateInitWait:
			switch (state) {
			case XenbusStateConnected:
				backend_connect(be);
				backend_switch_state(be, XenbusStateConnected);
				break;
			case XenbusStateClosing:
			case XenbusStateClosed:
				backend_switch_state(be, XenbusStateClosing);
				break;
			default:
				BUG();
			}
			break;
		case XenbusStateConnected:
			switch (state) {
			case XenbusStateInitWait:
			case XenbusStateClosing:
			case XenbusStateClosed:
				backend_disconnect(be);
				backend_switch_state(be, XenbusStateClosing);
				break;
			default:
				BUG();
			}
			break;
		case XenbusStateClosing:
			switch (state) {
			case XenbusStateInitWait:
			case XenbusStateConnected:
			case XenbusStateClosed:
				backend_switch_state(be, XenbusStateClosed);
				break;
			default:
				BUG();
			}
			break;
		default:
			BUG();
		}
	}
}

static int vspiback_remove(struct xenbus_device *dev)
{
	struct backend_info *be = dev_get_drvdata(&dev->dev);

	set_backend_state(be, XenbusStateClosed);

	if (be->vspi) {
		//~ kobject_uevent(&dev->dev.kobj, KOBJ_OFFLINE);
		//~ vspi_msg_complete(dev);
		vspidev_free(be->vspi);
		be->vspi = NULL;
	}
	kfree(be);
	dev_set_drvdata(&dev->dev, NULL);
	return 0;
}

// The function is called on activation of the device
static int vspiback_probe(struct xenbus_device *dev,
			const struct xenbus_device_id *id)
{
	int err;
	struct backend_info *be = kzalloc(sizeof(struct backend_info),
					  GFP_KERNEL);
	if (!be) {
		xenbus_dev_fatal(dev, -ENOMEM,
				 "allocating backend structure");
		return -ENOMEM;
	}

	be->dev = dev;
	dev_set_drvdata(&dev->dev, be);

	be->state = XenbusStateInitialising;
	err = xenbus_switch_state(dev, XenbusStateInitialising);
	if (err)
		goto fail;
	
	err = backend_create_vspidev(be);
	if (err)
		goto fail;
	

	return 0;
fail:
	VSPI_DEBUG("failed\n");
	vspiback_remove(dev);
	return err;
}



// The function is called on a state change of the frontend driver
static void vspi_frontend_changed(struct xenbus_device *dev, enum xenbus_state frontend_state)
{
	
	struct backend_info *be = dev_get_drvdata(&dev->dev);

	VSPI_DEBUG("frontend changed: %s -> %s\n", dev->otherend, xenbus_strstate(frontend_state));

	be->frontend_state = frontend_state;
	
	switch (frontend_state) {
		case XenbusStateInitialising:
			set_backend_state(be, XenbusStateInitWait);
			break;

		case XenbusStateInitialised:
			break;

		case XenbusStateConnected:
			set_backend_state(be, XenbusStateConnected);
			break;

		case XenbusStateClosing:
			VSPI_LOG("Frontend is closing\n");
			set_backend_state(be, XenbusStateClosing);
			break;

		case XenbusStateClosed:
			set_backend_state(be, XenbusStateClosed);
			if (xenbus_dev_is_online(dev))
				break;
			/* fall through if not online */
		case XenbusStateUnknown:
			set_backend_state(be, XenbusStateClosed);
			device_unregister(&dev->dev);
			break;

		default:
			xenbus_dev_fatal(dev, -EINVAL, "saw state %s (%d) at frontend",
					xenbus_strstate(frontend_state), frontend_state);
			break;
	}
}


static int vspiback_resume(struct xenbus_device *xbdev)
{
	VSPI_LOG("vspiback_resume()");
		
	return 0;
}

static int vspiback_suspend(struct xenbus_device *xbdev)
{
	VSPI_LOG("vspiback_suspend()");
	return 0;
}



// This defines the name of the devices the driver reacts to
static const struct xenbus_device_id vspiback_ids[] = {
	{ "vspi" },
	{ "" }
};

// We set up the callback functions
static struct xenbus_driver vspi_driver = {
	.ids  = vspiback_ids,
	.probe = vspiback_probe,
	.remove = vspiback_remove,
	.otherend_changed = vspi_frontend_changed,
	.resume = vspiback_resume,
	.suspend = vspiback_suspend,
};

int xenvspi_xenbus_init(void)
{
	return xenbus_register_backend(&vspi_driver);
}

void xenvspi_xenbus_fini(void)
{
	return xenbus_unregister_driver(&vspi_driver);
}
