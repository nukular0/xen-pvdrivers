#include "common.h"
#include <linux/string.h>

static void backend_connect(struct backend_info *be);
static void backend_disconnect(struct backend_info *be);
static void set_backend_state(struct backend_info *be,
			      enum xenbus_state state);
static int backend_create_pvcandev(struct backend_info *be);


/**
 * add a new pvcan_permission entry to list if necessary
 * (or extend the range of an existing entry)
 */
static void pvcan_add_permission(struct list_head *list, int from, int to)
{
	struct pvcan_permission *entry = NULL;
	struct pvcan_permission *new;
	
	if(from > to && to != -1)
		return;
	
	new = kzalloc(sizeof(struct pvcan_permission), GFP_KERNEL);
	
	if(!new){
		PVCAN_LOG("No memory for pvcan_permission!\n");
		return;
	}
	
	new->from = from;
	new->to = (to == -1) ? from : to;
	
	// if list is empty, we can just add a new element
	if(list_empty(list)){
		list_add(&new->list, list);
		PVCAN_LOG("list empty, added in beginning\n");
		goto done_added;
	} 
	
	/**
	 * otherwise we add perm in the right permission, so that list
	 * is sorted ascending and there are no overlapping regions
	 * if @from and @to fall within an existing region, we do nothing
	 * if from_new < to_other (@from starts in an existing region, but 
	 * @to goes beyong that region) we extend the existing region and do
	 * not add a new one
	 * same if @to goes into an existing region, but @from starts before that
	 * 
	 * not all cases are covered in this implementation, best to make sure
	 * in the vm config file not to create any overlapping regions
	 */
	 list_for_each_entry(entry, list, list){
		// case 'new' is completly inside 'entry' --> do nothing
		if(new->from >= entry->from && new->to <= entry->to){
			goto done_not_added;
		}
		// case 'new' starts in 'entry' but ends behind 'entry'
		// --> extend entry's to
		else if(new->from >= entry->from && new->from < entry->to){
			entry->to = new->to;
			goto done_not_added;
		}
		// case 'new' starts before 'entry' and ends within it
		// extend 'from'
		else if(new->from < entry->from && new->to < entry->to && new->to > entry->from){
			entry->from = new->from;
			goto done_not_added;
		}
		// case 'new' starts before 'entry' and ends after it
		// --> change both from and to of entry
		else if(new->from < entry->from && new->to > entry->to){
			entry->from = new->from;
			entry->to = new->to;
			goto done_not_added;
		}
		// case 'new' starts and ends before 'entry'
		// --> add 'new' to list before entry
		else if(new->to < entry->from){
			list_add(&new->list, &entry->list);
			goto done_added;
		} 
	 }
	 
	 if(!entry)
		goto done_added;
		
	// if we have not added 'new' yet, we have to add at at the end
	// of the list (entry points to the last element)
	list_add_tail(&new->list, &entry->list);
	goto done_added;

done_not_added:
	kfree(new);
done_added:
	return;
	  
}

/**
 * Parse a permission string read from xenstore and add the permissions
 * to list
 */
static void pvcan_parse_permission_string(char* str, struct list_head *list)
{
	char *p, *_str;
	int err;
	int num1, num2;
	
	if(!str){
		PVCAN_DEBUG("error str is NULL\n");
		return;
	}
	PVCAN_LOG("string: %s\n", str);
	
	_str = kstrdup(str, GFP_KERNEL);
	
	while ( (p = strsep(&_str, ";"))  != NULL){
		while (*p == ' ') p++;
		num1 = num2 = -1;
		err = sscanf(p, "%x-%x", &num1, &num2);
		if(!err)
			continue;
		PVCAN_DEBUG("found %d numbers: 0x%X, 0x%X\n", err, num1, num2);
		pvcan_add_permission(list, num1, num2);
	}
}

static void free_permissions_list(struct list_head *list)
{
	struct pvcan_permission *pos, *n;
	
	list_for_each_entry_safe(pos, n, list, list){
		list_del(&pos->list);
		kfree(pos);
	}
}

static int backend_create_pvcandev(struct backend_info *be)
{
	int err;
	struct xenbus_device *dev = be->dev;
	struct pvcandev *pvcan;
	
	if (be->pvcan != NULL)
		return 0;

	pvcan = pvcandev_alloc(dev->otherend_id);
	if (IS_ERR(pvcan)) {
		err = PTR_ERR(pvcan);
		xenbus_dev_fatal(dev, err, "creating interface ");
		return err;
	}
	be->pvcan = pvcan;
	
	INIT_LIST_HEAD(&pvcan->tx_permissions);
	INIT_LIST_HEAD(&pvcan->rx_permissions);

	kobject_uevent(&dev->dev.kobj, KOBJ_ONLINE);
	return 0;
}	

// This is where we set up path watchers and event channels
static void backend_connect(struct backend_info *be)
{
	unsigned int 			val;
	grant_ref_t 			ring_ref;
	unsigned int 			evtchn;
	int 					err;
	void 					*ring_addr;
	struct xenbus_device 	*dev = be->dev;
	struct pvcan_sring 		*shared;
	struct pvcandev 		*pvcan = be->pvcan;
	char 					tmpstr[512];
	
	
	if(!dev || !pvcan){
		PVCAN_LOG("backend_connect error");
		return;
	}
	
	PVCAN_LOG("Connecting the backend now\n");
	
	/* Fetch the grant reference (ring-ref from fe-path) */
	err = xenbus_scanf(XBT_NIL, dev->otherend,
			   "ring-ref", "%u", &val);
	if (err < 0){
		PVCAN_LOG("No ring-ref in %s\n", dev->otherend);
		goto done; /* The frontend does not have a control ring */
	}
	ring_ref = val;
	PVCAN_DEBUG("ring ref is %u\n", ring_ref);

	/* Map shared page */
	err = xenbus_map_ring_valloc(dev, &ring_ref,1, &ring_addr);
	if(err != 0){
		PVCAN_LOG("Error while trying to map shared page: %d\n", err);
		goto fail;
	}
	shared = (struct pvcan_sring*)ring_addr;

	/* Initialize shared ring */
    BACK_RING_INIT(&pvcan->ring, shared, XEN_PAGE_SIZE);
	PVCAN_LOG("Shared ring mapped successfully!\n");

	/* Fetch event channel (event-channel from fe-path) */
	err = xenbus_scanf(XBT_NIL, dev->otherend,
			   "event-channel", "%u", &val);
	if (err < 0) {
		PVCAN_LOG("No event-channel in %s\n", dev->otherend);
		xenbus_dev_fatal(dev, err,
				 "reading %s/event-channel-ctrl",
				 dev->otherend);
		goto fail;
	}

	evtchn = val;
	pvcan->comm_evtchn = evtchn;
	PVCAN_DEBUG("evtchn is %u\n", pvcan->comm_evtchn);
	
	/* Fetch device id */
	err = xenbus_scanf(XBT_NIL, dev->otherend,
			   "dev-id", "%u", &val);
	if (err < 0) {
		PVCAN_LOG("No dev-id in %s\n", dev->otherend);
		xenbus_dev_fatal(dev, err,
				 "reading %s/dev_id",
				 dev->otherend);
		goto fail;
	}

	pvcan->dev_id = val;
	PVCAN_DEBUG("dev_id is %u\n", pvcan->dev_id);
	
	/* bind event channel */
	err = bind_interdomain_evtchn_to_irqhandler(
			// id of FE domU, evtchn, interrupt handler,          flags
			dev->otherend_id, evtchn, pvcanback_comm_interrupt_handler, 0,
			// device name,  device-id (void*), will be passed to handler
			dev->devicetype, be);
	PVCAN_DEBUG("bind result: %d\n", err);
	
	if(err < 0){
		goto fail;
	}
	pvcan->comm_irq = err;
	
	err = xenbus_scanf(XBT_NIL, dev->nodename,
			   "tx-permissions", "%s", tmpstr);
	if (err < 0){
		PVCAN_LOG("Could not read tx-permissions from %s\n", dev->nodename);
		goto done;
	}
	
	if(!strncmp(tmpstr, "all", sizeof(tmpstr))){
		pvcan->tx_permission_all = 1;
	}
	else{
		pvcan_parse_permission_string(tmpstr, &pvcan->tx_permissions);
	}
	err = xenbus_scanf(XBT_NIL, dev->nodename,
			   "rx-permissions", "%s", tmpstr);
	if (err < 0){
		PVCAN_LOG("Could not read rx-permissions from %s\n", dev->nodename);
		goto done;
	}
	
	if(!strncmp(tmpstr, "all", sizeof(tmpstr))){
		pvcan->rx_permission_all = 1;
	}
	else{
		pvcan_parse_permission_string(tmpstr, &pvcan->rx_permissions);
	}
	
	
	list_add(&pvcan->list, &pvcan_devices);
	
	
done:
	return;

fail:
	backend_disconnect(be);		
	return;
	
}

// This will destroy event channel handlers
static void backend_disconnect(struct backend_info *be)
{
	int ret;
	struct pvcandev *pvcan;
	pr_info("Disconnecting the backend now\n");
	PVCAN_LOG("Unmapping shared page and event channel\n");
	
	pvcan = be->pvcan;
	if(!pvcan)
		return;
			
	if(pvcan->list.next){
		list_del(&pvcan->list);
	}
		
	if(pvcan->comm_irq){
		unbind_from_irqhandler(pvcan->comm_irq, be);
		pvcan->comm_irq = 0;
	}
	PVCAN_DEBUG("Unbound from IRQ handler");
	
	if(pvcan->ring.sring){
		ret = xenbus_unmap_ring_vfree(be->dev, (void*)pvcan->ring.sring);
		PVCAN_DEBUG("Unmap shared page result: %d\n", ret);		
	}
	
	free_permissions_list(&pvcan->tx_permissions);
	free_permissions_list(&pvcan->rx_permissions);
	
}

static inline void backend_switch_state(struct backend_info *be,
					enum xenbus_state state)
{
	struct xenbus_device *dev = be->dev;

	PVCAN_DEBUG("%s -> %s\n", dev->nodename, xenbus_strstate(state));
	be->state = state;

	xenbus_switch_state(dev, state);
}

// We try to switch to the next state from a previous one
static void set_backend_state(struct backend_info *be,
			      enum xenbus_state state)
{
	while (be->state != state) {
		switch (be->state) {
		case XenbusStateInitialising:
			switch (state) {
			case XenbusStateInitWait:
			case XenbusStateConnected:
			case XenbusStateClosing:
				backend_switch_state(be, XenbusStateInitWait);
				break;
			case XenbusStateClosed:
				backend_switch_state(be, XenbusStateClosed);
				break;
			default:
				BUG();
			}
			break;
		case XenbusStateClosed:
			switch (state) {
			case XenbusStateInitWait:
			case XenbusStateConnected:
				backend_switch_state(be, XenbusStateInitWait);
				break;
			case XenbusStateClosing:
				backend_switch_state(be, XenbusStateClosing);
				break;
			default:
				BUG();
			}
			break;
		case XenbusStateInitWait:
			switch (state) {
			case XenbusStateConnected:
				backend_connect(be);
				backend_switch_state(be, XenbusStateConnected);
				break;
			case XenbusStateClosing:
			case XenbusStateClosed:
				backend_switch_state(be, XenbusStateClosing);
				break;
			default:
				BUG();
			}
			break;
		case XenbusStateConnected:
			switch (state) {
			case XenbusStateInitWait:
			case XenbusStateClosing:
			case XenbusStateClosed:
				backend_disconnect(be);
				backend_switch_state(be, XenbusStateClosing);
				break;
			default:
				BUG();
			}
			break;
		case XenbusStateClosing:
			switch (state) {
			case XenbusStateInitWait:
			case XenbusStateConnected:
			case XenbusStateClosed:
				backend_switch_state(be, XenbusStateClosed);
				break;
			default:
				BUG();
			}
			break;
		default:
			BUG();
		}
	}
}

static int pvcanback_remove(struct xenbus_device *dev)
{
	struct backend_info *be = dev_get_drvdata(&dev->dev);

	set_backend_state(be, XenbusStateClosed);

	if (be->pvcan) {
		kobject_uevent(&dev->dev.kobj, KOBJ_OFFLINE);
		pvcandev_free(be->pvcan);
		be->pvcan = NULL;
	}
	kfree(be);
	dev_set_drvdata(&dev->dev, NULL);
	return 0;
}

// The function is called on activation of the device
static int pvcanback_probe(struct xenbus_device *dev,
			const struct xenbus_device_id *id)
{
	int err;
	struct backend_info *be = kzalloc(sizeof(struct backend_info),
					  GFP_KERNEL);
	if (!be) {
		xenbus_dev_fatal(dev, -ENOMEM,
				 "allocating backend structure");
		return -ENOMEM;
	}

	be->dev = dev;
	dev_set_drvdata(&dev->dev, be);

	be->state = XenbusStateInitialising;
	err = xenbus_switch_state(dev, XenbusStateInitialising);
	if (err)
		goto fail;
	
	err = backend_create_pvcandev(be);
	if (err)
		goto fail;
	

	return 0;
fail:
	PVCAN_DEBUG("failed\n");
	pvcanback_remove(dev);
	return err;
}



// The function is called on a state change of the frontend driver
static void pvcan_frontend_changed(struct xenbus_device *dev, enum xenbus_state frontend_state)
{
	
	struct backend_info *be = dev_get_drvdata(&dev->dev);

	PVCAN_DEBUG("frontend changed: %s -> %s\n", dev->otherend, xenbus_strstate(frontend_state));

	be->frontend_state = frontend_state;
	
	switch (frontend_state) {
		case XenbusStateInitialising:
			set_backend_state(be, XenbusStateInitWait);
			break;

		case XenbusStateInitialised:
			break;

		case XenbusStateConnected:
			set_backend_state(be, XenbusStateConnected);
			break;

		case XenbusStateClosing:
			PVCAN_LOG("Frontend is closing\n");
			set_backend_state(be, XenbusStateClosing);
			break;

		case XenbusStateClosed:
			set_backend_state(be, XenbusStateClosed);
			if (xenbus_dev_is_online(dev))
				break;
			/* fall through if not online */
		case XenbusStateUnknown:
			set_backend_state(be, XenbusStateClosed);
			device_unregister(&dev->dev);
			break;

		default:
			xenbus_dev_fatal(dev, -EINVAL, "saw state %s (%d) at frontend",
					xenbus_strstate(frontend_state), frontend_state);
			break;
	}
}


// This defines the name of the devices the driver reacts to
static const struct xenbus_device_id pvcanback_ids[] = {
	{ "vcan" },
	{ "" }
};

// We set up the callback functions
static struct xenbus_driver pvcan_driver = {
	.ids  = pvcanback_ids,
	.probe = pvcanback_probe,
	.remove = pvcanback_remove,
	.otherend_changed = pvcan_frontend_changed,
};

int xenpvcan_xenbus_init(void)
{
	return xenbus_register_backend(&pvcan_driver);
}

void xenpvcan_xenbus_fini(void)
{
	return xenbus_unregister_driver(&pvcan_driver);
}
