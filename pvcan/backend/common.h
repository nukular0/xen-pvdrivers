/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation; or, when distributed
 * separately from the Linux kernel or incorporated into other
 * software packages, subject to the following license:
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this source file (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy, modify,
 * merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
#ifndef __XEN_PVCANBACK__COMMON_H__
#define __XEN_PVCANBACK__COMMON_H__

#include <linux/module.h>  /* Needed by all modules */
#include <linux/kernel.h>  /* Needed for KERN_ALERT */

#include <xen/xen.h>       /* We are doing something with Xen */
#include <xen/xenbus.h>
#include <xen/events.h>
#include <xen/interface/event_channel.h>
#include <xen/interface/grant_table.h>
#include <xen/interface/io/ring.h>
#include <xen/page.h>
#include <linux/errno.h>
#include <linux/list.h>
#include <linux/workqueue.h>
#include <linux/init.h>
#include <linux/spinlock.h>
#include <linux/kthread.h>
#include <linux/sched.h>

#include <linux/netdevice.h>
#include <linux/can/dev.h>
#include <linux/interrupt.h>
#include <linux/delay.h>




#define nPVCAN_PRINT_DEBUG
#ifdef PVCAN_PRINT_DEBUG
#define PVCAN_DEBUG(fmt,...) printk("pvcan:Debug("__FILE__":%d) " fmt, __LINE__, ##__VA_ARGS__)
#define PVCAN_DEBUG_MORE(fmt,...) printk(fmt, ##__VA_ARGS__)
#else
#define PVCAN_DEBUG(fmt,...)
#endif

#define PVCAN_LOG(fmt,...) printk(KERN_NOTICE "pvcan:Info " fmt, ##__VA_ARGS__)
#define PVCAN_ERR(fmt,...) printk(KERN_ERR "pvcan:Error " fmt, ##__VA_ARGS__)


extern struct list_head 		pvcan_devices;
extern struct list_head 		pvcan_tx_queue;
extern spinlock_t				pvcan_txq_lock;
extern int 						pvcan_num_txq_elements;
extern struct task_struct 		*pvcan_tx_thread;
extern wait_queue_head_t 		pvcan_tx_thread_wakeup_event;

/**
 * Request type for the shared ring.
 * Requests are used to transmit can_frames to the backend that should be
 * transmitted on the CAN bus
 * @frame the can_frame to transmit
 */
struct pvcan_request {
	struct can_frame	frame;
};
typedef struct pvcan_request pvcan_request_t;

/**
 * Response type for the shared ring.
 * Responses are used to transmit can_frames that were received on 
 * the real (hardware) CAN bus to frontends
 */
struct pvcan_response {
	struct can_frame	frame;
};
typedef struct pvcan_response pvcan_response_t;

DEFINE_RING_TYPES(pvcan, struct pvcan_request, struct pvcan_response);

struct tx_list_element {
	struct list_head 	list;
	struct can_frame 	cf;
	uint32_t			sender;
	
};

struct backend_info {
	struct xenbus_device *dev;
	struct pvcandev *pvcan;

	/* This is the state that will be reflected in xenstore
	 */
	enum xenbus_state state;

	enum xenbus_state frontend_state;
};


struct pvcan_permission{
	struct list_head 	list;
	canid_t				from, to;
};

struct pvcandev {
	domid_t 					fe_domid;
	/* if used to as sender id, so we don't send incomming CAN messages back to the sender
	 * dev_id = 10*fe_domid + handle
	 */
	int 						dev_id;

	/* event channel for communication / commands from front end */
	evtchn_port_t 				comm_evtchn; 	
	/* interrupt number for events on comm_evtchn */
	unsigned int 				comm_irq;		

	/* Shared ring */
	struct pvcan_back_ring 		ring; 
	
	struct list_head			list;
	struct list_head			tx_permissions, rx_permissions;
	int 						tx_permission_all, rx_permission_all;
};


irqreturn_t pvcanback_comm_interrupt_handler(int irq, void *dev_id);

int xenpvcan_xenbus_init(void);
void xenpvcan_xenbus_fini(void);

struct pvcandev *pvcandev_alloc(domid_t fe_domid);
void pvcandev_free(struct pvcandev *pvcan);

void pvcan_handle_request(struct pvcandev *dev, pvcan_request_t *req);
void pvcan_send_response(struct pvcandev *dev, pvcan_response_t *rsp);
int pvcan_add_to_tx_queue(pvcan_request_t *req, int sender);
int pvcan_dev_has_tx_permission(struct pvcandev *dev, canid_t can_id);

#endif /* __XEN_PVCANBACK__COMMON_H__ */
