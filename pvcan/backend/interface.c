#include "common.h"


irqreturn_t pvcanback_comm_interrupt_handler(int irq, void *dev_id)
{
	struct pvcandev *dev;
	RING_IDX rq, cons;
	pvcan_request_t *req;
	int nr_consumed, more;
	struct backend_info *be = (struct backend_info*)dev_id;
	unsigned long flags;
	
	
	if(!be){
		PVCAN_DEBUG("interrupt! irq: %d\n",irq); 
		PVCAN_LOG("pvcanback_interrupt_handler: be is NULL\n");
		return IRQ_HANDLED;
	}
	
	dev = be->pvcan;
	
	if(!dev){
		PVCAN_ERR("pvcanback_comm_interrupt_handler: dev is NULL!\n");
		return IRQ_HANDLED;
	}

	nr_consumed = 0;
	local_irq_save(flags);
	
moretodo:
	rq = dev->ring.sring->req_prod;
    rmb(); /* Ensure we see queued responses up to 'rp'. */
    cons = dev->ring.req_cons;
   
	while ((cons != rq))
	{
		req = RING_GET_REQUEST(&dev->ring, cons);
		
		if(pvcan_dev_has_tx_permission(dev, req->frame.can_id))
			pvcan_add_to_tx_queue(req, dev->dev_id);
		
		dev->ring.req_cons = ++cons;
		if (dev->ring.req_cons != cons)
            /* We reentered, we must not continue here */
            break;
	}
	
	RING_FINAL_CHECK_FOR_REQUESTS(&dev->ring, more);
    if (more) 
		goto moretodo;
	
	local_irq_restore(flags);
	
	wake_up(&pvcan_tx_thread_wakeup_event);
	
	return IRQ_HANDLED;
}
void pvcan_send_response(struct pvcandev *dev, pvcan_response_t *rsp){
	RING_IDX i;
	pvcan_response_t *_rsp;
	int notify;
	
	i = dev->ring.rsp_prod_pvt;
	_rsp = RING_GET_RESPONSE(&dev->ring, i);
	//~ _rsp->ret = rsp->ret;
	dev->ring.rsp_prod_pvt = i + 1;

	wmb();
	RING_PUSH_RESPONSES_AND_CHECK_NOTIFY(&dev->ring, notify);
	if(notify) 
	{
		notify_remote_via_irq(dev->comm_irq);
	}
	else{
		//~ PVCAN_DEBUG("not notified\n");
	}
}


struct pvcandev *pvcandev_alloc(domid_t fe_domid)
{
	struct pvcandev *dev;
	
	dev = kzalloc(sizeof(struct pvcandev), GFP_KERNEL);
	if(!dev)
		return ERR_PTR(-ENOMEM);
	
	dev->fe_domid = fe_domid;
	
	// Allocate memory for any other things we need
	
	return dev;
}

void pvcandev_free(struct pvcandev *dev)
{
	// free any memory that has to be freed
	
	// free device memory
	kfree(dev);
}
