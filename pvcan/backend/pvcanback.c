#include "common.h"

#include <linux/rtnetlink.h>
#include <linux/sched/prio.h>

static char *interface = "can0";
module_param(interface, charp, 0000);
MODULE_PARM_DESC(interface, "name of CAN interface (net device) to use");


static int tx_queue_len = 100;
module_param(tx_queue_len, int, 0000);
MODULE_PARM_DESC(tx_queue_len, "number of TX  messages that can be buffered");

static int 			pvcan_rx_handler_registered = 0;
struct net_device 	*pvcan_netdev;

/* List of all registered pvcan devices */
struct list_head 	pvcan_devices;

/* List of can messages to be transmitted */
struct list_head 	pvcan_tx_queue;
spinlock_t			pvcan_txq_lock;
int 				pvcan_num_txq_elements = 0;

struct task_struct 		*pvcan_tx_thread;
wait_queue_head_t 		pvcan_tx_thread_wakeup_event;

/**
 * Send a can_frame to the frontend by adding a 
 * reponse to the shared ring of @dev
 * 
 * @cf: the can_frame to send
 * @dev: device to send the can_frame to
 */
static void pvcan_send_can_msg_to_frontend(struct can_frame *cf, struct pvcandev *dev)
{
	RING_IDX 			i;
	pvcan_response_t 	*rsp;
	int 				notify;
	
	i = dev->ring.rsp_prod_pvt;
	rsp = RING_GET_RESPONSE(&dev->ring, i);
	memcpy(&rsp->frame, cf, sizeof(*cf));
	dev->ring.rsp_prod_pvt = i + 1;

	wmb();
	RING_PUSH_RESPONSES_AND_CHECK_NOTIFY(&dev->ring, notify);
	if(notify) 
	{
		notify_remote_via_irq(dev->comm_irq);
	}
}

/**
 * pvcan_list_has_permission - Check if @can_id is on @list
 * 
 * @list: The list to check. Must be a list of struct pvcan_permission
 * @can_id: the can_id to check for
 * 
 * @return 1 if @can_id is on @list, 0 otherwise
 */
static int pvcan_list_has_permission(struct list_head *list, canid_t can_id)
{
	struct pvcan_permission *entry;
	// the id is max 29 bits --> ignore the highest 3 bits (flags) 
	can_id &= 0x1FFFFFFF;
	list_for_each_entry(entry, list, list){
		if(can_id >= entry->from && can_id <= entry->to){
			return 1;
		}
		
	}
	return 0;
}

/** 
 * pvcan_dev_has_tx_permission - Check if a device has permission to transmit a message with a specific can id
 * 
 * @dev: the device to check the permission for
 * @can_id: the id to check
 * 
 * @return 1 if device is allowed to transmit this can_id, 0 otherwise
 */
int pvcan_dev_has_tx_permission(struct pvcandev *dev, canid_t can_id )
{
	if(dev->tx_permission_all)
		return 1;
		
	return pvcan_list_has_permission(&dev->tx_permissions, can_id); 
}

/** 
 * pvcan_dev_has_tx_permission - Check if a device has permission to receive a message with a specific can id
 * 

 * @dev: the device to check the permission for
 * @can_id: the id to check
 * @return 1 if device is allowed to receive this can_id, 0 otherwise
 */
static int pvcan_dev_has_rx_permission(struct pvcandev *dev, canid_t can_id )
{
	if(dev->rx_permission_all)
		return 1;
		
	return pvcan_list_has_permission(&dev->rx_permissions, can_id); 
}

/**
 * pvcan_add_to_tx_queue - Add a new element to the tx queue
 * 
 * Elements are added so that the queue remains sorded by can_id
 * 
 * @req: tx request containing the can_frame to transmit
 * @sender: id of the device that wants to transmit this frame. 
 * 			The can_frame will not be transmitted to the device with this id. 
 */
int pvcan_add_to_tx_queue(pvcan_request_t *req, int sender)
{
	unsigned long 			flags;
	struct tx_list_element 	*e;
	struct tx_list_element	*entry = NULL, *last = NULL;
	
	if(pvcan_num_txq_elements >= tx_queue_len){
		pvcan_netdev->stats.tx_dropped++;
		//~ PVCAN_ERR("tx queue full, packet dropped\n");
		return -ETXTBSY;
	}
		
	e = kzalloc(sizeof(struct tx_list_element), GFP_ATOMIC);
	
	if(!e){
		PVCAN_ERR("could not allocate memory of for tx_list_element\n");
		return -ENOMEM;
	}
	
	memcpy(&e->cf, &req->frame, sizeof(struct can_frame));
	e->sender = sender;
		
	// lock tx queue so it won't break while we modify it
	spin_lock_irqsave(&pvcan_txq_lock, flags);
	
		
	// if tx queue is empty we just add the new element
	if(list_empty(&pvcan_tx_queue)){
		list_add(&e->list, &pvcan_tx_queue);
		goto done_added;
	}
	
	//~ PVCAN_DEBUG("txq not empty\n");
	
	// add entry at the correct position, so that tx queue 
	// is sorted by can_id in ascending order
	list_for_each_entry(entry, &pvcan_tx_queue, list){
		last = entry;
		if(req->frame.can_id < entry->cf.can_id){
			list_add_tail(&e->list, &entry->list);
			goto done_added;
		}
	}
	
	if(!last)
		goto done;
		
	// if we get here, there was no entry with a larger can_id,
	// so we have to add the new entry at the end of the list
	// (entry points to the last element of the tx queue)
	
	list_add(&e->list, &last->list);
	
done_added:
	pvcan_num_txq_elements++;
done:
	spin_unlock_irqrestore(&pvcan_txq_lock, flags);
	return 0;
} 

/**
 * pvcan_rx_handler - rx handler for the net device
 * 
 * This handler will be registered with the netdevice (global @interface).
 * can_frames are extracted from the skb and send to all pvcan devices 
 * that have RX permission for the can_id of that frame.
 * 
 * Does not modify the skb and returns RX_HANDLER_PASS so that the normal 
 * packet handling procedure of the network device is not changed. 
 */
static rx_handler_result_t pvcan_rx_handler(struct sk_buff **pskb)
{
	struct sk_buff 		*skb = *pskb;
	struct can_frame	*cf = (struct can_frame*)skb->data;
#ifdef PVCAN_PRINT_DEBUG
	int i = 0;
#endif
	struct pvcandev 	*dev;
	
	if(!pvcan_netdev || !skb)
		return RX_HANDLER_PASS;
		
	list_for_each_entry(dev, &pvcan_devices, list){
		if(pvcan_dev_has_rx_permission(dev, cf->can_id))
			pvcan_send_can_msg_to_frontend(cf, dev);
	}
	
#ifdef PVCAN_PRINT_DEBUGn
	printk(KERN_INFO "%s %8X [%d] ", pvcan_netdev->name, cf->can_id, cf->can_dlc);
	
	for(; i < cf->can_dlc; i++){
		printk(KERN_CONT "%2X ", cf->data[i]);
	}
	printk(KERN_CONT "\n");
#endif		


	return RX_HANDLER_PASS;
}

static struct tx_list_element* pvcan_get_next_tx_element(void)
{
	struct tx_list_element 	*e;
	unsigned long 			flags;
	spin_lock_irqsave(&pvcan_txq_lock, flags);
	e = list_first_entry_or_null(&pvcan_tx_queue, struct tx_list_element, list);
	spin_unlock_irqrestore(&pvcan_txq_lock, flags);
	return e;
}

static void pvcan_send_xen(struct tx_list_element *e)
{
	struct pvcandev *dev;
	
	list_for_each_entry(dev, &pvcan_devices, list){
		if(dev->dev_id == e->sender)
			continue;
		
		pvcan_send_can_msg_to_frontend(&e->cf, dev);
	}
}

static netdev_tx_t pvcan_send_hw(struct tx_list_element *e)
{
	struct can_frame 	*cf;
	struct netdev_queue *txq;
	struct sk_buff 		*skb;
	netdev_tx_t			ret;
	
	skb = alloc_can_err_skb(pvcan_netdev, &cf);
    
    if(unlikely(!skb)){
		PVCAN_ERR("could not alloc skb\n");
		return -ENOMEM;
	}
    
	memcpy(cf, &e->cf, sizeof(struct can_frame));
    
    txq = skb_get_tx_queue(pvcan_netdev, skb);
    if(!txq){
		PVCAN_ERR("could get tx_queue\n");
		dev_kfree_skb(skb);
		return -ENOMEM;
	}
    
    ret = pvcan_netdev->netdev_ops->ndo_start_xmit(skb, pvcan_netdev);
    if(ret != NETDEV_TX_OK){
		dev_kfree_skb(skb);
	}
	
	return ret;
}

static void pvcan_remove_tx_element(struct tx_list_element *element)
{
	unsigned long 	flags;
	
	spin_lock_irqsave(&pvcan_txq_lock, flags);
	list_del(&element->list);
	spin_unlock_irqrestore(&pvcan_txq_lock, flags);
	kfree(element);
	pvcan_num_txq_elements--;
}

static int pvcan_tx_loop(void *data)
{
	 struct tx_list_element 	*tx_element;
	 netdev_tx_t				ret;
	 //~ unsigned long 				ms, us, ns;
	 unsigned long 				wait_time_us;
	 int						timeout;
	 struct timespec			t_send_start, t_now;
	 
	 PVCAN_DEBUG("TX thread started (priority: %d)\n", pvcan_tx_thread->prio);
	 	 
	 ret = sched_setscheduler(pvcan_tx_thread, SCHED_FIFO, &(struct sched_param){.sched_priority = 49});
	 //~ ret = sched_setscheduler(pvcan_tx_thread, SCHED_FIFO, &(struct sched_param){.sched_priority = 49});
	 PVCAN_DEBUG("new priority: %d\n", pvcan_tx_thread->prio);
	 
		 
	 while(!kthread_should_stop()) {
      	wait_event_interruptible(pvcan_tx_thread_wakeup_event, (pvcan_num_txq_elements || kthread_should_stop()));
        
        if(kthread_should_stop()) 
			break;
		
		
		tx_element = pvcan_get_next_tx_element();
		
		if(!tx_element){
			PVCAN_DEBUG("no element in tx queue but pvcan_num_txq_elements is %d\n", pvcan_num_txq_elements--);
			pvcan_netdev->stats.tx_dropped++;
			continue;
		}
		
		//~ PVCAN_DEBUG("tx_element: cf with id 0x%X\n", tx_element->cf.can_id);
		pvcan_send_xen(tx_element);
		
		timeout = 1000;
		//~ getnstimeofday(&t_send_start);		
		do{
			ret = pvcan_send_hw(tx_element);
			if(ret != NETDEV_TX_OK){
				if(ret == -ENOMEM){
					pvcan_netdev->stats.tx_dropped++;
					PVCAN_ERR("packet dropped\n"); 
					break;
				}
			}
			
			//~ getnstimeofday(&t_now);
			//~ wait_time_us = (t_now.tv_nsec - t_send_start.tv_nsec) / 1000;
			
		}
		// try sending until we succeed (NETDEV_TX_OK) or timeout (10 ms)
		// TODO: make timeout configurable
		while((ret != NETDEV_TX_OK) && (timeout--));
		
		if(!timeout){
			PVCAN_DEBUG("timeout, packet dropped\n");
			pvcan_netdev->stats.tx_dropped++;
		}
		//~ getnstimeofday(&t2);
		
		pvcan_remove_tx_element(tx_element);
		//~ ms = (t2.tv_nsec - t1.tv_nsec) / (1000*1000);
		//~ ns = (t2.tv_nsec - t1.tv_nsec) - (ms * 1000*1000) - (us*1000);
		//~ PVCAN_DEBUG("TX thread: slept for: %lu,%03lu.%luus\n", ms,us,ns);
		//~ getnstimeofday(&now);
    }


    return 0;
}

void pvcan_handle_request(struct pvcandev *dev, pvcan_request_t *req)
{
	pvcan_response_t rsp;
	PVCAN_DEBUG("pvcan_handle_request: can_id %d\n", req->frame.can_id);
	
	pvcan_send_response(dev, &rsp);
}


// On loading this kernel module, we register as a frontend driver
static int __init pvcan_init(void)
{	
	int ret;
	
	printk(KERN_NOTICE "pvcan: init\n");
	if (!xen_domain()){	
		return -ENODEV;
	}
	
	// increase priority and scheduling policy of current thread 
	sched_setscheduler(current, SCHED_FIFO, &(struct sched_param){.sched_priority = 49});
	
	pvcan_netdev = dev_get_by_name(&init_net, interface);
	
	 if(!pvcan_netdev){
		printk(KERN_ERR "could not get device %s\n", interface);
		return -ENODEV;
	}
	
	rtnl_lock();
    ret = netdev_rx_handler_register(pvcan_netdev, pvcan_rx_handler, NULL);
	rtnl_unlock();
	
	if(ret){
		printk(KERN_ERR "could not register rx_handler for %s (error %d)\n", interface, ret);
		return ret;
	}
	pvcan_rx_handler_registered = 1;
	
	INIT_LIST_HEAD(&pvcan_devices);
	INIT_LIST_HEAD(&pvcan_tx_queue);
	spin_lock_init(&pvcan_txq_lock);
	init_waitqueue_head(&pvcan_tx_thread_wakeup_event);
	pvcan_tx_thread = kthread_run(pvcan_tx_loop, NULL, "pvcan_tx_thread");
	if(PTR_ERR(pvcan_tx_thread) == -ENOMEM){
		rtnl_lock();
		netdev_rx_handler_unregister(pvcan_netdev);
		rtnl_unlock();
		return -ENOMEM;
	}
	return xenpvcan_xenbus_init();
}
module_init(pvcan_init);

// ...and on unload we unregister
static void __exit pvcan_exit(void)
{
	printk(KERN_INFO "pvcan: exit\n");
	
	if(pvcan_tx_thread)
		kthread_stop(pvcan_tx_thread);
	
	if(pvcan_rx_handler_registered){
		rtnl_lock();
		netdev_rx_handler_unregister(pvcan_netdev);
		rtnl_unlock();
	}
	
	xenpvcan_xenbus_fini();	
}
module_exit(pvcan_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("David Tondorf (@tu-dortmund.de)");
MODULE_ALIAS("xen-backend:pvcan");
