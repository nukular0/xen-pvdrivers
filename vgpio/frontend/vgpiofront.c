#include "vgpiofront.h"
#include <linux/string.h>
#include <linux/gpio.h>
#include <linux/irq.h>
#include <linux/gpio/driver.h>
#include <linux/list_sort.h>


#define nVGPIO_PRINT_DEBUG
#ifdef VGPIO_PRINT_DEBUG
#define VGPIO_DEBUG(fmt,...) printk("vgpio:Debug("__FILE__":%d) " fmt, __LINE__, ##__VA_ARGS__)
#define VGPIO_DEBUG_MORE(fmt,...) printk(fmt, ##__VA_ARGS__)
#else
#define VGPIO_DEBUG(fmt,...)
#endif
#define VGPIO_LOG(fmt,...) printk(KERN_NOTICE "vgpio:Info " fmt, ##__VA_ARGS__)
#define VGPIO_ERR(fmt,...) printk(KERN_ERR "vgpio:Error("__FILE__":%d) " fmt, __LINE__, ##__VA_ARGS__)

void vgpiofront_set_state(struct xenbus_device *xbdev, 
									enum xenbus_state state);

static int vgpiofront_gpio_request(struct gpio_chip *gc, unsigned int offset);
static void vgpiofront_gpio_free(struct gpio_chip *gc, unsigned int offset);
static int vgpiofront_gpio_get(struct gpio_chip *gc, unsigned int offset);
static void vgpiofront_gpio_set(struct gpio_chip *gc, unsigned int offset, int
			     value);
static int vgpiofront_gpio_direction_input(struct gpio_chip *gc,
					unsigned int offset);
static int vgpiofront_gpio_direction_output(struct gpio_chip *gc,
					 unsigned int offset, int value);

struct vgpiofront_dev *dev;

static void print_requested_gpios(struct vgpiofront_dev *dev)
{
	struct list_head		*pos;
	struct requested_gpio	*entry;
	VGPIO_DEBUG("requested gpios: \n");
	list_for_each(pos, &dev->requested_gpios.list){
		entry = list_entry(pos, struct requested_gpio, list);
		VGPIO_DEBUG("pin: %d mode: %d\n", entry->pin, entry->mode);
	}
}

static irqreturn_t vgpiofront_dummy_irq_handler(int irq, void *dev_id)
{
	VGPIO_DEBUG("vgpiofront_dummy_irq_handler: irq %d, dev_id %p\n", irq, dev_id);
	return IRQ_HANDLED;
}

static irqreturn_t vgpiofront_sysfs_irq_handler(int irq, void *dev_id)
{
	struct irq_desc * desc;
	unsigned int gpio_irq = *((unsigned int*)dev_id);
	VGPIO_DEBUG("vgpiofront_sysfs_irq_handler: irq %d, gpio_irq %d\n", irq, gpio_irq);
	desc = irq_to_desc(gpio_irq);
	handle_simple_irq(desc);
	
	return IRQ_HANDLED;
}


static void vgpiofront_gpio_irq_enable(struct irq_data *d){
	VGPIO_DEBUG("vgpiofront_gpio_irq_enable: irq %d, hwirq: %ld\n", d->irq, d->hwirq);
}

static void vgpiofront_gpio_irq_shutdown(struct irq_data *d){
	VGPIO_DEBUG("vgpiofront_gpio_irq_shutdown: irq %d, hwirq: %ld\n", d->irq, d->hwirq);
	vgpio_free_irq(dev, d->irq);
}

static int vgpiofront_gpio_irq_type(struct irq_data *d, unsigned int flow_type){
	int ret;
	VGPIO_DEBUG("vgpiofront_gpio_irq_type: irq %d, hwirq: %ld, flow_type: %d\n", d->irq, d->hwirq, flow_type);
	VGPIO_DEBUG("mask: 0x%X, pin: %d, chip_data %p\n", d->mask++, d->irq, d->chip_data);
	if(!dev){
		VGPIO_ERR("vgpio device not initialized!\n");
		return -1;
	}
	VGPIO_DEBUG("requesting irq with data at %p\n", &d->irq);
	ret = vgpio_request_irq(dev, d->irq, vgpiofront_sysfs_irq_handler, &d->irq, flow_type);
	if(ret < 0){
		VGPIO_ERR("could not request irq for gpio %d\n", d->irq);
		return ret;
		
	}
	return 0;
}

static struct irq_chip vgpiofront_gpio_irqchip = {
	.name = "vgpiofront-irq-chip",
	.irq_enable = vgpiofront_gpio_irq_enable,
	.irq_shutdown = vgpiofront_gpio_irq_shutdown,
	.irq_set_type = vgpiofront_gpio_irq_type,
};


static void init_gpio_chip(struct gpio_chip *chip, unsigned base, unsigned ngpio, const char* label)
{
	if(!chip)
		return;
	chip->label = label;
	chip->base = base;
	chip->ngpio = ngpio;
	chip->request = vgpiofront_gpio_request;
	chip->free = vgpiofront_gpio_free;
	chip->get = vgpiofront_gpio_get;
	chip->set = vgpiofront_gpio_set;
	chip->direction_input = vgpiofront_gpio_direction_input;
	chip->direction_output = vgpiofront_gpio_direction_output;
	chip->owner = THIS_MODULE;
}

static int vgpiofront_add_gpio_chip(struct vgpiofront_dev *dev, 
								unsigned base, unsigned ngpio, 
								const char* label)
{
	int err;
	
	struct gpio_chip_list *chip_list_item;
	chip_list_item = kzalloc(sizeof(struct gpio_chip_list), GFP_KERNEL);
	if(!chip_list_item){
		return -ENOMEM;					
	}
	
	chip_list_item->chip = kzalloc(sizeof(struct gpio_chip), GFP_KERNEL);
	if(!chip_list_item->chip){
		kfree(chip_list_item);
		return -ENOMEM;
	}
	
	init_gpio_chip(chip_list_item->chip, base, ngpio, label);
	chip_list_item->chip->parent = &dev->xbdev->dev;
	
	
	err = gpiochip_add(chip_list_item->chip);
	if(err){
		VGPIO_DEBUG("adding gpio_chip (base %d, ngpio %d) failed: %d\n", base, ngpio, err);
		kfree(chip_list_item->chip);
		kfree(chip_list_item);
		return err;
	}
	

	VGPIO_DEBUG("gpiochip_irqchip_add()\n");
	err = gpiochip_irqchip_add(chip_list_item->chip, &vgpiofront_gpio_irqchip, base,
				   handle_simple_irq, IRQ_TYPE_NONE);
	if (err) {
		VGPIO_ERR("failed to add irqchip\n");
		goto fail;
	}


	VGPIO_DEBUG("gpiochip_set_chained_irqchip()\n");
	gpiochip_set_chained_irqchip(chip_list_item->chip, &vgpiofront_gpio_irqchip, base,
				     NULL);
	
	list_add(&chip_list_item->list, &dev->gpio_chips.list);
	
	return 0;
	
fail:
	gpiochip_remove(chip_list_item->chip);
	kfree(chip_list_item->chip);
	kfree(chip_list_item);
	return err;
	
}

/* Creates gpio_chips for the vailable gpios of dev
 * and adds these to the gpio subsystem
 */
static int create_gpio_chips(struct vgpiofront_dev *dev){
	struct available_gpio_list *item;
	struct list_head *pos;
	unsigned base, ngpio, last;
	int err;
	base = ngpio = last = 0;
	
	list_for_each(pos, &dev->available_gpios.list){
		item = list_entry(pos, struct available_gpio_list, list);
		VGPIO_DEBUG("pin %d, base %d, ngpio %d, last %d\n", item->pin, base, ngpio, last);
		// if ngpio is 0 we are at the beginning of a new range
		if(!ngpio){
		 base = item->pin;
		 ngpio = 1;
		 last = item->pin;
		}
		// if ngpio is > 0, we are in a range and need to check if
		// the current element is still in the same range as the last pin
		else{
			// still in range
			if(item->pin == (last+1)){
				ngpio++;
				last = item->pin;
			}
			else{
				VGPIO_DEBUG("found range: base %d, ngpio %d (%d - %d)\n", base, ngpio, base, base+ngpio-1);
				// not in range anymore:
				// create new gpio_chip for range and add it to devs list
				err = vgpiofront_add_gpio_chip(dev, base, ngpio, "VGPIO Frontend GPIO chip");
				if(err)
					return err;
				
				// start new range
				base = item->pin;
				ngpio = 1;				
				last = item->pin;
			}
		}
	}

	// after we went through the whole list of gpios
	// close the last range
	// make sure there was at least one range
	if(!ngpio)
		return -ENODEV;
		
	VGPIO_DEBUG("final range: base %d, ngpio %d (%d - %d)\n", base, ngpio, base, base+ngpio-1);
	return vgpiofront_add_gpio_chip(dev, base, ngpio, "VGPIO Frontend GPIO chip");
} 

/* Returns 1 if gpio is in dev->available_gpio_list */
static int gpio_available(struct vgpiofront_dev *dev, unsigned gpio)
{
	struct available_gpio_list *item;
	struct list_head *pos;
	
	list_for_each(pos, &dev->available_gpios.list){
		item = list_entry(pos, struct available_gpio_list, list);
		if(item->pin == gpio)
			return 1;
	}
	
	return 0;
}

/* parse a comma separated string of gpio numbers (unsigned ints)
 * new gpios will be added to dev->available_gpio_list
 * dev->num_available_gpios will be increased by the number of gpios added
 */
static void parse_and_add_gpio_string(char* str, struct vgpiofront_dev *dev)
{
	char *p;
	int err;
	unsigned _pin;
	struct available_gpio_list *new_item;
	
	if(!str || !dev){
		VGPIO_DEBUG("error str or dev NULL: *str: %p, *dev: %p\n", str, dev);
		return;
	}
	
	while ( (p = strsep(&str, ","))  != NULL){
		while (*p == ' ') p++;
		// get the gpio number
		err = sscanf(p, "%u", &_pin);
		if(err != 1){
			VGPIO_DEBUG("parse_gpio_string: error (%d)\n",err);
		}
		// find out if gpio ins already in list
		if(!gpio_available(dev, _pin)){
			// add it to list
			new_item = kzalloc(sizeof(struct available_gpio_list), GFP_KERNEL);
			new_item->pin = _pin;
			list_add(&new_item->list, &dev->available_gpios.list);
			dev->num_available_gpios++;			
			VGPIO_DEBUG("added gpio %d to list (%d total)\n", _pin, dev->num_available_gpios);
		}
		else{
			VGPIO_DEBUG("gpio %d already in list)\n", _pin);
		}
	}
}

/*
 * Used to sort available_gpio_list 
 * Returns -1 if the pin value of a is smaller than the one of b
 */
static int gpio_compare(void *priv, struct list_head *a,
			  struct list_head *b)
{
	struct available_gpio_list *element_a, *element_b;
	
	element_a = list_entry(a, struct available_gpio_list, list); 
	element_b = list_entry(b, struct available_gpio_list, list); 
	
	return (element_a->pin < element_b->pin) ? -1 : 1;
}	  
		

void test(struct vgpiofront_dev *gpio_dev){
	int ret;
	unsigned int gpioLED = 403;       		// Linux 403 = Up 37
	unsigned int gpioButton = 404;       	// Linux 464 = Up 31
	unsigned int gpioIn = 464;       		// Linux 464 = Up 27
	unsigned int gpioOut = 430;				// Linux 430 = Up 29
	unsigned int gpio431 = 431;
	unsigned int gpio432 = 432;
	unsigned int gpio433 = 433;
	

	VGPIO_DEBUG(" ####### test() #######\n");
	
	ret = _vgpiofront_gpio_request(gpio_dev, gpioLED, NULL);
	VGPIO_DEBUG("gpio_request (pin %d): %d\n",gpioLED, ret);
	
	ret = _vgpiofront_gpio_request(gpio_dev, gpioButton, NULL);
	VGPIO_DEBUG("button gpio_request (pin %d): %d\n",gpioButton, ret);
	
	ret = _vgpiofront_gpio_request(gpio_dev, gpioIn, NULL);
	VGPIO_DEBUG("button gpio_request (pin %d): %d\n",gpioIn, ret);
	
	ret = _vgpiofront_gpio_request(gpio_dev, gpioOut, NULL);
	VGPIO_DEBUG("button gpio_request (pin %d): %d\n",gpioOut, ret);
	
	ret = _vgpiofront_direction_input(gpio_dev, gpioButton);
	VGPIO_DEBUG("gpio_direction_input (pin %d): %d\n",gpioButton, ret);
	
	ret = _vgpiofront_direction_input(gpio_dev, gpioIn);
	VGPIO_DEBUG("gpio_direction_input (pin %d): %d\n",gpioIn, ret);
	
	ret = _vgpiofront_direction_output(gpio_dev, gpioOut, 1);
	VGPIO_DEBUG("gpio_direction_output (pin %d): %d\n",gpioOut, ret);
	
	ret = vgpio_request_irq(gpio_dev, gpioButton, vgpiofront_dummy_irq_handler, NULL, IRQF_TRIGGER_FALLING);
	VGPIO_DEBUG("irq request (pin %d): %d\n",gpioButton, ret);
	
	ret = vgpio_request_irq(gpio_dev, gpioIn, vgpiofront_dummy_irq_handler, NULL, IRQF_TRIGGER_FALLING);
	VGPIO_DEBUG("irq request (pin %d): %d\n",gpioIn, ret);
	
	ret = _vgpiofront_gpio_request(gpio_dev, gpio431, NULL);
	ret = _vgpiofront_gpio_request(gpio_dev, gpio432, NULL);
	ret = _vgpiofront_gpio_request(gpio_dev, gpio433, NULL);
	
	vgpio_request_irq(gpio_dev, gpio433, vgpiofront_dummy_irq_handler, NULL,IRQF_TRIGGER_FALLING);
	vgpio_request_irq(gpio_dev, gpio432, vgpiofront_dummy_irq_handler, NULL,IRQF_TRIGGER_FALLING);
	vgpio_request_irq(gpio_dev, gpio431, vgpiofront_dummy_irq_handler, NULL,IRQF_TRIGGER_FALLING);
	
	vgpio_free_irq(gpio_dev, gpioButton);
	vgpio_free_irq(gpio_dev, gpioIn);
	_vgpiofront_gpio_free(gpio_dev, gpioLED);
	_vgpiofront_gpio_free(gpio_dev, gpioButton);
	_vgpiofront_gpio_free(gpio_dev, gpioIn);
	_vgpiofront_gpio_free(gpio_dev, gpioOut);
	_vgpiofront_gpio_free(gpio_dev, gpio431);
	_vgpiofront_gpio_free(gpio_dev, gpio432);
	_vgpiofront_gpio_free(gpio_dev, gpio433);
	//~ shutdown_vgpiofront(gpio_dev->xbdev);
}

int vgpio_request_irq(struct vgpiofront_dev *dev, unsigned gpio, irq_handler_t handler, void* handler_data, unsigned trigger)
{
	struct pin_irq_list 	*pin_irq;
	struct requested_gpio 	req_gpio;
	evtchn_port_t 			_irq_evtchn;
	int 					err;
	unsigned 				_irq;
	vgpio_request_t 		req;
	
	if(!dev){
		VGPIO_ERR("gpio_request_irq: dev must not be NULL\n");
		err = -1;
		goto error;
	}
	
	if(!handler){
		VGPIO_ERR("gpio_request_irq: handler must not be NULL\n");
		err = -1;
		goto error;
	}
	
	
	/* Create event channel for communication with backend */
	err = xenbus_alloc_evtchn(dev->xbdev, &_irq_evtchn);
	if(err < 0) {
	  VGPIO_ERR("Unable to allocate comm_event channel\n");
	  goto error;
	}
	VGPIO_DEBUG("irq_event channel is %lu\n", (unsigned long) _irq_evtchn);
	
	err = bind_evtchn_to_irqhandler(_irq_evtchn,
					handler,
					0, "vgpiofront", handler_data);
	if (err < 0){
		VGPIO_ERR("Could not bind evtchn to irq handler (error %d)\n", err);
		goto error_postalloc;
	}
	_irq = err;
	
	VGPIO_DEBUG("irq for pin %d is %d\n", gpio, _irq);

	req.cmd = CMD_GPIO_REQUEST_IRQ;
	req.pin = gpio;
	req.val = (unsigned int)_irq_evtchn;
	req.irq_edge = trigger;
	
	/* alloc memory for irq_list entry 
	 * do it before vgpiofront_send_request(), so that if it should fail,
	 * wo do not have to undo the irq_request in the backend */
	pin_irq = (struct pin_irq_list*)kzalloc(sizeof(struct pin_irq_list), GFP_KERNEL);
	if(!pin_irq){
		VGPIO_ERR("gpio_request_irq: malloc() for pin_irq failed\n");
		err = -ENOMEM;
		goto error;
	}
	
	/* Request IRQ for pin from backend */
	if((err = vgpiofront_send_request(dev, req))){
		VGPIO_ERR("gpio_request_irq: backend could not request IRQ, error: %d\n", err);
		goto error_postbind;
	}
	
	/* Add irq to dev's irq_list */
	pin_irq->pin = gpio;
	pin_irq->handler_data = handler_data;
	pin_irq->port = _irq_evtchn;
	pin_irq->irq = _irq;
	list_add_tail(&(pin_irq->list), &(dev->irq_list.list));
		
	/* Add to dev's requested gpio list */
	req_gpio.mode = MODE_IRQ;
	req_gpio.pin = gpio;
	req_gpio.trigger = trigger;
	req_gpio.handler = handler;
	req_gpio.handler_data = handler_data;
	
	err = add_requested_gpio(dev, &req_gpio);
	
	if(err){
		VGPIO_ERR("gpio_request_irq: error adding to requested_gpios: %d\n", err);
		vgpio_free_irq(dev, gpio);
		return err;
	}	
	
	return 0;

error_postbind:
	kfree(pin_irq);
	unbind_from_irqhandler(_irq, handler_data);
error_postalloc:
	xenbus_free_evtchn(dev->xbdev, _irq_evtchn);
error:
	return err;
}

void vgpio_free_irq(struct vgpiofront_dev *dev, unsigned gpio)
{
	struct pin_irq_list *item;
	struct requested_gpio req_gpio;
	struct list_head *pos;
	evtchn_port_t _port;
	vgpio_request_t req;
	
	item = NULL;
	_port = 0;
	
	if(!dev){
		VGPIO_ERR("gpio_request_irq: dev must not be NULL\n");
		return;
	}
	
	
	// get event channel port for gpio from dev's irq_list
	list_for_each(pos, &dev->irq_list.list){
		item = list_entry(pos, struct pin_irq_list, list);
		if(item->pin == gpio){
			_port = item->port;
			break;
		}
	}
	
	// check if we found the gpio in our list
	if(!_port){
		VGPIO_DEBUG("gpio %d not in irq_list\n", gpio);
		return;
	}
	
	// Send free request with pin number to backend
	req.cmd = CMD_GPIO_FREE_IRQ;
	req.pin = gpio;
	req.val = 0;
	vgpiofront_send_request(dev, req);
		
	VGPIO_DEBUG("vgpio_free_irq(): unbind_from_irqhandler (irq %d / %p)\n",item->irq, item->handler_data);
	unbind_from_irqhandler(item->irq, item->handler_data);
	
	list_del(pos);
	kfree(item);
	
	// IRQ pins are input pins, so if we free the irq, it is now just an 
	// input pin again
	req_gpio.mode = MODE_INPUT;
	req_gpio.pin = gpio;
	req_gpio.val = 0;
	req_gpio.trigger = 0;
	req_gpio.handler = NULL;
	req_gpio.handler_data = NULL;
	add_requested_gpio(dev, &req_gpio);
}

int _vgpiofront_gpio_request(struct vgpiofront_dev *dev, unsigned gpio, const char *label)
{
	int ret;
	struct requested_gpio req_gpio;
	
	vgpio_request_t req = {
		.cmd = CMD_GPIO_REQUEST,
		.pin = gpio,
	};
	ret = vgpiofront_send_request(dev, req);
	
	if(ret)
		return ret;
		
	req_gpio.mode = MODE_REQUESTED;
	req_gpio.pin = gpio;
	return add_requested_gpio(dev, &req_gpio);
	
	
}
void _vgpiofront_gpio_free(struct vgpiofront_dev *dev, unsigned gpio)
{
	struct requested_gpio 	*entry;
	struct list_head	  	*pos, *n;
		
	vgpio_request_t req = {
		.cmd = CMD_GPIO_FREE,
		.pin = gpio,
	};
	vgpiofront_send_request(dev, req);
	
	entry = get_requested_gpio(dev, gpio);
	if(!entry)
		return;
	
	list_del(&entry->list);
	kfree(entry);
}

int _vgpiofront_direction_input(struct vgpiofront_dev *dev, unsigned gpio)
{
	int ret;
	struct requested_gpio req_gpio;
	
	vgpio_request_t req = {
		.cmd = CMD_GPIO_DIRECTION_INPUT,
		.pin = gpio,
	};
	ret = vgpiofront_send_request(dev, req);
	
	if(ret)
		return ret;
		
	req_gpio.mode = MODE_INPUT;
	req_gpio.pin = gpio;
	return add_requested_gpio(dev, &req_gpio);
}

int _vgpiofront_direction_output(struct vgpiofront_dev *dev, unsigned gpio, int value)
{
	int ret;
	struct requested_gpio req_gpio;
	
	vgpio_request_t req = {
		.cmd = CMD_GPIO_DIRECTION_OUTPUT,
		.pin = gpio,
		.val = value,
	};
	ret = vgpiofront_send_request(dev, req);
	
	if(ret)
		return ret;
		
	req_gpio.mode = MODE_OUTPUT;
	req_gpio.pin = gpio;
	req_gpio.val = value;
	
	return add_requested_gpio(dev, &req_gpio);
}

int _vgpiofront_get_value(struct vgpiofront_dev *dev, unsigned gpio)
{
	vgpio_request_t req = {
		.cmd = CMD_GPIO_GET_VALUE,
		.pin = gpio,
	};
	return vgpiofront_send_request(dev, req);
}

void _vgpiofront_set_value(struct vgpiofront_dev *dev, unsigned gpio, int value)
{
	vgpio_request_t req = {
		.cmd = CMD_GPIO_SET_VALUE,
		.pin = gpio,
		.val = value,
	};
	vgpiofront_send_request(dev, req);
}

int vgpiofront_send_request(struct vgpiofront_dev* dev, vgpio_request_t req){
	RING_IDX i;
	vgpio_request_t *_req;
	int notify;
	int _ret;
	
	//~ VGPIOFRONT_LOG("sending...");
	if(dev->state == XenbusStateConnected){
		i = dev->ring.req_prod_pvt;
		_req = RING_GET_REQUEST(&dev->ring, i);
		memcpy(_req, &req, sizeof(req));
		dev->ring.req_prod_pvt = i + 1;

		wmb();
		RING_PUSH_REQUESTS_AND_CHECK_NOTIFY(&dev->ring, notify);
		if(notify) 
		{
			VGPIO_DEBUG("sending request: cmd %d pin %d val %d edge %d\n", req.cmd, req.pin, req.val, req.irq_edge);
			notify_remote_via_irq(dev->comm_evtchn_irq);    
		}
		down(&dev->sem);
	}
	else{
		VGPIO_DEBUG("error: not connected");
		return -1;
	}
	_ret = dev->last_response.ret;
	VGPIO_DEBUG("got response: %d\n", _ret);
	dev->last_response.ret = INVALID_RESPONSE;
	return _ret;
}

//~ static irqreturn_t vgpiofront_hw_irq_handler(int irq, void *dev_id){
	//~ // dev_id is a pointer to the handler that was registered for this event/irq
	//~ void (*handler)(int irq, void *dev_id) = (void(*)(int,void*))dev_id;
	//~ handler(irq, dev_id);
	//~ return IRQ_HANDLED;
//~ }

static irqreturn_t vgpiofront_comm_interrupt(int irq, void *dev_id)
{
	RING_IDX rp, cons;
	vgpio_response_t *rsp;
	int nr_consumed, more;
	struct vgpiofront_dev *dev = (struct vgpiofront_dev*) dev_id;

moretodo:
	rp = dev->ring.sring->rsp_prod;
    rmb(); /* Ensure we see queued responses up to 'rp'. */
    cons = dev->ring.rsp_cons;
   
	while ((cons != rp))
    {
		rsp = RING_GET_RESPONSE(&dev->ring, cons);
		nr_consumed++;
		dev->last_response = *rsp;
		dev->ring.rsp_cons = ++cons;   
        if (dev->ring.rsp_cons != cons)
            /* We reentered, we must not continue here */
            break;
			
	}
	
	RING_FINAL_CHECK_FOR_RESPONSES(&dev->ring, more);
    if (more) goto moretodo;    
	
	up(&dev->sem);   
	
	return IRQ_HANDLED;
}

static int publish_xenbus(struct vgpiofront_dev* dev) {
   struct xenbus_transaction xbt;
   int err;
   /* Write the grant reference and event channel to xenstore */
again:
   if((err = xenbus_transaction_start(&xbt))) {
      VGPIO_ERR("Unable to start xenbus transaction, error was %d\n", err);
      
      return -1;
   }

   if((err = xenbus_printf(xbt, dev->nodename, "ring-ref", "%u", (unsigned int) dev->ring_ref))) {
      VGPIO_ERR("Unable to write %s/ring-ref, error was %d\n", dev->nodename, err);
      goto abort_transaction;
   }

   if((err = xenbus_printf(xbt, dev->nodename, "event-channel", "%u", (unsigned int) dev->comm_evtchn))) {
      VGPIO_ERR("Unable to write %s/event-channel, error was %d\n", dev->nodename, err);
      goto abort_transaction;
   }

   if((err = xenbus_transaction_end(xbt, 0))) {
      VGPIO_ERR("Unable to complete xenbus transaction, error was %d\n", err);
      if(err == -EAGAIN){
		goto again;
	  }
      return -1;
   }
  
   return 0;
abort_transaction:
   xenbus_transaction_end(xbt, 1);
   return -1;
}


static int vgpiofront_connect(struct xenbus_device *xbdev)
{
	int err;
	struct vgpio_sring *sring;
	struct vgpiofront_dev* dev;
   
	dev = dev_get_drvdata(&xbdev->dev);
   
	/* Create shared page/ring */
	dev->ring_page = get_zeroed_page(GFP_KERNEL);
	sring = (struct vgpio_sring *)dev->ring_page;
	if(sring == NULL) {
	  VGPIO_ERR("Unable to allocate page for shared memory\n");
	  goto error;	
	}

	/* Initialize shared ring in shared page */
	SHARED_RING_INIT(sring);
	FRONT_RING_INIT(&dev->ring, sring, XEN_PAGE_SIZE);
	
	err = xenbus_grant_ring(xbdev, sring, 1, &dev->ring_ref);
	if(err < 0){
		VGPIO_ERR("error granting access to ring: %d\n", err);
		goto error_postalloc;
	}
	
	VGPIO_DEBUG("grant ref is %lu\n", (unsigned long) dev->ring_ref);

	/* Create event channel for communication with backend */
	err = xenbus_alloc_evtchn(xbdev, &dev->comm_evtchn);
	if(err < 0) {
	  VGPIO_ERR("Unable to allocate comm_event channel\n");
	  goto error_postmap;
	}
	VGPIO_DEBUG("comm_event channel is %lu\n", (unsigned long) dev->comm_evtchn);
	
	err = bind_evtchn_to_irqhandler(dev->comm_evtchn,
					vgpiofront_comm_interrupt,
					0, "vgpiofront", dev);
	if (err < 0){
		VGPIO_ERR("Could not bind evtchn to irq handler (error %d)\n", err);
		goto error_postbind;
	}
	dev->comm_evtchn_irq = err;
	//~ unmask_evtchn(dev->comm_evtchn);
	
	/* Write the entries to xenstore */
	if(publish_xenbus(dev)) {
	  goto error_postevtchn;
	}
	

	return 0;
error_postevtchn:
      unbind_from_irqhandler(dev->comm_evtchn_irq, dev);
error_postbind:
	  xenbus_free_evtchn(xbdev, dev->comm_evtchn);
	  dev->comm_evtchn = 0;
error_postmap:
      gnttab_end_foreign_access_ref(dev->ring_ref, 0);
error_postalloc:
      free_page((unsigned long)sring);
error:
   return -1;
}

void free_vgpiofront(struct xenbus_device *xbdev){
	struct vgpiofront_dev 		*dev;
	struct pin_irq_list 		*item;
	struct available_gpio_list 	*gitem;
	struct gpio_chip_list 		*citem;
	struct requested_gpio 		*ritem;
	struct list_head 			*pos, *q;
	
	dev = dev_get_drvdata(&xbdev->dev);
	if(!dev) return;
	
	if(dev->nodename) 	kfree(dev->nodename);
	if(dev->bepath)		kfree(dev->bepath);
	
	// free communication event channel
	if(dev->comm_evtchn){
		VGPIO_DEBUG("unbind_from_irqhandler (irq %d)\n", dev->comm_evtchn_irq);
		unbind_from_irqhandler(dev->comm_evtchn_irq, dev);
		dev->comm_evtchn = 0;
	}
	
	// free shared ring and memory
	if(dev->ring_ref){
		VGPIO_DEBUG("gnttab_free_grant_reference (ring_ref %d)\n", dev->ring_ref);
		gnttab_free_grant_reference(dev->ring_ref);
		VGPIO_DEBUG("gnttab_end_foreign_access (ring_ref %d)\n", dev->ring_ref);
		gnttab_end_foreign_access(dev->ring_ref, 0, dev->ring_page);
		dev->ring_ref = 0;
		dev->ring_page = 0;
	}
	
	// free all pin irqs and irq list memory
	// TODO: request pins to be freed in the backend as well
	VGPIO_DEBUG("freeing pin irqs\n");
	list_for_each_safe(pos, q, &dev->irq_list.list){
		item = list_entry(pos, struct pin_irq_list, list);
		unbind_from_irqhandler(item->irq, item->handler_data);
		list_del(pos);
		kfree(item);
	}
	
	// free available_gpio_list memory
	VGPIO_DEBUG("freeing available_gpio_list\n");
	list_for_each_safe(pos, q, &dev->available_gpios.list){
		gitem = list_entry(pos, struct available_gpio_list, list);
		list_del(pos);
		kfree(gitem);
	}
	
	// remove gpio_chips and free list memory
	VGPIO_DEBUG("freeing gpio_chips\n");
	list_for_each_safe(pos, q, &dev->gpio_chips.list){
		citem = list_entry(pos, struct gpio_chip_list, list);
		if(citem->chip)
			gpiochip_remove(citem->chip);
		list_del(pos);
		kfree(citem);
	}
	
	// remove elements from requested_gpios and free list memory
	list_for_each_safe(pos, q, &dev->requested_gpios.list){
		ritem = list_entry(pos, struct requested_gpio, list);
		list_del(pos);
		kfree(ritem);
	}
	
	// free device memory
	kfree(dev);
	
	vgpiofront_set_state(xbdev, XenbusStateClosed);	
}

void shutdown_vgpiofront(struct xenbus_device *xbdev){
	
	struct vgpiofront_dev *dev;
	int err;
	if(xbdev->state != XenbusStateConnected) return;
	dev = dev_get_drvdata(&xbdev->dev);
	if(!dev) return;
	
	vgpiofront_set_state(xbdev, XenbusStateClosing);	
	
	// clean up xenstore
	err = xenbus_rm(XBT_NIL, dev->nodename, "ring-ref");
	if(err){
		VGPIO_ERR("removing ring-ref (%d)\n", err);
	}
	err = xenbus_rm(XBT_NIL, dev->nodename, "event-channel");
	if(err){
		VGPIO_ERR("removing event-channel (%d)\n", err);
	}
		
	free_vgpiofront(xbdev);
	
}

int vgpio_setup(struct vgpiofront_dev *dev)
{
	struct list_head 		*pos;
	struct requested_gpio	*entry;
	int						ret;
	
	if(!dev){
		VGPIO_ERR("vgpio_setup: no device\n");
		return -ENODEV;
	}
	
	VGPIO_DEBUG("vgpio_setup()\n");
		
	list_for_each(pos, &dev->requested_gpios.list){
		entry = list_entry(pos, struct requested_gpio, list);
		VGPIO_DEBUG("pin: %d\n", entry->pin);
		
		ret = _vgpiofront_gpio_request(dev, entry->pin, NULL);
		
		if(ret){
			VGPIO_ERR("vgpio_setup: error requesting gpio %d: %d\n", entry->pin, ret);
			return ret;
		}
		
		switch(entry->mode){
			case MODE_REQUESTED:
				VGPIO_DEBUG("   MODE_REQUESTED\n");
				break;
			case MODE_OUTPUT:
				VGPIO_DEBUG("   MODE_OUTPUT\n");
				ret = _vgpiofront_direction_output(dev, entry->pin, entry->val);
				if(ret){
					VGPIO_ERR("vgpio_setup: error setting direction to output of gpio %d: %d\n", entry->pin, ret);
					return ret;
				}
				break;
			case MODE_IRQ:
			case MODE_INPUT:
				VGPIO_DEBUG("   MODE_IN\n");
				ret = _vgpiofront_direction_input(dev, entry->pin);
				if(ret){
					VGPIO_ERR("vgpio_setup: error setting direction to out of gpio %d: %d\n", entry->pin, ret);
					return ret;
				}
				
				if(entry->mode == MODE_INPUT)
					break;
					
				ret = vgpio_request_irq(dev, entry->pin, entry->handler, entry->handler_data, entry->trigger);
				if(ret){
					VGPIO_ERR("vgpio_setup: error requesting irq for gpio %d: %d\n", entry->pin, ret);
					return ret;
				}
				break;
			default:
					VGPIO_ERR("vgpio_setup: invalid mode for pin %d (%d)\n", entry->pin, entry->mode);
					return -EINVAL;
		}
	}	
	VGPIO_DEBUG("   done!\n");
	return 0;
}

struct requested_gpio* get_requested_gpio(struct vgpiofront_dev *dev, unsigned pin)
{
	struct list_head 		*pos;
	
	struct requested_gpio	*entry;
	int						ret;
	
	if(!dev){
		VGPIO_ERR("get_requested_gpio: no device\n");
		return NULL;
	}
		
	list_for_each(pos, &dev->requested_gpios.list){
		entry = list_entry(pos, struct requested_gpio, list);
		if(entry->pin == pin)
			return entry;
	}
	
	return NULL;
}

int add_requested_gpio(struct vgpiofront_dev *dev, struct requested_gpio *req)
{
	struct requested_gpio	*entry, *new;
	
	if(!dev){
		VGPIO_ERR("add_requested_gpio: no device\n");
		return -ENODEV;
	}
	
	entry = get_requested_gpio(dev, req->pin);
	if(entry){
		VGPIO_DEBUG("add_requested_gpio: pin %d exists\n", req->pin);
		entry->pin = req->pin;
		entry->val = req->val;
		entry->mode = req->mode;
		entry->trigger = req->trigger;
		entry->handler = req->handler;
		entry->handler_data = req->handler_data;
	}
	else{
		new = kzalloc(sizeof(struct requested_gpio), GFP_KERNEL);
		if(!new){
			VGPIO_ERR("add_requested_gpio: error allocating memory for new\n");
			return -ENOMEM;
		}
		new->pin = req->pin;
		new->val = req->val;
		new->mode = req->mode;
		new->trigger = req->trigger;
		new->handler = req->handler;
		new->handler_data = req->handler_data;
		list_add(&new->list, &dev->requested_gpios.list);
		VGPIO_DEBUG("add_requested_gpio: pin %d added\n", req->pin);
	}
	
	print_requested_gpios(dev);
	
	return 0;
	
}

struct vgpiofront_dev* init_vgpiofront(struct xenbus_device *xbdev, const char* _nodename)
{
	struct vgpiofront_dev* dev;
	unsigned int val;
	const char* nodename;
	int err;
	char tmpstr[512];

	VGPIO_DEBUG("============= Init VGPIO Front ================\n");

	dev = kzalloc(sizeof(*dev), GFP_KERNEL);
	
	if(!dev){
		VGPIO_ERR("allocating memory for dev\n");
		return NULL;
	}

	dev->last_response.ret = INVALID_RESPONSE;
	dev->xbdev = xbdev;

	/* Init semaphore */
	sema_init(&dev->sem, 0);
	/* Set node name */
	nodename = _nodename ? _nodename : "device/vgpio/0";
	dev->nodename = kstrdup(nodename, GFP_KERNEL);
  
	/* Init lists */
	INIT_LIST_HEAD(&dev->irq_list.list);
	INIT_LIST_HEAD(&dev->available_gpios.list);
	INIT_LIST_HEAD(&dev->gpio_chips.list);
	INIT_LIST_HEAD(&dev->requested_gpios.list);

	/* Get backend domid */
	err = xenbus_scanf(XBT_NIL, dev->nodename, "backend-id", "%u", &val);
	if(err != 1) {
	  VGPIO_ERR("Unable to read backend-id during vgpiofront initialization! error = %d\n", err);
	  goto error;
	}
	dev->bedomid = val;
	VGPIO_LOG("backend dom-id is %d\n", val);

	/* Get backend xenstore path */
	err = xenbus_scanf(XBT_NIL, dev->nodename, "backend", "%s", tmpstr);
	if(err != 1) {
	  VGPIO_ERR("Unable to read backend during vgpiofront initialization! error = %d\n", err);
	  goto error;
	}
	dev->bepath = kstrdup((const char*)tmpstr, GFP_KERNEL);
	VGPIO_LOG("backend path is %s\n", dev->bepath);

	/* Read GPIOs from xenstore and add to available_gpio_list */
	err = xenbus_scanf(XBT_NIL, dev->nodename, "output-pins", "%s", tmpstr);
	if(err != 1) {
	  VGPIO_ERR("Unable to read output-pins during vgpiofront initialization! error = %d\n", err);
	  goto error;
	}
	if(!strncmp(tmpstr, "all", sizeof(tmpstr))){
		VGPIO_ERR("option 'all' not supported\n");
	}
	else{
		parse_and_add_gpio_string(tmpstr, dev);		
	}
	
	err = xenbus_scanf(XBT_NIL, dev->nodename, "input-pins", "%s", tmpstr);
	if(err != 1) {
	  VGPIO_ERR("Unable to read input-pins during vgpiofront initialization! error = %d\n", err);
	  goto error;
	}
	if(!strncmp(tmpstr, "all", sizeof(tmpstr))){
		VGPIO_ERR("option 'all' not supported\n");
	}
	else{
		parse_and_add_gpio_string(tmpstr, dev);		
	}
	
	err = xenbus_scanf(XBT_NIL, dev->nodename, "irq-pins", "%s", tmpstr);
	if(err != 1) {
	  VGPIO_ERR("Unable to read irq-pins during vgpiofront initialization! error = %d\n", err);
	  goto error;
	}
	if(!strncmp(tmpstr, "all", sizeof(tmpstr))){
		VGPIO_ERR("option 'all' not supported\n");
	}
	else{
		parse_and_add_gpio_string(tmpstr, dev);		
	}
	
	// Sort list in ascending order
	list_sort(NULL, &dev->available_gpios.list, gpio_compare);

	err = create_gpio_chips(dev);
	if(err){
		VGPIO_ERR("Creating gpio_chips failed: %d\n", err);
		goto error;
	}

	return dev;

error:
   shutdown_vgpiofront(xbdev);
   return NULL;
}

void vgpiofront_set_state(struct xenbus_device *xbdev, 
									enum xenbus_state state)
{
	struct vgpiofront_dev* dev;
	if(!xbdev) return;
	
	dev = dev_get_drvdata(&xbdev->dev);
	if(!dev) return;
	
	dev->state = state;
	xenbus_switch_state(xbdev, state);
										
}

// The function is called on activation of the device
static int vgpiofront_probe(struct xenbus_device *xbdev,
              const struct xenbus_device_id *id)
{
	
	if(_xbdev){
		VGPIO_LOG("vgpiofront_probe() called morce than once. Only one device is supported\n");
		return -1;
	}
	_xbdev = xbdev;
	VGPIO_DEBUG("vgpiofront_probe called\n");
	
	// Create vgpio device
	dev = init_vgpiofront(xbdev, "device/vgpio/0");
	if(!dev) goto error;
	
	VGPIO_DEBUG("xbdev: %p dev %p\n", xbdev, dev);
	
	dev_set_drvdata(&xbdev->dev, dev);
	vgpiofront_set_state(xbdev, XenbusStateInitialising);
	
	
	return 0;
	
error:
	dev_set_drvdata(&xbdev->dev, NULL);
	return -1;
}


// The function is called on a state change of the backend driver
static void vgpioback_changed(struct xenbus_device *xbdev,
			    enum xenbus_state backend_state)
{
	struct vgpiofront_dev* dev;
	dev = dev_get_drvdata(&xbdev->dev);
	
	VGPIO_DEBUG("backend changed: %s -> %s\n", xbdev->otherend, xenbus_strstate(backend_state));
	
	switch (backend_state)
	{
		case XenbusStateInitialising:
			vgpiofront_set_state(xbdev, XenbusStateInitialising);
			break;
		case XenbusStateInitialised:
		case XenbusStateReconfiguring:
		case XenbusStateReconfigured:
		case XenbusStateUnknown:
			break;

		case XenbusStateInitWait:
			if (xbdev->state != XenbusStateInitialising)
				break;
			if (vgpiofront_connect(xbdev) != 0)
				break;

			vgpiofront_set_state(xbdev, XenbusStateConnected);

			break;

		case XenbusStateConnected:
			VGPIO_DEBUG("Other side says it is connected as well.\n");
			if(dev->resume)
				vgpio_setup(dev);
			dev->resume = 0;
			//~ test(dev);
			break;

		case XenbusStateClosed:
			if (xbdev->state == XenbusStateClosed)
				break;
			/* Missed the backend's CLOSING state -- fallthrough */
		case XenbusStateClosing:
			if (xbdev->state != XenbusStateConnected)
				break;
			shutdown_vgpiofront(xbdev);
			xenbus_frontend_closed(xbdev);
			vgpiofront_set_state(xbdev, XenbusStateInitialising);
	}
}

static int vgpiofront_gpio_request(struct gpio_chip *gc, unsigned int offset)
{
	struct vgpiofront_dev* dev;
	
	VGPIO_DEBUG("vgpiofront_gpio_request() offset: %d\n", offset);
	if(!_xbdev) return -1;
	
	dev = dev_get_drvdata(&_xbdev->dev);
	if(!dev) return -1;
	
	return _vgpiofront_gpio_request(dev, gc->base + offset, module_name(THIS_MODULE));
}

static void vgpiofront_gpio_free(struct gpio_chip *gc, unsigned int offset)
{
	struct vgpiofront_dev* dev;
	
	VGPIO_DEBUG("vgpiofront_gpio_request() offset: %d\n", offset);
	if(!_xbdev) return;
	dev = dev_get_drvdata(&_xbdev->dev);
	
	if(!dev) return;
	
	dev = dev_get_drvdata(&_xbdev->dev);
	if(!dev) return;
	
	_vgpiofront_gpio_free(dev, gc->base + offset);
}

static int vgpiofront_gpio_get(struct gpio_chip *gc, unsigned int offset)
{	struct vgpiofront_dev* dev;
	
	VGPIO_DEBUG("vgpiofront_gpio_request() offset: %d\n", offset);
	if(!_xbdev) return -1;
	
	dev = dev_get_drvdata(&_xbdev->dev);
	if(!dev) return -1;
	
	return _vgpiofront_get_value(dev, gc->base + offset);
}

static void vgpiofront_gpio_set(struct gpio_chip *gc, unsigned int offset, int
			     value)
{
	struct vgpiofront_dev* dev;
	
	if(!_xbdev) return;
	
	dev = dev_get_drvdata(&_xbdev->dev);
	if(!dev) return;
	
	_vgpiofront_set_value(dev, gc->base + offset, value);
}

static int vgpiofront_gpio_direction_input(struct gpio_chip *gc,
					unsigned int offset)
{
	struct vgpiofront_dev* dev;
	
	if(!_xbdev) return -1;
	
	dev = dev_get_drvdata(&_xbdev->dev);

	return _vgpiofront_direction_input(dev, gc->base + offset);
}

static int vgpiofront_gpio_direction_output(struct gpio_chip *gc,
					 unsigned int offset, int value)
{
	struct vgpiofront_dev* dev;
	
	if(!_xbdev) return -1;
	
	dev = dev_get_drvdata(&_xbdev->dev);
	if(!dev) return -1;
	
	return _vgpiofront_direction_output(dev, gc->base + offset, value);
}


static int vgpiofront_remove(struct xenbus_device *xbdev)
{
	VGPIO_DEBUG("vgpiofront_remove()\n");
	shutdown_vgpiofront(xbdev);
	return 0;
}

static const struct xenbus_device_id vgpiofront_ids[] = {
	{ "vgpio" },
	{ "" }
};

static int vgpiofront_suspend(struct xenbus_device *xbdev)
{
	struct vgpiofront_dev   *dev;
	VGPIO_DEBUG("vgpiofront_suspend()\n");
	
	dev = dev_get_drvdata(&xbdev->dev);

	if(!dev){
		VGPIO_LOG("no device!");
		return -ENODEV;
	}
	
	print_requested_gpios(dev);

}
static int vgpiofront_resume(struct xenbus_device *xbdev)
{
	struct vgpiofront_dev* dev;
	
	VGPIO_DEBUG("vgpiofront_resume()\n");
	
	dev = dev_get_drvdata(&xbdev->dev);
	
	if(!dev){
		VGPIO_LOG("no device!");
		return -ENODEV;
	}
	
	dev->resume = 1;
	
	print_requested_gpios(dev);
	
	return 0;
}

static struct xenbus_driver vgpiofront_driver = {
	.ids = vgpiofront_ids,
	.probe = vgpiofront_probe,
	.remove = vgpiofront_remove,
	.otherend_changed = vgpioback_changed,
	.resume = vgpiofront_resume,
	.suspend = vgpiofront_suspend,
};
static int __init vgpiofront_init(void)
{
	int ret;
	printk(KERN_NOTICE "vgpio: init\n");
	if (!xen_domain()){	
		printk(KERN_NOTICE "vgpio: no xen domain\n");
		return -ENODEV;
	}

	
	ret = xenbus_register_frontend(&vgpiofront_driver);
	VGPIO_DEBUG("xenbus_register_frontend(): %d\n", ret);

	return ret;
}
module_init(vgpiofront_init);

static void __exit vgpiofront_exit(void)
{
	VGPIO_DEBUG("vgpio: exit\n");
	if(_xbdev) vgpiofront_remove(_xbdev);
	
	xenbus_unregister_driver(&vgpiofront_driver);	
}
module_exit(vgpiofront_exit);

MODULE_DESCRIPTION("Xen virtual gpio device frontend");
MODULE_LICENSE("GPL");
MODULE_ALIAS("xen-frontend:vgpio");
