#ifndef _XEN_VGPIOFRONT_H
#define _XEN_VGPIOFRONT_H

#include <linux/module.h>  /* Needed by all modules */
#include <linux/kernel.h>  /* Needed for KERN_ALERT */

#include <xen/xen.h>       /* We are doing something with Xen */
#include <xen/xenbus.h>
#include <xen/events.h>
#include <xen/interface/event_channel.h>
#include <xen/interface/grant_table.h>
#include <xen/interface/io/ring.h>
#include <xen/page.h>
#include <linux/errno.h>
#include <linux/list.h>
#include <linux/semaphore.h>
#include <xen/grant_table.h>

#define INVALID_RESPONSE			-9999

typedef enum { CMD_GPIO_REQUEST, CMD_GPIO_FREE, CMD_GPIO_DIRECTION_OUTPUT, 
		CMD_GPIO_DIRECTION_INPUT, CMD_GPIO_SET_DEBOUNCE, CMD_GPIO_GET_VALUE, 
		CMD_GPIO_SET_VALUE, CMD_GPIO_REQUEST_IRQ, CMD_GPIO_FREE_IRQ } vgpio_command_t;
	
typedef enum { MODE_REQUESTED, MODE_OUTPUT, MODE_INPUT, MODE_IRQ } vgpio_mode_t;
	
struct gpio_chip_list{
	struct list_head list;
	struct gpio_chip *chip;
};

struct available_gpio_list{
	struct list_head list;
	unsigned pin;
};
		
struct requested_gpio{
	struct list_head 	list;
	unsigned 			pin;
	int		 			val;
	vgpio_mode_t 		mode;
	unsigned			trigger;
	irqreturn_t			(*handler)(int, void*);
	void				*handler_data;
};

/**
 * List of irqs a device has requested
 * @pin:			the gpio pin the interrupt was requested for
 * @port: 			event channel used to receive notifications about interrupts
 * @irq: 			the interrupt number assigend to the event channel
 * @handler_data: 	data passed to the interrupt handler for the event channel
 * 					we need to keep this pointer to we can pass it to unbind_from_irqhandler
 * 					when freeing the irq again
 */
struct pin_irq_list {
	struct list_head list;
	unsigned pin;
	evtchn_port_t port;
	unsigned irq;
	void* handler_data;
};
/*
 * Request type for the shared ring.
 * cmd is the command that is requested, e.g. CMD_GPIO_REQUEST
 * pin is the number of the gpio pin the command is to be performed on
 * val is the value some gpio_* functions requeire, e.g. 1 or 0 for gpio_set_value()
 */
struct vgpio_request {
	vgpio_command_t cmd;
	unsigned pin;
	unsigned val; 
	unsigned irq_edge;
};
typedef struct vgpio_request vgpio_request_t;

/*
 * Response type for the shared ring.
 * ret is the return value of the function that was called 
 * (e.g. gpio_request()) 
 */
struct vgpio_response {
	int ret;
};
typedef struct vgpio_response vgpio_response_t;

DEFINE_RING_TYPES(vgpio, struct vgpio_request, struct vgpio_response);



struct vgpiofront_dev {
	grant_ref_t ring_ref;
	evtchn_port_t comm_evtchn;
	unsigned int comm_evtchn_irq;		
	
	struct vgpio_front_ring ring;
	unsigned long ring_page;

	domid_t bedomid;
	char* nodename;
	char* bepath;

	enum xenbus_state state;
	struct xenbus_device *xbdev;
	struct semaphore sem; // Semaphore used for waiting for responses from backend

	vgpio_response_t last_response;


	struct pin_irq_list irq_list;
	
	/* List of all GPIOs a device can use
	* This is a merged and sorted list of the gpio numbers from xenstore.
	* Every number only appears once and the pin numbers are sorted in ascending order.
	* This is important, because this list is used to create the gpio_chip structures,
	* for which we will find the ranges of consecutive gpios in this list. 
	*/
	struct available_gpio_list available_gpios;
	
	/* Number of elements in available_gpios, since there is no 
	 * list.size() or anything like that
	 */
	unsigned num_available_gpios; 
	
	/* List of gpio_chips this device manages 
	 * There will be one gpio_chip for each range of
	 * consecutive gpios in available_gpios
	 * The base of that chip will be the first gpio in a range
	 * and ngpio will be the number of gpios in a range.
	 * Example:
	 *   The device has access to gpios 403, 404, 405, 408, 410, 411.
	 *   Then there will be 3 gpio_chips:
	 * 		1. base 403, ngpio 3
	 * 		2. base 408, ngpio 1
	 * 		3. base 410, ngpio 2
	 */
	struct gpio_chip_list gpio_chips;
	
	/*
	 * List of all gpios this device has requested
	 * Will be used on resume to put the hardware back 
	 * to the correct state
	 */
	struct requested_gpio requested_gpios;
	
	int resume;
};

struct xenbus_device *_xbdev = NULL;

/*Initialize frontend */
struct vgpiofront_dev* init_vgpiofront(struct xenbus_device *xbdev, const char* nodename);
/*Shutdown frontend */
void shutdown_vgpiofront(struct xenbus_device *xbdev);
void free_vgpiofront(struct xenbus_device *xbdev);

static irqreturn_t vgpiofront_comm_interrupt(int irq, void *dev_id);

int vgpiofront_send_request(struct vgpiofront_dev* dev, vgpio_request_t req);

int _vgpiofront_gpio_request(struct vgpiofront_dev *dev, unsigned gpio, const char *label);
void _vgpiofront_gpio_free(struct vgpiofront_dev *dev, unsigned gpio);
int _vgpiofront_direction_input(struct vgpiofront_dev *dev, unsigned gpio);
int _vgpiofront_direction_output(struct vgpiofront_dev *dev, unsigned gpio, int value);
int _vgpiofront_get_value(struct vgpiofront_dev *dev, unsigned gpio);
void _vgpiofront_set_value(struct vgpiofront_dev *dev, unsigned gpio, int value);
int vgpio_request_irq(struct vgpiofront_dev *dev, unsigned gpio, irq_handler_t handler, void* handler_data, unsigned trigger);
void vgpio_free_irq(struct vgpiofront_dev *dev, unsigned gpio);

void test(struct vgpiofront_dev *dev);

int vgpio_setup(struct vgpiofront_dev *dev);

struct requested_gpio* get_requested_gpio(struct vgpiofront_dev *dev, unsigned pin);

/**
 * Adds req to dev's requested_gpios list if there is no entry with the 
 * same pin number yet. If there is an element with the same pin number,
 * it will be overwritten.
 */
int add_requested_gpio(struct vgpiofront_dev *dev, struct requested_gpio *req);

#endif /* _XEN_VGPIOFRONT_H */
