/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation; or, when distributed
 * separately from the Linux kernel or incorporated into other
 * software packages, subject to the following license:
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this source file (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy, modify,
 * merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
#ifndef __XEN_VGPIOBACK__COMMON_H__
#define __XEN_VGPIOBACK__COMMON_H__

#include <linux/module.h>  /* Needed by all modules */
#include <linux/kernel.h>  /* Needed for KERN_ALERT */

#include <xen/xen.h>       /* We are doing something with Xen */
#include <xen/xenbus.h>
#include <xen/events.h>
#include <xen/interface/event_channel.h>
#include <xen/interface/grant_table.h>
#include <xen/interface/io/ring.h>
#include <xen/page.h>
#include <linux/errno.h>
#include <linux/list.h>
#include <linux/workqueue.h>
#include <linux/init.h>

#define VGPIO_PRINT_DEBUG
#ifdef VGPIO_PRINT_DEBUG
#define VGPIO_DEBUG(fmt,...) printk("vgpio:Debug("__FILE__":%d) " fmt, __LINE__, ##__VA_ARGS__)
#define VGPIO_DEBUG_MORE(fmt,...) printk(fmt, ##__VA_ARGS__)
#else
#define VGPIO_DEBUG(fmt,...)
#endif

#define VGPIO_LOG(fmt,...) printk(KERN_NOTICE "vgpio:Info " fmt, ##__VA_ARGS__)


typedef enum { CMD_GPIO_REQUEST, CMD_GPIO_FREE, CMD_GPIO_DIRECTION_OUTPUT, 
		CMD_GPIO_DIRECTION_INPUT, CMD_GPIO_SET_DEBOUNCE, CMD_GPIO_GET_VALUE, 
		CMD_GPIO_SET_VALUE, CMD_GPIO_REQUEST_IRQ, CMD_GPIO_FREE_IRQ } vgpio_command_t;

/*
 * Request type for the shared ring.
 * cmd is the command that is requested, e.g. CMD_GPIO_REQUEST
 * pin is the number of the gpio pin the command is to be performed on
 * val is the value some gpio_* functions requeire, e.g. 1 or 0 for gpio_set_value()
 */
struct vgpio_request {
	vgpio_command_t cmd;
	unsigned pin;
	unsigned val; 
	unsigned irq_edge;
};
typedef struct vgpio_request vgpio_request_t;

/*
 * Response type for the shared ring.
 * ret is the return value of the function that was called 
 * (e.g. gpio_request()) 
 */
struct vgpio_response {
	int ret;
};
typedef struct vgpio_response vgpio_response_t;

DEFINE_RING_TYPES(vgpio, struct vgpio_request, struct vgpio_response);

/* 
 * List of event channels that have to be notified when irq occurs
 * irq is the irq number of the GPIO pin, needed for free_irq
 * port_irq is the irq number of the event channel that will be used to 
 * notify the remote domain
 */ 
struct gpio_irq_notifyee_list{
	struct list_head list;
	//~ unsigned irq;
	unsigned port_irq;
};

/*
 * List of all GPIOs that have been requested from all VMs.
 * This list is managed by the backend driver to keep track of all the GPIOs
 * that have been requested, so that a GPIO is not requested a second time if 
 * it has already been requested by another device before.
 * pin is the number of the GPIO.
 * num_requested is a counter that indicates how often (by how many devices)
 * this pin has been requested. A gpio can only be freed if the counter is 0,
 * which means, that no device is using it anymore.
 */
struct global_gpio_list{
	struct list_head list;
	unsigned pin;
	unsigned num_requested;
};

/* Data that will be passed to free_irq_helper */
struct free_irq_helper_info{
	struct vgpioif *dev;
	vgpio_request_t req;
	struct work_struct work;
};

/*
 * List of all GPIOs for which interrupts have been requested.
 * Similar to global_gpio_list but only for GPIOs that are used as interrupts.
 * pin is the GPIO pin number.
 * irq is the irq line associated with that pin
 * free_irq_work_info is the work_struct used for schedule_work to free the IRQ on CMD_GPIO_FREE_IRQ
 * notifyees is the list of irqs that will be used to notify the remote domains 
 * 
 */ 
struct global_gpio_irq_list{
	struct list_head list;
	unsigned pin;
	unsigned irq;
	struct free_irq_helper_info free_irq_work_info;
	struct gpio_irq_notifyee_list notifyees;
};

/* Global GPIO list */
extern struct global_gpio_list g_gpio_list;
/* Global list of GPIOs with interrupts */
extern struct global_gpio_list g_gpio_irq_list;

/*
 * List used by devices to keep track of their requested GPIOs.
 * If a GPIO is requested, this list is consulted first to see if 
 * it has already be requested before, to avoid requesting a pin more 
 * than once.
 * port_irq is the irq number associated with the event channel used for 
 * delivering interrupts to the frontend
 */
struct dev_gpio_list{
	struct list_head list;
	unsigned pin;
	unsigned port_irq;
};


struct backend_info {
	struct xenbus_device *dev;
	struct vgpioif *vgpio;

	/* This is the state that will be reflected in xenstore
	 */
	enum xenbus_state state;

	enum xenbus_state frontend_state;
};


struct vgpioif {
	domid_t fe_domid;
	unsigned int handle;

	/* event channel for communication / commands from front end */
	evtchn_port_t comm_evtchn; 	
	/* interrupt number for events on comm_evtchn */
	unsigned int comm_irq;		

	/* Shared ring */
	struct vgpio_back_ring ring;

	/* GPIOs this device has permission to use as output pins */
	unsigned* out_pins;
	/* total number of pins this device has permission to use as ouput pins */
	unsigned num_out_pins;
	/* access to all pins as output, out_pins will be ignored if set*/
	unsigned out_all_access;

	/* GPIOs this device has permission to use as input pins */
	unsigned* in_pins;
	/* total number of pins this device has permission to use as input pins */
	unsigned num_in_pins;
	/* access to all pins as input, in_pins will be ignored if set*/
	unsigned in_all_access;

	/* GPIOs this device has permission to request interrupts on  */
	unsigned* irq_pins;
	/* total number of pins this device has pegpio_listrmission to request interrupts on */
	unsigned num_irq_pins;
	/* access to all pins as irq pins, irq_pins will be ignored if set*/
	unsigned irq_all_access;

	/* if this is 1, the permissions above will be ignored and the 
	* device has full access to all GPIOs */
	unsigned full_access;

	struct dev_gpio_list gpio_list;
   
};
//~ typedef struct vgpioif vgpioif_t;


irqreturn_t vgpioback_interrupt_handler(int irq, void *dev_id);

int xenvgpio_xenbus_init(void);
void xenvgpio_xenbus_fini(void);

struct vgpioif *vgpioif_alloc(domid_t fe_domid);
void vgpioif_free(struct vgpioif *vgpio);

void vgpio_send_response(struct vgpioif *dev, vgpio_response_t *rsp);
void vgpio_handle_request(struct vgpioif *dev, vgpio_request_t *req);

int vgpio_request(unsigned gpio, const char *label);
int vgpio_irq_request(struct vgpioif *dev, unsigned gpio, unsigned trigger, evtchn_port_t port);
void vgpio_free(unsigned gpio);
void vgpio_irq_free(unsigned gpio, unsigned port_irq);


int dev_vgpio_request(struct vgpioif *dev, unsigned gpio, const char *label);
int dev_vgpio_irq_request(struct vgpioif *dev, unsigned gpio, unsigned trigger, evtchn_port_t port);
void dev_vgpio_free(struct vgpioif *dev, unsigned gpio);
void dev_vgpio_irq_free(struct vgpioif *dev, unsigned gpio);


int dev_vgpio_check_permission(struct vgpioif *dev, unsigned gpio, vgpio_command_t cmd);

#endif /* __XEN_VGPIOBACK__COMMON_H__ */
