#include "common.h"

void vgpio_send_response(struct vgpioif *dev, vgpio_response_t *rsp){
	RING_IDX i;
	vgpio_response_t *_rsp;
	int notify;
	
	i = dev->ring.rsp_prod_pvt;
	_rsp = RING_GET_RESPONSE(&dev->ring, i);
	_rsp->ret = rsp->ret;
	dev->ring.rsp_prod_pvt = i + 1;

	wmb();
	RING_PUSH_RESPONSES_AND_CHECK_NOTIFY(&dev->ring, notify);
	if(notify) 
	{
		notify_remote_via_irq(dev->comm_irq);
	}
	else{
		//~ VGPIO_DEBUG("not notified\n");
	}
}

irqreturn_t vgpioback_interrupt_handler(int irq, void *dev_id)
{
	struct vgpioif *dev;
	RING_IDX rq, cons;
	vgpio_request_t *req;
	int nr_consumed, more;
	struct backend_info *be = (struct backend_info*)dev_id;
	unsigned long flags;
	
	
	if(!be){
		VGPIO_DEBUG("interrupt! irq: %d\n",irq); 
		VGPIO_LOG("vgpioback_interrupt_handler: be is NULL\n");
		return IRQ_HANDLED;
	}
	
	dev = be->vgpio;
	

	//~ VGPIO_DEBUG("interrupt! irq: %d\n",irq); 
	
	nr_consumed = 0;
	local_irq_save(flags);
	
moretodo:
	rq = dev->ring.sring->req_prod;
    rmb(); /* Ensure we see queued responses up to 'rp'. */
    cons = dev->ring.req_cons;
   
	while ((cons != rq))
	{
		req = RING_GET_REQUEST(&dev->ring, cons);
		nr_consumed++;
		vgpio_handle_request(dev, req);
		dev->ring.req_cons = ++cons;
		if (dev->ring.req_cons != cons)
            /* We reentered, we must not continue here */
            break;
	}
	
	RING_FINAL_CHECK_FOR_REQUESTS(&dev->ring, more);
    if (more) goto moretodo;
    if(!nr_consumed) goto done;
	
	
done:
	local_irq_restore(flags);
	return IRQ_HANDLED;
}

struct vgpioif *vgpioif_alloc(domid_t fe_domid)
{
	struct vgpioif *dev;
	
	dev = kzalloc(sizeof(struct vgpioif), GFP_KERNEL);
	if(!dev)
		return ERR_PTR(-ENOMEM);
	
	dev->fe_domid = fe_domid;
	
	INIT_LIST_HEAD(&dev->gpio_list.list);
	
	return dev;
}

void vgpioif_free(struct vgpioif *dev)
{
	struct dev_gpio_list *tmp;
	struct list_head *pos, *q;
		
	// free any remaining gpios and gpio_list memory
	list_for_each_safe(pos, q, &dev->gpio_list.list){
		tmp = list_entry(pos, struct dev_gpio_list, list);
		VGPIO_DEBUG("dev freeing gpio %d\n", tmp->pin);	
		vgpio_irq_free(tmp->pin, tmp->port_irq);
		vgpio_free(tmp->pin);
		list_del(pos);
		kfree(tmp);
	}
	
	// free device memory
	kfree(dev);
}
