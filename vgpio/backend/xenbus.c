#include "common.h"
#include <linux/string.h>


static void backend_connect(struct backend_info *be);
static void backend_disconnect(struct backend_info *be);
static void set_backend_state(struct backend_info *be,
			      enum xenbus_state state);
static int backend_create_vgpioif(struct backend_info *be);

/* parse a comma separated string of gpio numbers (e.g. "10,11,12")
 * pin numbers will be stored in pins, total numbder of pins will be stored 
 * in num_pins.
 * allocates num_pins * sizeof(unsigned) bytes of memory for pins.
 * Call is responsible for freeing the memory once it is no longer needed.
 * Does NOT allocate memory for num_pins, must point to an unsigend int.
 */
void parse_gpio_string(char* str, unsigned **pins, unsigned *num_pins)
{
	char *p, *_str;
	int err;
	unsigned num = 0;
	if(!str || !num_pins){
		VGPIO_DEBUG("error str or num_pins NULL: *str: %p, *num_pins: %p\n", str, num_pins);
		return;
	}
	*num_pins = 0;
	_str = kstrdup(str, GFP_KERNEL);
	
	// First pass: Count how many pins there are
	while ( (p = strsep(&_str, ","))  != NULL){
		while (*p == ' ') p++;
		num++;
	}
	
	// Allocate memory
	*pins = kzalloc(num * sizeof(unsigned), GFP_KERNEL);
	
	if(!*pins){
		VGPIO_DEBUG("parse_gpio_string: error allocating memory\n"); 
		return;
	}
	
	num = 0;
	_str = kstrdup(str, GFP_KERNEL);
	
	// Second pass: parse pins
	while ( (p = strsep(&_str, ","))  != NULL){
		while (*p == ' ') p++;
		err = sscanf(p, "%u", &(*pins)[num]);
		if(err != 1){
			VGPIO_DEBUG("parse_gpio_string: error in pass 2 (%d), num %d\n",err, num);
		}
		num++;
	}
	
	*num_pins = num;
}

static int backend_create_vgpioif(struct backend_info *be)
{
	int err;
	struct xenbus_device *dev = be->dev;
	struct vgpioif *vgpio;

	if (be->vgpio != NULL)
		return 0;

	vgpio = vgpioif_alloc(dev->otherend_id);
	if (IS_ERR(vgpio)) {
		err = PTR_ERR(vgpio);
		xenbus_dev_fatal(dev, err, "creating interface ");
		return err;
	}
	be->vgpio = vgpio;

	kobject_uevent(&dev->dev.kobj, KOBJ_ONLINE);
	return 0;
}	

// This is where we set up path watchers and event channels
static void backend_connect(struct backend_info *be)
{
	unsigned int val;
	grant_ref_t ring_ref;
	unsigned int evtchn;
	int err;
	void* ring_addr;
	struct xenbus_device *dev = be->dev;
	struct vgpio_sring *shared;
	struct vgpioif *vgpio = be->vgpio;
	char tmpstr[512];
	
	if(!dev || !vgpio){
		VGPIO_LOG("backend_connect error");
		return;
	}
	
	VGPIO_LOG("Connecting the backend now\n");
	
	VGPIO_DEBUG("Fetching ring-ref\n");
	/* Fetch the grant reference (ring-ref from fe-path) */
	err = xenbus_scanf(XBT_NIL, dev->otherend,
			   "ring-ref", "%u", &val);
	if (err < 0){
		VGPIO_LOG("No ring-ref in %s\n", dev->otherend);
		goto done; /* The frontend does not have a control ring */
	}
	ring_ref = val;
	VGPIO_DEBUG("ring ref is %u\n", ring_ref);

	/* Map shared page */
	err = xenbus_map_ring_valloc(dev, &ring_ref,1, &ring_addr);
	if(err != 0){
		VGPIO_LOG("Error while trying to map shared page: %d\n", err);
		goto fail;
	}
	shared = (struct vgpio_sring*)ring_addr;

	/* Initialize shared ring */
    BACK_RING_INIT(&vgpio->ring, shared, XEN_PAGE_SIZE);
	VGPIO_LOG("Shared ring mapped successfully!\n");

	VGPIO_DEBUG("fetching event-channel\n");
	/* Fetch event channel (event-channel from fe-path) */
	err = xenbus_scanf(XBT_NIL, dev->otherend,
			   "event-channel", "%u", &val);
	if (err < 0) {
		VGPIO_LOG("No event-channel in %s\n", dev->otherend);
		xenbus_dev_fatal(dev, err,
				 "reading %s/event-channel-ctrl",
				 dev->otherend);
		goto fail;
	}

	evtchn = val;
	vgpio->comm_evtchn = evtchn;
	VGPIO_DEBUG("evtchn is %u\n", vgpio->comm_evtchn);
	
	/* bind event channel */
	err = bind_interdomain_evtchn_to_irqhandler(
			// id of FE domU, evtchn, interrupt handler,          flags
			dev->otherend_id, evtchn, vgpioback_interrupt_handler, 0,
			// device name,  device-id (void*), will be passed to handler
			dev->devicetype, be);
	VGPIO_DEBUG("bind result: %d\n", err);
	
	if(err < 0){
		goto fail;
	}
	vgpio->comm_irq = err;
	
	/* Read GPIO permissions from Xenstore */
	err = xenbus_scanf(XBT_NIL, dev->nodename,
			   "output-pins", "%s", tmpstr);
	if (err < 0){
		VGPIO_LOG("Could not read output-pins from %s\n", dev->otherend);
		goto done;
	}
	VGPIO_LOG("output pins: %s\n", (const char*)tmpstr);
	// If string is 'all', just set the flag, otherwise parse string
	if(!strncmp(tmpstr, "all", sizeof(tmpstr))){
		vgpio->out_all_access = 1;
	}
	else{
		parse_gpio_string((char*)tmpstr, &vgpio->out_pins, &vgpio->num_out_pins);		
#ifdef VGPIO_PRINT_DEBUG	
		VGPIO_DEBUG("%d out_pins: ", vgpio->num_out_pins); 
		for(err = 0; err < vgpio->num_out_pins; err++){
			printk("%d ", vgpio->out_pins[err]);			
		}
		printk("\n");
#endif
	}
	
	err = xenbus_scanf(XBT_NIL, dev->nodename,
			   "input-pins", "%s", tmpstr);
	if (err < 0){
		VGPIO_LOG("Could not read input-pins from %s\n", dev->otherend);
		goto done;
	}
	VGPIO_LOG("input pins: %s\n", (const char*)tmpstr);
	
	if(!strncmp(tmpstr, "all", sizeof(tmpstr))){
		vgpio->in_all_access = 1;
	}
	else{
		parse_gpio_string((char*)tmpstr, &vgpio->in_pins, &vgpio->num_in_pins);		
#ifdef VGPIO_PRINT_DEBUG	
		VGPIO_DEBUG("%d in_pins: ", vgpio->num_in_pins); 
		for(err = 0; err < vgpio->num_in_pins; err++){
			printk("%d ", vgpio->in_pins[err]);			
		}
		printk("\n");
#endif
	}	
	err = xenbus_scanf(XBT_NIL, dev->nodename,
			   "irq-pins", "%s", tmpstr);
	if (err < 0){
		VGPIO_LOG("Could not read irq-pins from %s\n", dev->otherend);
		goto done;
	}
	VGPIO_LOG("irq pins: %s\n", (const char*)tmpstr);
	if(!strncmp(tmpstr, "all", sizeof(tmpstr))){
		vgpio->irq_all_access = 1;
	}
	else{
		parse_gpio_string((char*)tmpstr, &vgpio->irq_pins, &vgpio->num_irq_pins);
#ifdef VGPIO_PRINT_DEBUG	
		VGPIO_DEBUG("%d irq_pins: ", vgpio->num_irq_pins); 
		for(err = 0; err < vgpio->num_irq_pins; err++){
			printk("%d ", vgpio->irq_pins[err]);			
		}
		printk("\n");
#endif
	}
	
done:
	return;

fail:
	backend_disconnect(be);		
	return;
	
}

// This will destroy event channel handlers
static void backend_disconnect(struct backend_info *be)
{
	int ret;
	struct vgpioif *vgpio;
	pr_info("Disconnecting the backend now\n");
	VGPIO_LOG("Unmapping shared page and event channel\n");
	
	vgpio = be->vgpio;
	if(!vgpio)
		return;
			
	if(vgpio->comm_irq){
		unbind_from_irqhandler(vgpio->comm_irq, be);
		vgpio->comm_irq = 0;
	}
	VGPIO_DEBUG("Unbound from IRQ handler");
	
	if(vgpio->ring.sring){
		ret = xenbus_unmap_ring_vfree(be->dev, (void*)vgpio->ring.sring);
		VGPIO_DEBUG("Unmap shared page result: %d\n", ret);		
	}
	if(vgpio->out_pins){
		kfree(vgpio->out_pins);
		vgpio->out_pins = NULL;
	}
	if(vgpio->in_pins){
		kfree(vgpio->in_pins);
		vgpio->in_pins = NULL;
	}
	if(vgpio->irq_pins){
		kfree(vgpio->irq_pins);
		vgpio->irq_pins = NULL;
	}
}

static inline void backend_switch_state(struct backend_info *be,
					enum xenbus_state state)
{
	struct xenbus_device *dev = be->dev;

	VGPIO_DEBUG("%s -> %s\n", dev->nodename, xenbus_strstate(state));
	be->state = state;

	xenbus_switch_state(dev, state);
}

// We try to switch to the next state from a previous one
static void set_backend_state(struct backend_info *be,
			      enum xenbus_state state)
{
	while (be->state != state) {
		switch (be->state) {
		case XenbusStateInitialising:
			switch (state) {
			case XenbusStateInitWait:
			case XenbusStateConnected:
			case XenbusStateClosing:
				backend_switch_state(be, XenbusStateInitWait);
				break;
			case XenbusStateClosed:
				backend_switch_state(be, XenbusStateClosed);
				break;
			default:
				BUG();
			}
			break;
		case XenbusStateClosed:
			switch (state) {
			case XenbusStateInitWait:
			case XenbusStateConnected:
				backend_switch_state(be, XenbusStateInitWait);
				break;
			case XenbusStateClosing:
				backend_switch_state(be, XenbusStateClosing);
				break;
			default:
				BUG();
			}
			break;
		case XenbusStateInitWait:
			switch (state) {
			case XenbusStateConnected:
				backend_connect(be);
				backend_switch_state(be, XenbusStateConnected);
				break;
			case XenbusStateClosing:
			case XenbusStateClosed:
				backend_switch_state(be, XenbusStateClosing);
				break;
			default:
				BUG();
			}
			break;
		case XenbusStateConnected:
			switch (state) {
			case XenbusStateInitWait:
			case XenbusStateClosing:
			case XenbusStateClosed:
				backend_disconnect(be);
				backend_switch_state(be, XenbusStateClosing);
				break;
			default:
				BUG();
			}
			break;
		case XenbusStateClosing:
			switch (state) {
			case XenbusStateInitWait:
			case XenbusStateConnected:
			case XenbusStateClosed:
				backend_switch_state(be, XenbusStateClosed);
				break;
			default:
				BUG();
			}
			break;
		default:
			BUG();
		}
	}
}

static int vgpioback_remove(struct xenbus_device *dev)
{
	struct backend_info *be = dev_get_drvdata(&dev->dev);

	set_backend_state(be, XenbusStateClosed);

	if (be->vgpio) {
		kobject_uevent(&dev->dev.kobj, KOBJ_OFFLINE);
		//xenbus_rm(XBT_NIL, dev->nodename, "hotplug-status");
		vgpioif_free(be->vgpio);
		be->vgpio = NULL;
	}
	kfree(be);
	dev_set_drvdata(&dev->dev, NULL);
	return 0;
}

// The function is called on activation of the device
static int vgpioback_probe(struct xenbus_device *dev,
			const struct xenbus_device_id *id)
{
	int err;
	struct backend_info *be = kzalloc(sizeof(struct backend_info),
					  GFP_KERNEL);
	if (!be) {
		xenbus_dev_fatal(dev, -ENOMEM,
				 "allocating backend structure");
		return -ENOMEM;
	}
	printk(KERN_NOTICE "vgpio: Probe called. We are good to go.\n");
	printk(KERN_NOTICE "vgpio: devicetype: %s\n", dev->devicetype);
	printk(KERN_NOTICE "vgpio: nodename: %s\n", dev->nodename);
	printk(KERN_NOTICE "vgpio: otherend: %s\n", dev->otherend);
	printk(KERN_NOTICE "vgpio: otherend_id: %d\n", dev->otherend_id);

	be->dev = dev;
	dev_set_drvdata(&dev->dev, be);

	be->state = XenbusStateInitialising;
	err = xenbus_switch_state(dev, XenbusStateInitialising);
	if (err)
		goto fail;
	
	err = backend_create_vgpioif(be);
	if (err)
		goto fail;
	

	return 0;
fail:
	VGPIO_DEBUG("failed\n");
	vgpioback_remove(dev);
	return err;
}



// The function is called on a state change of the frontend driver
static void vgpio_frontend_changed(struct xenbus_device *dev, enum xenbus_state frontend_state)
{
	
	struct backend_info *be = dev_get_drvdata(&dev->dev);

	VGPIO_DEBUG("frontend changed: %s -> %s\n", dev->otherend, xenbus_strstate(frontend_state));

	be->frontend_state = frontend_state;
	
	switch (frontend_state) {
		case XenbusStateInitialising:
			set_backend_state(be, XenbusStateInitWait);
			break;

		case XenbusStateInitialised:
			break;

		case XenbusStateConnected:
			set_backend_state(be, XenbusStateConnected);
			break;

		case XenbusStateClosing:
			VGPIO_LOG("Frontend is closing\n");
			set_backend_state(be, XenbusStateClosing);
			break;

		case XenbusStateClosed:
			set_backend_state(be, XenbusStateClosed);
			if (xenbus_dev_is_online(dev))
				break;
			/* fall through if not online */
		case XenbusStateUnknown:
			set_backend_state(be, XenbusStateClosed);
			device_unregister(&dev->dev);
			break;

		default:
			xenbus_dev_fatal(dev, -EINVAL, "saw state %s (%d) at frontend",
					xenbus_strstate(frontend_state), frontend_state);
			break;
	}
}


// This defines the name of the devices the driver reacts to
static const struct xenbus_device_id vgpioback_ids[] = {
	{ "vgpio" },
	{ "" }
};

// We set up the callback functions
static struct xenbus_driver vgpio_driver = {
	.ids  = vgpioback_ids,
	.probe = vgpioback_probe,
	.remove = vgpioback_remove,
	.otherend_changed = vgpio_frontend_changed,
};

int xenvgpio_xenbus_init(void)
{
	return xenbus_register_backend(&vgpio_driver);
}

void xenvgpio_xenbus_fini(void)
{
	xenbus_unregister_driver(&vgpio_driver);
}
