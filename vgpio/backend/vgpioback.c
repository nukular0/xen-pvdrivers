#include "common.h"
#include <linux/gpio.h>
#include <linux/interrupt.h> 



#define nVGPIO_BENCH_EXT_REACT

#define nVGPIO_BENCH
#ifdef VGPIO_BENCH
#include "../../../ma-various/evaluation/gpio/eval.h"

#define REPS 20000
#define LOGFILE "/root/various/evaluation/result.log"

uint64_t c_start, c_end;
uint64_t c_set[REPS], c_irq[REPS];
uint32_t rep = 0;

static void logTimes(void)
{
	struct file *f = file_open(LOGFILE, O_RDWR |  O_CREAT, 0);
	char buf[512];
	int i = 0;
	int offset = 0;
	uint64_t min = 0xFFFFFFFFFFFFFF, max = 0, avg = 0;
	uint64_t cycles;
	
	if(!f){
		printk("could not open logfile (%s)\n", LOGFILE);
		return;
	}
	
	offset = file_write(f, 0, "c_set,c_irq,diff\n", 17);
	for(; i < rep; i++){
		cycles = c_irq[i] - c_set[i];
		if(cycles < min)
			min = cycles;
		if(cycles > max)
			max = cycles;
		avg += cycles;
		snprintf(buf, 512, "%llu,%llu,%llu\n",c_set[i], c_irq[i], cycles);
		offset += file_write(f, offset, buf, strlen(buf));
	}
	
	file_close(f);
	

	if(rep > 0){
		avg /= rep;
	}
	
	printk("logged %u elements: min %llu | max %llu | avg %llu\n", rep, min, max, avg);
	
}

#endif

struct global_gpio_list g_gpio_list;
struct global_gpio_list g_gpio_irq_list;

static void free_irq_helper(struct work_struct *work)
{
	struct free_irq_helper_info *info = container_of(work, struct free_irq_helper_info, work);
	
	vgpio_response_t rsp = {0};
	
	// free irq
	dev_vgpio_irq_free(info->dev, info->req.pin);
	
	// send response
	vgpio_send_response(info->dev, &rsp);	
}

/*
 * Handler for GPIO interrupts.
 * All gpio interrupts will land here. dev_id points to the notifyee list
 * that is used to notify all domains that have requested an interrupt for the corresponding pin
 */
static irq_handler_t vgpio_pin_irq_handler(unsigned int irq, void *dev_id, struct pt_regs *regs)
{

	struct gpio_irq_notifyee_list *tmp;
	struct list_head *pos;
	struct gpio_irq_notifyee_list *notifyees = (struct gpio_irq_notifyee_list*)dev_id;



#ifdef nVGPIO_BENCH_EXT_REACT
	gpio_set_value(430, 0);
#endif

#ifdef VGPIO_BENCH
	if(rep < REPS)
		c_irq[rep++] = get_tsc();
#endif

	list_for_each(pos, &notifyees->list){
		tmp = list_entry(pos, struct gpio_irq_notifyee_list, list);
		notify_remote_via_irq(tmp->port_irq);
	}
	

	return (irq_handler_t)IRQ_HANDLED;
}

/* Check if dev has permission to use gpio pin for the action specified by cmd 
 * Returns 1 if device has permission, 0 otherwise
 */
int dev_vgpio_check_permission(struct vgpioif *dev, unsigned gpio, vgpio_command_t cmd)
{
	unsigned i;
	if(dev->full_access == 1) return 1;
	
	switch(cmd){
		// dev can request or free gpio if it has any permission to use it (out, in, irq) 
		case CMD_GPIO_REQUEST:
		case CMD_GPIO_FREE:
			if(dev->out_all_access || dev->in_all_access || dev->irq_all_access) return 1;
			for(i = 0; i < dev->num_out_pins; i++){
				if(dev->out_pins[i] == gpio) return 1;
			}
			for(i = 0; i < dev->num_in_pins; i++){
				if(dev->in_pins[i] == gpio) return 1;
			}
			for(i = 0; i < dev->num_irq_pins; i++){
				if(dev->irq_pins[i] == gpio) return 1;
			}
			break;
		/* SET_VALUE can only be used if dev has output permission */
		case CMD_GPIO_SET_VALUE:
		case CMD_GPIO_DIRECTION_OUTPUT:
			if(dev->out_all_access) return 1;
			for(i = 0; i < dev->num_out_pins; i++){
					if(dev->out_pins[i] == gpio) return 1;
			}
			break;
		/* GET_VALUE and SET_DEBOUNCE can only be used for pins with
		 * permission to use as input
		 */
		case CMD_GPIO_GET_VALUE:	
		case CMD_GPIO_SET_DEBOUNCE:
		case CMD_GPIO_DIRECTION_INPUT:
			if(dev->in_all_access) return 1;
			for(i = 0; i < dev->num_in_pins; i++){
				if(dev->in_pins[i] == gpio) return 1;
			}
			break;
		/* REQEST_IRQ and FREE_IRQ can be used with irq pins */
		case CMD_GPIO_REQUEST_IRQ:
		case CMD_GPIO_FREE_IRQ:
			if(dev->irq_all_access) return 1;
			for(i = 0; i < dev->num_irq_pins; i++){
				if(dev->irq_pins[i] == gpio) return 1;
			}
			break;
		default:
			return 0;
	}
	return 0;
}

/* Request a gpio pin for dev.
 * Will first check dev->gpio_list if dev has requested gpio before.
 * If not gpio will be requested and added to dev->gpio_list on success.
 */ 
int dev_vgpio_request(struct vgpioif *dev, unsigned gpio, const char *label)
{
	struct dev_gpio_list *tmp;
	struct list_head *pos;
	int ret;
	
	list_for_each(pos, &(dev->gpio_list.list)){
		tmp = list_entry(pos, struct dev_gpio_list, list);
		if(tmp->pin == gpio){
			VGPIO_DEBUG("GPIO %d has been requested by this device before\n", gpio);
			return 0;
		}
	}
	
	ret = vgpio_request(gpio, label);
	if(ret) return ret;
	
	tmp = (struct dev_gpio_list *)kzalloc(sizeof(struct dev_gpio_list), GFP_KERNEL);
	tmp->pin = gpio;
	list_add_tail(&(tmp->list), &(dev->gpio_list.list));
	return 0;
	
}

/*
 * Request an irq on pin for dev
 * Will first check dev->gpio_list of dev has already requested gpio
 * and if dev has already requested an irq on this pin
 * Will return 0 on success or an error code < 0 otherwise
 */
int dev_vgpio_irq_request(struct vgpioif *dev, unsigned gpio, unsigned trigger, evtchn_port_t port)
{
	struct dev_gpio_list *tmp;
	struct list_head *pos;
	int ret;
	unsigned found = 0;
	
	list_for_each(pos, &(dev->gpio_list.list)){
		tmp = list_entry(pos, struct dev_gpio_list, list);
		if(tmp->pin == gpio){
			found = 1;
			if(tmp->port_irq){
				VGPIO_DEBUG("IRQ has already been requested on pin %d by this device before\n", gpio);
				return 0;				
			}
			else{
				break;
			}
		}
	}
	
	if(!found) return -EPERM;
	
	ret = vgpio_irq_request(dev, gpio, trigger, port);
	if(ret < 0) return ret;
	
	tmp->port_irq = ret;
	return 0;
	
}

/* Free gpio pin for dev.
 * Will remove gpio from dev->gpio_list.
 * Whether gpio is actually freed or not depends on if any other devices
 * are still using it (== gpio is still in g_gpio_list)
 */ 
void dev_vgpio_free(struct vgpioif *dev, unsigned gpio)
{
	struct dev_gpio_list *tmp;
	struct list_head *pos, *q;
	

	list_for_each_safe(pos, q, &(dev->gpio_list.list)){
		tmp = list_entry(pos, struct dev_gpio_list, list);
		
		if(tmp->pin == gpio){
			list_del(pos);
			kfree(tmp);
			break;
		}
	}
	vgpio_free(gpio);
}

/* Free gpio pin irq for dev
 * dev will be removed from notifyee list and irq in dev's gpio_list will be unset,
 */
void dev_vgpio_irq_free(struct vgpioif *dev, unsigned gpio)
{
	struct dev_gpio_list *tmp;
	struct list_head *pos, *q;
	
	// Find the irq number for gpio
	list_for_each_safe(pos, q, &(dev->gpio_list.list)){
		tmp = list_entry(pos, struct dev_gpio_list, list);
		
		// remove from notifyee list, unbind event channel and free irq
		// then unset irq in gpio_list
		if(tmp->pin == gpio){
			vgpio_irq_free(gpio, tmp->port_irq);
			tmp->port_irq = 0;
			break;
		}
	}
}

/* Request a GPIO pin
 * Will check the global list of requested GPIO pins first.
 * If gpio has already been requested, it is not requested again
 */
int vgpio_request(unsigned gpio, const char *label)
{
	struct global_gpio_list *tmp;
	struct list_head *pos;
	int ret;
	
	// check the global list to see if this pin has been requested before
	list_for_each(pos, &g_gpio_list.list){
		tmp = list_entry(pos, struct global_gpio_list, list);
		
		// if the gpio has been requested before, just increase the
		// num_requested counter and return success 
		if(tmp->pin == gpio){
			tmp->num_requested++;
			VGPIO_DEBUG("GPIO %d has been requested before: %d\n", gpio, tmp->num_requested);
			return 0;
		}
	}
	
	// if the gpio has not been requested, it is not in the list
	// so try to request it and add it to the list on success
	ret = gpio_request(gpio, label);
	
	// if there was an error, just return the return error code
	if(ret) return ret;
	
	// add GPIO to the global list of requested GPIOs
	tmp = (struct global_gpio_list *)kzalloc(sizeof(struct global_gpio_list), GFP_KERNEL);
	tmp->pin = gpio;
	tmp->num_requested = 1;
	list_add(&(tmp->list), &(g_gpio_list.list));
	VGPIO_DEBUG("GPIO %d added to list\n", gpio);
	
	return 0;
}

/* Request interrupt for a GPIO pin
 * Will check the global list of requested irqs first.
 * If an irq has already been requested for this pin then we will 
 * just bind the event channel specified by port and add the resulting 
 * irq number to the notifyee list of global_gpio_irq_list
 */
int vgpio_irq_request(struct vgpioif *dev, unsigned gpio, unsigned trigger, evtchn_port_t port)
{
	struct global_gpio_irq_list *tmp;
	struct gpio_irq_notifyee_list *new_notifyee;
	struct list_head *pos;
	int port_irq, pin_irq, gpio_irq;
	
	// check the global irq list to see if there is an element with pin == gpio
	list_for_each(pos, &g_gpio_irq_list.list){
		tmp = list_entry(pos, struct global_gpio_irq_list, list);
		
		// if gpio is in the list we don't need to add it again, so skip ahead 
		if(tmp->pin == gpio){
			goto bind;
		}
	}
	
	// if gpio is not in the list we need to add a new element
	tmp = (struct global_gpio_irq_list *)kzalloc(sizeof(struct global_gpio_irq_list), GFP_KERNEL);
	tmp->pin = gpio;
	INIT_LIST_HEAD(&tmp->notifyees.list);
	list_add(&(tmp->list), &(g_gpio_irq_list.list));
	
bind:

	gpio_irq = gpio_to_irq(gpio);
	
	
	VGPIO_DEBUG("IRQ for pin %d is %d\n", gpio, gpio_irq);
	
	if(gpio_irq < 0){
		return gpio_irq;
	}
	
	tmp->irq = gpio_irq;
	
	// request irq for gpio
	pin_irq = request_irq(gpio_irq,             // The interrupt number requested
                        (irq_handler_t) vgpio_pin_irq_handler, // The pointer to the handler function 
                        trigger,   // Interrupt on rising edge (button press, not release)
                        "vgpioback_pin_irq_handler",    // Used in /proc/interrupts to identify the owner
                        &tmp->notifyees);                 // notifyee list passed to handler
	
	if(pin_irq < 0){
		VGPIO_DEBUG("Could not request irq %d: error %d\n", gpio_irq, pin_irq);
		return pin_irq;
	}
	
	// bind event channel and add irq to notifyee list
	port_irq = bind_interdomain_evtchn_to_irq(dev->fe_domid, port);
	
	if(port_irq < 0){
		VGPIO_DEBUG("Error trying to bind port %d: error %d\n", port, port_irq);
		return port_irq;
	}
	VGPIO_DEBUG("port %d bound to irq %d\n", port, port_irq);
	new_notifyee = (struct gpio_irq_notifyee_list *)kzalloc(sizeof(struct gpio_irq_notifyee_list), GFP_KERNEL);
	
	if(!new_notifyee) return -ENOMEM;
	
	//~ new_notifyee->irq = pin_irq;
	new_notifyee->port_irq = port_irq;
	
	list_add(&new_notifyee->list, &tmp->notifyees.list);
	
	return port_irq;
}

/* Free previously requested GPIO pin.
 * gpio will onyl be freed if it is not in use by any other vgpio devices anymore.
 */
void vgpio_free(unsigned gpio)
{
	struct global_gpio_list *tmp;
	struct list_head *pos, *q;
	
	// check the global list to see if this pin has been requested before
	list_for_each_safe(pos, q, &g_gpio_list.list){
		tmp = list_entry(pos, struct global_gpio_list, list);
		
		// if the gpio is in the list, decrease num_requested counter and
		// check if it is still in use by other vgpio devices 
		if(tmp->pin == gpio){
			// if so, don't free the gpio
			if(--tmp->num_requested){
				VGPIO_DEBUG("GPIO %d still in use\n", gpio);
				return;
			}
			// otherwise free it
			gpio_free(gpio);
			// and remove it from the list
			list_del(pos);
			kfree(tmp);
			VGPIO_DEBUG("GPIO %d freed\n", gpio);
			return;
		}
	}
	
}

/*
 * Free IRQ if it is not needed anymore.
 * Will remove port_irq from the notifyee list associated with gpio and
 * unbind the event channel
 * If the list is empty after that, will free the gpio irq and remove
 * gpio from g_gpio_irq_list
 */
void vgpio_irq_free(unsigned gpio, unsigned port_irq)
{
	struct global_gpio_irq_list *tmp;
	struct gpio_irq_notifyee_list *notifyee;
	struct list_head *pos, *npos, *q, *nq;
	int done = 0;
	int i = 0, j = 0;
	
	VGPIO_DEBUG("vgpio_irq_free(): pin %d, port_irq %d\n", gpio, port_irq);
	
	// find the correct motifyee list
	list_for_each_safe(pos, q, &g_gpio_irq_list.list){
		tmp = list_entry(pos, struct global_gpio_irq_list, list);
		VGPIO_DEBUG("g_gpio_irq_list element %d: pin %d, irq %d\n", i++, tmp->pin, tmp->irq);
		if(tmp->pin == gpio){
			// find the correct element on the notifyee list
			list_for_each_safe(npos, nq, &tmp->notifyees.list){
				notifyee = list_entry(npos, struct gpio_irq_notifyee_list, list);
				VGPIO_DEBUG("notifyees element %d: port_irq %d\n", j++, notifyee->port_irq);
				// unbind event channel, delete list element and free memory
				if(notifyee->port_irq == port_irq){
					VGPIO_DEBUG("vgpio_irq_free(): unbind port_irq %d\n", port_irq);
//					unbind_from_irqhandler(port_irq, &tmp->notifyees);
					list_del(npos);
					kfree(notifyee);
					done = 1;
					break;
				}
			}
			VGPIO_DEBUG("done: %d\n", done);
			if(done){
				// if notifyees is empty now, we can free the irq and 
				// delete this item from global irq list and free its memory
				if(list_empty(&tmp->notifyees.list)){
					i = gpio_to_irq(tmp->pin);
					VGPIO_DEBUG("vgpio_irq_free(): free_irq %d\n", i);
					if(i) free_irq(i, &tmp->notifyees);
					
					list_del(pos);
					kfree(tmp);
					break;	
				}
				else{
					VGPIO_DEBUG("not done!\n");
				}
			}
		}
	}
	VGPIO_DEBUG("done with outer loop\n");
}

void vgpio_handle_request(struct vgpioif *dev, vgpio_request_t *req)
{
	struct global_gpio_irq_list *element;
	struct list_head *pos;
	struct free_irq_helper_info *helper_info = NULL;
	vgpio_response_t rsp = {0};
	
	// Check if dev has permission to use the gpio
	if(!dev_vgpio_check_permission(dev, req->pin, req->cmd)){ 
		rsp.ret = -EACCES; 
		VGPIO_DEBUG("Permission denied: gpio %d, cmd %d\n", req->pin, req->cmd);
		goto done;		
	}
	
	switch(req->cmd){
		case CMD_GPIO_REQUEST: 
			rsp.ret = dev_vgpio_request(dev, req->pin, "sysfs");
			break;
		case CMD_GPIO_FREE:
			dev_vgpio_free(dev, req->pin);
			break;
		case CMD_GPIO_DIRECTION_OUTPUT: 
			rsp.ret = gpio_direction_output(req->pin, req->val);
			break;
		case CMD_GPIO_DIRECTION_INPUT:
			rsp.ret = gpio_direction_input(req->pin);
			break;
		case CMD_GPIO_SET_DEBOUNCE:
			rsp.ret = gpio_set_debounce(req->pin, req->val);
			break;
		case CMD_GPIO_GET_VALUE:
			rsp.ret = gpio_get_value(req->pin);
			break;
		case CMD_GPIO_SET_VALUE:	
			gpio_set_value(req->pin, req->val);
			#ifdef VGPIO_BENCH
			if(req->val){
				c_set[rep] = get_tsc();
			}
			#endif
			#ifdef VGPIO_BENCH_EXT_REACT
			gpio_set_value(430, 1);
			#endif
			
			break;
		case CMD_GPIO_REQUEST_IRQ:
			rsp.ret = dev_vgpio_irq_request(dev, req->pin, req->irq_edge, req->val);
			break;
		case CMD_GPIO_FREE_IRQ:
			/* We can't free the IRQ here directly, because we are in IRQ context
			 * so instead we defer the execution to process context with schedule_work()
			 * 
			 * First we need to find the element of global_gpio_irq_list for this pin
			 * to get the work_struct we will use
			 */ 
			list_for_each(pos, &g_gpio_irq_list.list){
				element = list_entry(pos, struct global_gpio_irq_list, list);
				if(element->pin == req->pin){
					helper_info = &element->free_irq_work_info;
					break;
				}
			}
			
			/* Make a copy of the data to be passed to free_irq_helper */			
			if(!helper_info){
				rsp.ret = -EINVAL;
				goto done;
			}			
			
			helper_info->dev = dev;
			memcpy(&helper_info->req, req, sizeof(vgpio_request_t));
			
			/* Initialize the work_struct and schedule it */
			INIT_WORK(&helper_info->work,free_irq_helper);			
			
			schedule_work(&helper_info->work);
			
			/* We do only want to send the response after freeing the irq */
			return;
		default: rsp.ret = -EINVAL;
	}
	
done:	
	vgpio_send_response(dev, &rsp);
}

// On loading this kernel module, we register as a frontend driver
static int __init vgpio_init(void)
{
	printk(KERN_NOTICE "vgpio: init\n");
	if (!xen_domain()){	
		return -ENODEV;
	}
	/* Initialize global lists*/
	INIT_LIST_HEAD(&g_gpio_list.list);
	INIT_LIST_HEAD(&g_gpio_irq_list.list);
	
	return xenvgpio_xenbus_init();
}
module_init(vgpio_init);

// ...and on unload we unregister
static void __exit vgpio_exit(void)
{
	struct global_gpio_list *tmp;
	struct global_gpio_irq_list *itmp;
	struct gpio_irq_notifyee_list *n;
	struct list_head *pos, *npos, *q, *nq;
	
	int iirq = 0, inot = 0, ipin = 0;
	
	printk(KERN_ALERT "vgpio: exit\n");
	xenvgpio_xenbus_fini();
	
	// free all remaining pin irqs, irq event channels and list memory
	list_for_each_safe(pos, q, &g_gpio_irq_list.list){
		VGPIO_DEBUG("checking element %d of irq list\n", iirq++);
		itmp = list_entry(pos, struct global_gpio_irq_list, list);
		
		inot = 0;
		list_for_each_safe(npos, nq, &itmp->notifyees.list){
			VGPIO_DEBUG("checking element %d of notifyee list\n", inot++);
			n = list_entry(npos, struct gpio_irq_notifyee_list, list);
			VGPIO_DEBUG("freeing port_irq %d\n", n->port_irq);
			//~ unbind_from_irqhandler(n->port_irq, &itmp->notifyees);
			list_del(npos);
			kfree(n);
		}
		VGPIO_DEBUG("freeing irq %d (pin %d)\n", itmp->irq, itmp->pin);
		if(itmp->irq) free_irq(itmp->irq, NULL);
		list_del(pos);
		kfree(itmp);
	}
	
	// free all gpios and global gpio list memory
	list_for_each_safe(pos, q, &g_gpio_list.list){
		VGPIO_DEBUG("checking element %d of pin list\n", ipin++);
		tmp = list_entry(pos, struct global_gpio_list, list);
		VGPIO_DEBUG("freeing gpio %d\n", tmp->pin);	
		if(tmp->pin) gpio_free(tmp->pin);
		list_del(pos);
		kfree(tmp);
	}
	
#ifdef VGPIO_BENCH
	logTimes();
#endif
	
}
module_exit(vgpio_exit);

MODULE_LICENSE("GPL");
MODULE_ALIAS("xen-backend:vgpio");
