#include <linux/string.h>
#include <stdio.h>
#include <malloc.h>

#define VGPIO_PRINT_DEBUG
#ifdef VGPIO_PRINT_DEBUG
#define VGPIO_DEBUG(fmt,...) printf("vgpio:Debug("__FILE__":%d) " fmt, __LINE__, ##__VA_ARGS__)
#define VGPIO_DEBUG_MORE(fmt,...) printf(fmt, ##__VA_ARGS__)
#else
#define VGPIO_DEBUG(fmt,...)
#endif

#define VGPIO_LOG(fmt,...) printf(KERN_NOTICE "vgpio:Info " fmt, ##__VA_ARGS__)

void parse_gpio_string(char* str, unsigned **pins, unsigned *num_pins)
{
	char *p = 0, *_str = 0;
	int err;
	unsigned num = 0;
	if(!str || !num_pins){
		VGPIO_DEBUG("error str or num_pins NULL: *str: %p, *num_pins: %p\n", str, num_pins);
		return;
	}
	*num_pins = 0;
	_str = strdup(str);
	
	// First pass: Count how many pins there are
	VGPIO_DEBUG_MORE("p: %p\n", p);
	while ( (p = strsep(&_str, ","))  != NULL){
		VGPIO_DEBUG_MORE("p: %p\n", p);
		while (*p == ' ') p++;
		num++;
		VGPIO_DEBUG_MORE("num: %d\n", num);
	}
	
	
	VGPIO_DEBUG("parse_gpio_string: found %d pins\n", num);
	// Allocate memory
	*pins = malloc(num * sizeof(unsigned));
	
	if(!*pins){
		VGPIO_DEBUG("parse_gpio_string: error allocating memory\n"); 
		return;
	}
	
	num = 0;
	_str = strdup(str);
	
	// Second pass: parse pins
	while ( (p = strsep(&_str, ","))  != NULL){
		while (*p == ' ') p++;
		err = sscanf(p, "%u", &(*pins)[num]);
		if(err != 1){
			VGPIO_DEBUG("parse_gpio_string: error in pass 2 (%d), num %d\n",err, num);
		}
		VGPIO_DEBUG("parse_gpio_string: found pin %d\n", (*pins)[num]);
		num++;
	}
	
	*num_pins = num;
}


int main( int argc, const char* argv[] )
{
	char *out = strdup("403,404,405");
	//~ char *in = "403,404,405";
	
	unsigned *result = 0;
	unsigned num = 0;
	int i;
	
	
	parse_gpio_string(out, &result, &num); 
	
	printf("num: %d\n", num);
	for(i = 0; i < num; i++){
		printf("%d ", result[i]);
	}
	printf("\n");
	
	free(result);
	
	return 0;
}
