XL 
Config option
GPIO pins can be added to a domain configuration file by using the keywork gpio.
Format:
gpio = [‘<mode>=<pin1>,<pin2>,<pinN>;<mode>=<...>;...’]
Where mode can be output, input or irq,
And <pinX> is the linux pin number.
Example:
gpio = ['output=403,430;input=403,404,464;irq=431,432,433,404,464']

The pins will be written to the xenstore paths for the backend and frontend, e.g.
.../output-pins = “403,430”
Backend
Description
The backend manages the virtual vgpio devices. It communicates with the frontend via a shared ring + event channel for commands and an additional event channel per irq pin that the frontends register.
The device is a struct vgpioif, which is created in _probe / on device activation.
It holds the event channel + irq number + shared ring for the communication event channel, pin_permissions and a list of the devices’ gpios.
The event channel, ring and permissions (out, in, irq) are read from xenstore when the frontend connects. 
Permissions are read from xenstore as a string from <input|output|irq>-pins (e.g. /local/domain/0/backend/vgpio/9/0/output-pins) . The string is a comma separated list of integers, e.g. “400,403,405”. These ints are stored as an unsigned int array in the device together with the total number of in-/out-/irq-pins the device has access to (--> gives the size of the array). The device can also have full access to all gpio pins or full access to all pins as in, out, irq. Full access is only supported by the miniOS frontend, since the linux frontend needs to know the exact pin numbers to create the necessary gpio chips.
The device also keeps a list of a gpios it has currently requested (struct dev_gpio_list gpio_list). This list is used to check if a pin in available (was requested before) before performing any action (set_direction_*, set_value, …). It also stores the irq number associated with the event channel used for delivering interrupts to the frontend.

Additionally there are two global gpio lists:
struct global_gpio_list g_gpio_list
struct global_gpio_list g_gpio_irq_list

g_gpio_list ist the global list of all gpios that are currently in use (-> have been requested) by all frontends/VMs. It stores a counter (num_requested) for each pin. Requesting the pin increases the counter, freeing the pin decreases it. A pin is only really freed once the counter reaches 0, i.e. when the last frontend frees it. Prior to that, the pin is freed from the frontend’s point of view (it can’t use the pin anymore), but not from the backends point of view. This list is also used to free any remaining gpios when the backend shuts down.

g_gpio_irq_list is the list of all GPIOs for which interrupts have been requested. It stores the irq number for each pin as well as a list of all event channels / ports that have to be notified when the interrupt occurs.
Each item also has a struct free_irq_helper_info, which is needed for freeing the irq. 
When a frontend sends a request to free a gpio, this request is delivered via an event channel and the handler for that event channel is therefore executed in irq context. Irqs can not be freed from irq context, so this work has to be scheduled for later execution from process context. This is done via schedule_work(). Free_irq_helper_info contains the struct work_struct for that as well as the data passed to the function that will free the gpio (void free_irq_helper()).




Requests are defined as
struct vgpio_request {
	vgpio_command_t cmd;
	unsigned pin;
	unsigned val; 
	unsigned irq_edge;
};
They contain the command that is to be executed (free, ..set_direction, …), the pin to be used, a value, which is dependent on the command (e.g. for set_value it is the value to set the pin to) and an additional variable irq_edge, which is used for requesting interrupts only and specifies the trigger for the interrupt (rising, falling, …).

When a request in received via comm_evtchn, this request is handled in vgpio_handle_request(), which first checks if the device making the request has permission to use the specified pin for the requested command and then the request is fulfilled. For the most part this is a 1:1 mapping of the commands to the functions from the linux/gpio.h interface. Then a response is send via the same event channel. The response only contains the return value  of the function called (if there is one).

When a GPIO is requested, the devices’ gpio list is checked first, to see if the device has already requested the gpio. If it has, the pin is not requested again. If not, the global list is checked to see if another frontend has requested the pin before. If so, only the num_requested counter is increased. If not, the pin is requested via gpio_request() and added to both lists.
Freeing gpios works analogously. 
Requesting irq pins also works analogously, only the global irq pin list is used and if an interrupt has been requested for the gpio before, the devices’ port number for delivering interrupts is added to the notifyee list. 
When an interrupt is requested for a pin for the first time, vgpio_pin_irq_handler() is registered as the handler for that interrupt and a pointer to the notifyee list for that pin is passed as data to the interrupt handler.
That way, when the interrupt occurs, the notifyee list is traversed and each frontend is notified.

DoTo

Check if pin is in use (by other frontend) before reconfiguring with set_direction_
Store the trigger for irq pins, and return error if a domain requests an irq pin with a different trigger instead of just adding the dom/port to the notifyee list 



Frontend (Mini-OS)
Description
The frontend provides an interface similar to linux/gpio.h:

int gpio_request(struct vgpiofront_dev *dev, unsigned gpio, const char *label);
void gpio_free(struct vgpiofront_dev *dev, unsigned gpio);
int gpio_direction_input(struct vgpiofront_dev *dev, unsigned gpio);
int gpio_direction_output(struct vgpiofront_dev *dev, unsigned gpio, int value);
int gpio_set_debounce(struct vgpiofront_dev *dev, unsigned gpio, unsigned debounce);
int gpio_get_value(struct vgpiofront_dev *dev, unsigned gpio);
void gpio_set_value(struct vgpiofront_dev *dev, unsigned gpio, int value);

The difference is, that these functions take an additional struct vgpiofront_dev*, which specifies the device pointer which you have to get via init_vgpiofront() first.

In MiniOS, interrupts for gpios are also requested via this frontend interface:

int gpio_request_irq(struct vgpiofront_dev *dev, unsigned gpio, void (*handler), unsigned trigger);
void gpio_free_irq(struct vgpiofront_dev *dev, unsigned gpio);

The frontend device (struct vgpiofront_dev) keeps information about the event channel, shared ring and grant table ref for communication with the backend. It also has a semaphore to wait for responses from the backend, a last_response variable to store the last received response and a list of all requested irqs and their handlers.

When an api function is called. E.g. gpio_request(), the driver creates a vgpio_request_t and then sends it via vgpiofront_send_request(). This inserts the request into the shared ring, notifies the remote and then downs the semaphore. When a response is received (→ vgpiofront_handler()), it is read from the shared ring, stored in last_response and then the semaphore is increased again.
This means that the communication is synchronous and only one request can be handled at a time per frontend. 

If an interrupt is requested for a pin, then an event channel is allocated, a request is send to the backend which contains the event channel number as the .val parameter so that the backend can bind this channel, and a new entry is added to the irq_list, which includes the handler function pointer.
All gpio irq event channels have vgpiofront_hw_irq_handler() as their handler, which gets the user defined handler function as it’s void *data argument. vgpiofront_hw_irq_handler() then just calls this handler.
This was done to create a simpler interface where the handler function definition in the app looks like this
static void irq_handler(void);

Instead of like this

void vgpiofront_hw_irq_handler(evtchn_port_t port, struct pt_regs *regs, void *data)

ToDo
Evaluate overhead for simpler irq handler interface
Add optional api function without overhead (user supplied handler is handler for xen event channel)

Frontend (Linux)
Description
The linux frontend is at it’s core the same as the Mini-OS Frontend. However, there are some differences:
The frontend also serves as a linux GPIO driver, which means to the system and users there is no difference in using the virtual gpios as opposed to “real” gpios.
Kernel modules can use the functions from linux/gpio.h to request and use gpios.
For this, the frontend creates a number of struct gpio_chip and registers them with the linux gpio subsystem. Each gpio_chip has a base (first gpio it provides) and ngpio (number of gpios it provides). So gpio chips can only represent a continuous range of gpios [base , base+ngpio). Since a domU can have access to a non-continuous range of gpios, on device creation the continuous ranges of gpios are identified by creating a sorted list of gpios (read from xenstore) and finding continuous ranges in that list. For each range a gpio_chip is created. This could done differently: Usually a pin controller (pinctrl) is used to achieve this, but the current implementation with multiple gpio chips seemed easier / faster to achieve without any real drawbacks.

To provide interrupt capability for gpios, an irq_chip is also added to gpio chips, so one can also use gpio_to_irq() and request_irq() to request interrupts for gpios. However, currently you have to pass a pointer to an unsigned int containing the gpio number as *dev_id to request_irq(). Also, since all gpio functions (get_value, set_value, …) communicate with the backend via an event channel and use a Semaphore to wait for the response, you can’t use them inside an interrupt handler.

ToDo
Change requesting of irqs so that request_irq() does not require dev_id to point to gpio number
Use pincontroller instead of multiple gpio chips
(Make gpio api functions usable in interrupt handlers)

