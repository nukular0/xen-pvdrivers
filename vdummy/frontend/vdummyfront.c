#include "vdummyfront.h"
#include <linux/string.h>
#include <linux/irq.h>
#include <linux/list_sort.h>


#define VDUMMY_PRINT_DEBUG
#ifdef VDUMMY_PRINT_DEBUG
#define VDUMMY_DEBUG(fmt,...) printk("vdummy:Debug("__FILE__":%d) " fmt, __LINE__, ##__VA_ARGS__)
#define VDUMMY_DEBUG_MORE(fmt,...) printk(fmt, ##__VA_ARGS__)
#else
#define VDUMMY_DEBUG(fmt,...)
#endif
#define VDUMMY_LOG(fmt,...) printk(KERN_NOTICE "vdummy:Info " fmt, ##__VA_ARGS__)
#define VDUMMY_ERR(fmt,...) printk(KERN_ERR "vdummy:Error " fmt, ##__VA_ARGS__)

void vdummyfront_set_state(struct xenbus_device *xbdev, 
									enum xenbus_state state);


int vdummyfront_send_request(struct vdummyfront_dev* dev, vdummy_request_t req){
	RING_IDX i;
	vdummy_request_t *_req;
	int notify;
	int _ret;
	
	//~ VDUMMYFRONT_LOG("sending...");
	if(dev->state == XenbusStateConnected){
		i = dev->ring.req_prod_pvt;
		_req = RING_GET_REQUEST(&dev->ring, i);
		memcpy(_req, &req, sizeof(req));
		dev->ring.req_prod_pvt = i + 1;

		wmb();
		RING_PUSH_REQUESTS_AND_CHECK_NOTIFY(&dev->ring, notify);
		if(notify) 
		{
			VDUMMY_DEBUG("sending request: cmd %d pin %d val %d edge %d\n", req.cmd, req.pin, req.val, req.irq_edge);
			notify_remote_via_irq(dev->comm_evtchn_irq);    
		}
		down(&dev->sem);
	}
	else{
		VDUMMY_DEBUG("error: not connected");
		return -1;
	}
	_ret = dev->last_response.ret;
	VDUMMY_DEBUG("got response: %d\n", _ret);
	dev->last_response.ret = INVALID_RESPONSE;
	return _ret;
}


static irqreturn_t vdummyfront_comm_interrupt(int irq, void *dev_id)
{
	RING_IDX rp, cons;
	vdummy_response_t *rsp;
	int nr_consumed, more;
	struct vdummyfront_dev *dev = (struct vdummyfront_dev*) dev_id;

moretodo:
	rp = dev->ring.sring->rsp_prod;
    rmb(); /* Ensure we see queued responses up to 'rp'. */
    cons = dev->ring.rsp_cons;
   
	while ((cons != rp))
    {
		rsp = RING_GET_RESPONSE(&dev->ring, cons);
		nr_consumed++;
		dev->last_response = *rsp;
		dev->ring.rsp_cons = ++cons;   
        if (dev->ring.rsp_cons != cons)
            /* We reentered, we must not continue here */
            break;
			
	}
	
	RING_FINAL_CHECK_FOR_RESPONSES(&dev->ring, more);
    if (more) goto moretodo;    
	
	up(&dev->sem);   
	
	return IRQ_HANDLED;
}

static int publish_xenbus(struct vdummyfront_dev* dev) {
   struct xenbus_transaction xbt;
   int err;
   /* Write the grant reference and event channel to xenstore */
again:
   if((err = xenbus_transaction_start(&xbt))) {
      VDUMMY_ERR("Unable to start xenbus transaction, error was %d\n", err);
      
      return -1;
   }

   if((err = xenbus_printf(xbt, dev->nodename, "ring-ref", "%u", (unsigned int) dev->ring_ref))) {
      VDUMMY_ERR("Unable to write %s/ring-ref, error was %d\n", dev->nodename, err);
      goto abort_transaction;
   }

   if((err = xenbus_printf(xbt, dev->nodename, "event-channel", "%u", (unsigned int) dev->comm_evtchn))) {
      VDUMMY_ERR("Unable to write %s/event-channel, error was %d\n", dev->nodename, err);
      goto abort_transaction;
   }

   if((err = xenbus_transaction_end(xbt, 0))) {
      VDUMMY_ERR("Unable to complete xenbus transaction, error was %d\n", err);
      if(err == -EAGAIN){
		goto again;
	  }
      return -1;
   }
  
   return 0;
abort_transaction:
   xenbus_transaction_end(xbt, 1);
   return -1;
}


static int vdummyfront_connect(struct xenbus_device *xbdev)
{
	int err;
	struct vdummy_sring *sring;
	struct vdummyfront_dev* dev;
   
	dev = dev_get_drvdata(&xbdev->dev);
   
	/* Create shared page/ring */
	dev->ring_page = get_zeroed_page(GFP_KERNEL);
	sring = (struct vdummy_sring *)dev->ring_page;
	if(sring == NULL) {
	  VDUMMY_ERR("Unable to allocate page for shared memory\n");
	  goto error;	
	}

	/* Initialize shared ring in shared page */
	SHARED_RING_INIT(sring);
	FRONT_RING_INIT(&dev->ring, sring, XEN_PAGE_SIZE);
	
	err = xenbus_grant_ring(xbdev, sring, 1, &dev->ring_ref);
	if(err < 0){
		VDUMMY_ERR("error granting access to ring: %d\n", err);
		goto error_postalloc;
	}
	
	VDUMMY_DEBUG("grant ref is %lu\n", (unsigned long) dev->ring_ref);

	/* Create event channel for communication with backend */
	err = xenbus_alloc_evtchn(xbdev, &dev->comm_evtchn);
	if(err < 0) {
	  VDUMMY_ERR("Unable to allocate comm_event channel\n");
	  goto error_postmap;
	}
	VDUMMY_DEBUG("comm_event channel is %lu\n", (unsigned long) dev->comm_evtchn);
	
	err = bind_evtchn_to_irqhandler(dev->comm_evtchn,
					vdummyfront_comm_interrupt,
					0, "vdummyfront", dev);
	if (err < 0){
		VDUMMY_ERR("Could not bind evtchn to irq handler (error %d)\n", err);
		goto error_postbind;
	}
	dev->comm_evtchn_irq = err;
	
	/* Write the entries to xenstore */
	if(publish_xenbus(dev)) {
	  goto error_postevtchn;
	}
	

	return 0;
error_postevtchn:
      unbind_from_irqhandler(dev->comm_evtchn_irq, dev);
error_postbind:
	  xenbus_free_evtchn(xbdev, dev->comm_evtchn);
	  dev->comm_evtchn = 0;
error_postmap:
      gnttab_end_foreign_access_ref(dev->ring_ref, 0);
error_postalloc:
      free_page((unsigned long)sring);
error:
   return -1;
}

void free_vdummyfront(struct xenbus_device *xbdev){
	struct vdummyfront_dev *dev;
		
	dev = dev_get_drvdata(&xbdev->dev);
	if(!dev) 
		return;
	
	if(dev->nodename) 	
		kfree(dev->nodename);
	if(dev->bepath)		
		kfree(dev->bepath);
	
	// free communication event channel
	if(dev->comm_evtchn){
		VDUMMY_DEBUG("unbind_from_irqhandler (irq %d)\n", dev->comm_evtchn_irq);
		unbind_from_irqhandler(dev->comm_evtchn_irq, dev);
		dev->comm_evtchn = 0;
	}
	
	// free shared ring and memory
	if(dev->ring_ref){
		VDUMMY_DEBUG("gnttab_free_grant_reference (ring_ref %d)\n", dev->ring_ref);
		gnttab_free_grant_reference(dev->ring_ref);
		VDUMMY_DEBUG("gnttab_end_foreign_access (ring_ref %d)\n", dev->ring_ref);
		gnttab_end_foreign_access(dev->ring_ref, 0, dev->ring_page);
		dev->ring_ref = 0;
		dev->ring_page = 0;
	}
	
	// free device memory
	kfree(dev);
	
	vdummyfront_set_state(xbdev, XenbusStateClosed);	
}

void shutdown_vdummyfront(struct xenbus_device *xbdev){
	
	struct vdummyfront_dev *dev;
	int err;
	if(xbdev->state != XenbusStateConnected) 
		return;
		
	dev = dev_get_drvdata(&xbdev->dev);
	
	if(!dev) 
		return;
	
	vdummyfront_set_state(xbdev, XenbusStateClosing);	
	
	// clean up xenstore
	err = xenbus_rm(XBT_NIL, dev->nodename, "ring-ref");
	if(err){
		VDUMMY_ERR("removing ring-ref (%d)\n", err);
	}
	err = xenbus_rm(XBT_NIL, dev->nodename, "event-channel");
	if(err){
		VDUMMY_ERR("removing event-channel (%d)\n", err);
	}
		
	free_vdummyfront(xbdev);
	
}

struct vdummyfront_dev* init_vdummyfront(struct xenbus_device *xbdev, const char* _nodename)
{
	struct vdummyfront_dev* dev;
	unsigned int val;
	const char* nodename;
	int err;
	char tmpstr[512];

	VDUMMY_DEBUG("============= Init VDUMMY Front ================\n");

	dev = kzalloc(sizeof(*dev), GFP_KERNEL);
	
	if(!dev){
		VDUMMY_ERR("allocating memory for dev\n");
		return NULL;
	}

	dev->last_response.ret = INVALID_RESPONSE;
	dev->xbdev = xbdev;

	/* Init semaphore */
	sema_init(&dev->sem, 0);
	/* Set node name */
	nodename = _nodename ? _nodename : "device/vdummy/0";
	dev->nodename = kstrdup(nodename, GFP_KERNEL);
  

	/* Get backend domid */
	err = xenbus_scanf(XBT_NIL, dev->nodename, "backend-id", "%u", &val);
	if(err != 1) {
	  VDUMMY_ERR("Unable to read backend-id during vdummyfront initialization! error = %d\n", err);
	  goto error;
	}
	dev->bedomid = val;
	VDUMMY_LOG("backend dom-id is %d\n", val);

	/* Get backend xenstore path */
	err = xenbus_scanf(XBT_NIL, dev->nodename, "backend", "%s", tmpstr);
	if(err != 1) {
	  VDUMMY_ERR("Unable to read backend during vdummyfront initialization! error = %d\n", err);
	  goto error;
	}
	dev->bepath = kstrdup((const char*)tmpstr, GFP_KERNEL);
	VDUMMY_LOG("backend path is %s\n", dev->bepath);

	
	return dev;

error:
   shutdown_vdummyfront(xbdev);
   return NULL;
}

void vdummyfront_set_state(struct xenbus_device *xbdev, 
									enum xenbus_state state)
{
	struct vdummyfront_dev* dev;
	if(!xbdev) 
		return;
	
	dev = dev_get_drvdata(&xbdev->dev);
	if(!dev) 
		return;
	
	dev->state = state;
	xenbus_switch_state(xbdev, state);						
}

// The function is called on activation of the device
static int vdummyfront_probe(struct xenbus_device *xbdev,
              const struct xenbus_device_id *id)
{
	struct vdummyfront_dev* dev;
	VDUMMY_DEBUG("vdummyfront_probe called: %s\n", xbdev->nodename);
	
	// Create vdummy device
	dev = init_vdummyfront(xbdev, xbdev->nodename);
	if(!dev) 
		goto error;
	
	VDUMMY_DEBUG("xbdev: %p dev %p\n", xbdev, dev);
	
	dev_set_drvdata(&xbdev->dev, dev);
	
	vdummyfront_set_state(xbdev, XenbusStateInitialising);
		
	return 0;
	
error:
	dev_set_drvdata(&xbdev->dev, NULL);
	return -1;
}


// The function is called on a state change of the backend driver
static void vdummyback_changed(struct xenbus_device *xbdev,
			    enum xenbus_state backend_state)
{
	struct vdummyfront_dev* dev;
	dev = dev_get_drvdata(&xbdev->dev);
	
	VDUMMY_DEBUG("backend changed: %s -> %s\n", xbdev->otherend, xenbus_strstate(backend_state));
	
	switch (backend_state)
	{
		case XenbusStateInitialising:
			vdummyfront_set_state(xbdev, XenbusStateInitialising);
			break;
		case XenbusStateInitialised:
		case XenbusStateReconfiguring:
		case XenbusStateReconfigured:
		case XenbusStateUnknown:
			break;

		case XenbusStateInitWait:
			if (xbdev->state != XenbusStateInitialising)
				break;
			if (vdummyfront_connect(xbdev) != 0)
				break;

			vdummyfront_set_state(xbdev, XenbusStateConnected);

			break;

		case XenbusStateConnected:
			VDUMMY_DEBUG("Other side says it is connected as well.\n");
			break;

		case XenbusStateClosed:
			if (xbdev->state == XenbusStateClosed)
				break;
			/* Missed the backend's CLOSING state -- fallthrough */
		case XenbusStateClosing:
			if (xbdev->state != XenbusStateConnected)
				break;
			shutdown_vdummyfront(xbdev);
			xenbus_frontend_closed(xbdev);
			vdummyfront_set_state(xbdev, XenbusStateInitialising);
	}
}

static int vdummyfront_remove(struct xenbus_device *xbdev)
{
	VDUMMY_DEBUG("vdummyfront_remove: %s\n", xbdev->nodename);
	shutdown_vdummyfront(xbdev);
	return 0;
}

static const struct xenbus_device_id vdummyfront_ids[] = {
	{ "vdummy" },
	{ "" }
};

static struct xenbus_driver vdummyfront_driver = {
	.ids = vdummyfront_ids,
	.probe = vdummyfront_probe,
	.remove = vdummyfront_remove,
	.otherend_changed = vdummyback_changed,
};
static int __init vdummyfront_init(void)
{
	int ret;
	printk(KERN_NOTICE "vdummy: init\n");
	if (!xen_domain()){	
		printk(KERN_NOTICE "vdummy: no xen domain\n");
		return -ENODEV;
	}

	ret = xenbus_register_frontend(&vdummyfront_driver);

	return ret;
}
module_init(vdummyfront_init);

static void __exit vdummyfront_exit(void)
{
	VDUMMY_DEBUG("vdummy: exit\n");

//	vdummyfront_remove(_xbdev);
	
	xenbus_unregister_driver(&vdummyfront_driver);	
}
module_exit(vdummyfront_exit);

MODULE_DESCRIPTION("Xen virtual dummy device frontend");
MODULE_LICENSE("GPL");
MODULE_ALIAS("xen-frontend:vdummy");
