#ifndef _XEN_VDUMMYFRONT_H
#define _XEN_VDUMMYFRONT_H

#include <linux/module.h>  /* Needed by all modules */
#include <linux/kernel.h>  /* Needed for KERN_ALERT */

#include <xen/xen.h>       /* We are doing something with Xen */
#include <xen/xenbus.h>
#include <xen/events.h>
#include <xen/interface/event_channel.h>
#include <xen/interface/grant_table.h>
#include <xen/interface/io/ring.h>
#include <xen/page.h>
#include <linux/errno.h>
#include <linux/list.h>
#include <linux/semaphore.h>
#include <xen/grant_table.h>

#define INVALID_RESPONSE			-9999

typedef enum { CMD_DUMMY_REQUEST } vdummy_command_t;
	
/*
 * Request type for the shared ring.
 * cmd is the command that is requested, e.g. CMD_DUMMY_REQUEST
 */
struct vdummy_request {
	vdummy_command_t cmd;
	unsigned pin;
	unsigned val; 
	unsigned irq_edge;
};
typedef struct vdummy_request vdummy_request_t;

/*
 * Response type for the shared ring.
 */
struct vdummy_response {
	int ret;
};
typedef struct vdummy_response vdummy_response_t;

DEFINE_RING_TYPES(vdummy, struct vdummy_request, struct vdummy_response);



struct vdummyfront_dev {
	grant_ref_t ring_ref;
	evtchn_port_t comm_evtchn;
	unsigned int comm_evtchn_irq;		
	
	struct vdummy_front_ring ring;
	unsigned long ring_page;

	domid_t bedomid;
	char* nodename;
	char* bepath;

	enum xenbus_state state;
	struct xenbus_device *xbdev;
	struct semaphore sem; // Semaphore used for waiting for responses from backend

	vdummy_response_t last_response;
};


/*Initialize frontend */
struct vdummyfront_dev* init_vdummyfront(struct xenbus_device *xbdev, const char* nodename);
/*Shutdown frontend */
void shutdown_vdummyfront(struct xenbus_device *xbdev);
void free_vdummyfront(struct xenbus_device *xbdev);

static irqreturn_t vdummyfront_comm_interrupt(int irq, void *dev_id);

int vdummyfront_send_request(struct vdummyfront_dev* dev, vdummy_request_t req);


#endif /* _XEN_VDUMMYFRONT_H */
