#include "common.h"

irqreturn_t vdummyback_comm_interrupt_handler(int irq, void *dev_id)
{
	struct vdummydev *dev;
	RING_IDX rq, cons;
	vdummy_request_t *req;
	int nr_consumed, more;
	struct backend_info *be = (struct backend_info*)dev_id;
	unsigned long flags;
	
	
	if(!be){
		VDUMMY_DEBUG("interrupt! irq: %d\n",irq); 
		VDUMMY_LOG("vdummyback_interrupt_handler: be is NULL\n");
		return IRQ_HANDLED;
	}
	
	dev = be->vdummy;
	

	VDUMMY_DEBUG("interrupt! irq: %d\n",irq); 
	
	nr_consumed = 0;
	local_irq_save(flags);
	
moretodo:
	rq = dev->ring.sring->req_prod;
    rmb(); /* Ensure we see queued responses up to 'rp'. */
    cons = dev->ring.req_cons;
   
	while ((cons != rq))
	{
		req = RING_GET_REQUEST(&dev->ring, cons);
		nr_consumed++;
		vdummy_handle_request(dev, req);
		dev->ring.req_cons = ++cons;
		if (dev->ring.req_cons != cons)
            /* We reentered, we must not continue here */
            break;
	}
	
	RING_FINAL_CHECK_FOR_REQUESTS(&dev->ring, more);
    if (more) goto moretodo;
    if(!nr_consumed) goto done;
	
done:
	local_irq_restore(flags);
	return IRQ_HANDLED;
}
void vdummy_send_response(struct vdummydev *dev, vdummy_response_t *rsp){
	RING_IDX i;
	vdummy_response_t *_rsp;
	int notify;
	
	i = dev->ring.rsp_prod_pvt;
	_rsp = RING_GET_RESPONSE(&dev->ring, i);
	_rsp->ret = rsp->ret;
	dev->ring.rsp_prod_pvt = i + 1;

	wmb();
	RING_PUSH_RESPONSES_AND_CHECK_NOTIFY(&dev->ring, notify);
	if(notify) 
	{
		notify_remote_via_irq(dev->comm_irq);
	}
	else{
		//~ VDUMMY_DEBUG("not notified\n");
	}
}


struct vdummydev *vdummydev_alloc(domid_t fe_domid)
{
	struct vdummydev *dev;
	
	dev = kzalloc(sizeof(struct vdummydev), GFP_KERNEL);
	if(!dev)
		return ERR_PTR(-ENOMEM);
	
	dev->fe_domid = fe_domid;
	
	// Allocate memory for any other things we need
	
	return dev;
}

void vdummydev_free(struct vdummydev *dev)
{
	// free any memory that has to be freed
	
	// free device memory
	kfree(dev);
}
