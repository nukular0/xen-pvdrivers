#include "common.h"

void vdummy_handle_request(struct vdummydev *dev, vdummy_request_t *req)
{
	vdummy_response_t rsp;
	VDUMMY_DEBUG("vdummy_handle_request: %d\n", req->cmd);
	rsp.ret = 1;
	
done:	
	vdummy_send_response(dev, &rsp);
}

// On loading this kernel module, we register as a frontend driver
static int __init vdummy_init(void)
{
	printk(KERN_NOTICE "vdummy: init\n");
	if (!xen_domain()){	
		return -ENODEV;
	}
	
	return xenvdummy_xenbus_init();
}
module_init(vdummy_init);

// ...and on unload we unregister
static void __exit vdummy_exit(void)
{
	
	printk(KERN_ALERT "vdummy: exit\n");
	xenvdummy_xenbus_fini();	
}
module_exit(vdummy_exit);

MODULE_LICENSE("GPL");
MODULE_ALIAS("xen-backend:vdummy");
