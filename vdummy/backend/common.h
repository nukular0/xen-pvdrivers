/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation; or, when distributed
 * separately from the Linux kernel or incorporated into other
 * software packages, subject to the following license:
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this source file (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy, modify,
 * merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
#ifndef __XEN_VDUMMYBACK__COMMON_H__
#define __XEN_VDUMMYBACK__COMMON_H__

#include <linux/module.h>  /* Needed by all modules */
#include <linux/kernel.h>  /* Needed for KERN_ALERT */

#include <xen/xen.h>       /* We are doing something with Xen */
#include <xen/xenbus.h>
#include <xen/events.h>
#include <xen/interface/event_channel.h>
#include <xen/interface/grant_table.h>
#include <xen/interface/io/ring.h>
#include <xen/page.h>
#include <linux/errno.h>
#include <linux/list.h>
#include <linux/workqueue.h>
#include <linux/init.h>

#define VDUMMY_PRINT_DEBUG
#ifdef VDUMMY_PRINT_DEBUG
#define VDUMMY_DEBUG(fmt,...) printk("vdummy:Debug("__FILE__":%d) " fmt, __LINE__, ##__VA_ARGS__)
#define VDUMMY_DEBUG_MORE(fmt,...) printk(fmt, ##__VA_ARGS__)
#else
#define VDUMMY_DEBUG(fmt,...)
#endif

#define VDUMMY_LOG(fmt,...) printk(KERN_NOTICE "vdummy:Info " fmt, ##__VA_ARGS__)


typedef enum { CMD_DUMMY_REQUEST } vdummy_command_t;

/*
 * Request type for the shared ring.
 * cmd is the command that is requested, e.g. CMD_DUMMY_REQUEST
 */
struct vdummy_request {
	vdummy_command_t cmd;
};
typedef struct vdummy_request vdummy_request_t;

/*
 * Response type for the shared ring.
 */
struct vdummy_response {
	int ret;
};
typedef struct vdummy_response vdummy_response_t;

DEFINE_RING_TYPES(vdummy, struct vdummy_request, struct vdummy_response);

struct backend_info {
	struct xenbus_device *dev;
	struct vdummydev *vdummy;

	/* This is the state that will be reflected in xenstore
	 */
	enum xenbus_state state;

	enum xenbus_state frontend_state;
};


struct vdummydev {
	domid_t fe_domid;
	unsigned int handle;

	/* event channel for communication / commands from front end */
	evtchn_port_t comm_evtchn; 	
	/* interrupt number for events on comm_evtchn */
	unsigned int comm_irq;		

	/* Shared ring */
	struct vdummy_back_ring ring; 
};


irqreturn_t vdummyback_comm_interrupt_handler(int irq, void *dev_id);

int xenvdummy_xenbus_init(void);
void xenvdummy_xenbus_fini(void);

struct vdummydev *vdummydev_alloc(domid_t fe_domid);
void vdummydev_free(struct vdummydev *vdummy);

void vdummy_handle_request(struct vdummydev *dev, vdummy_request_t *req);
void vdummy_send_response(struct vdummydev *dev, vdummy_response_t *rsp);


#endif /* __XEN_VDUMMYBACK__COMMON_H__ */
