#include "common.h"
#include <linux/string.h>


static void backend_connect(struct backend_info *be);
static void backend_disconnect(struct backend_info *be);
static void set_backend_state(struct backend_info *be,
			      enum xenbus_state state);
static int backend_create_vdummydev(struct backend_info *be);


static int backend_create_vdummydev(struct backend_info *be)
{
	int err;
	struct xenbus_device *dev = be->dev;
	struct vdummydev *vdummy;

	if (be->vdummy != NULL)
		return 0;

	vdummy = vdummydev_alloc(dev->otherend_id);
	if (IS_ERR(vdummy)) {
		err = PTR_ERR(vdummy);
		xenbus_dev_fatal(dev, err, "creating interface ");
		return err;
	}
	be->vdummy = vdummy;

	kobject_uevent(&dev->dev.kobj, KOBJ_ONLINE);
	return 0;
}	

// This is where we set up path watchers and event channels
static void backend_connect(struct backend_info *be)
{
	unsigned int val;
	grant_ref_t ring_ref;
	unsigned int evtchn;
	int err;
	void* ring_addr;
	struct xenbus_device *dev = be->dev;
	struct vdummy_sring *shared;
	struct vdummydev *vdummy = be->vdummy;
	char tmpstr[512];
	
	if(!dev || !vdummy){
		VDUMMY_LOG("backend_connect error");
		return;
	}
	
	VDUMMY_LOG("Connecting the backend now\n");
	
	VDUMMY_DEBUG("Fetching ring-ref\n");
	/* Fetch the grant reference (ring-ref from fe-path) */
	err = xenbus_scanf(XBT_NIL, dev->otherend,
			   "ring-ref", "%u", &val);
	if (err < 0){
		VDUMMY_LOG("No ring-ref in %s\n", dev->otherend);
		goto done; /* The frontend does not have a control ring */
	}
	ring_ref = val;
	VDUMMY_DEBUG("ring ref is %u\n", ring_ref);

	/* Map shared page */
	err = xenbus_map_ring_valloc(dev, &ring_ref,1, &ring_addr);
	if(err != 0){
		VDUMMY_LOG("Error while trying to map shared page: %d\n", err);
		goto fail;
	}
	shared = (struct vdummy_sring*)ring_addr;

	/* Initialize shared ring */
    BACK_RING_INIT(&vdummy->ring, shared, XEN_PAGE_SIZE);
	VDUMMY_LOG("Shared ring mapped successfully!\n");

	VDUMMY_DEBUG("fetching event-channel\n");
	/* Fetch event channel (event-channel from fe-path) */
	err = xenbus_scanf(XBT_NIL, dev->otherend,
			   "event-channel", "%u", &val);
	if (err < 0) {
		VDUMMY_LOG("No event-channel in %s\n", dev->otherend);
		xenbus_dev_fatal(dev, err,
				 "reading %s/event-channel-ctrl",
				 dev->otherend);
		goto fail;
	}

	evtchn = val;
	vdummy->comm_evtchn = evtchn;
	VDUMMY_DEBUG("evtchn is %u\n", vdummy->comm_evtchn);
	
	/* bind event channel */
	err = bind_interdomain_evtchn_to_irqhandler(
			// id of FE domU, evtchn, interrupt handler,          flags
			dev->otherend_id, evtchn, vdummyback_comm_interrupt_handler, 0,
			// device name,  device-id (void*), will be passed to handler
			dev->devicetype, be);
	VDUMMY_DEBUG("bind result: %d\n", err);
	
	if(err < 0){
		goto fail;
	}
	vdummy->comm_irq = err;
	
done:
	return;

fail:
	backend_disconnect(be);		
	return;
	
}

// This will destroy event channel handlers
static void backend_disconnect(struct backend_info *be)
{
	int ret;
	struct vdummydev *vdummy;
	pr_info("Disconnecting the backend now\n");
	VDUMMY_LOG("Unmapping shared page and event channel\n");
	
	vdummy = be->vdummy;
	if(!vdummy)
		return;
			
	if(vdummy->comm_irq){
		unbind_from_irqhandler(vdummy->comm_irq, be);
		vdummy->comm_irq = 0;
	}
	VDUMMY_DEBUG("Unbound from IRQ handler");
	
	if(vdummy->ring.sring){
		ret = xenbus_unmap_ring_vfree(be->dev, (void*)vdummy->ring.sring);
		VDUMMY_DEBUG("Unmap shared page result: %d\n", ret);		
	}
}

static inline void backend_switch_state(struct backend_info *be,
					enum xenbus_state state)
{
	struct xenbus_device *dev = be->dev;

	VDUMMY_DEBUG("%s -> %s\n", dev->nodename, xenbus_strstate(state));
	be->state = state;

	xenbus_switch_state(dev, state);
}

// We try to switch to the next state from a previous one
static void set_backend_state(struct backend_info *be,
			      enum xenbus_state state)
{
	while (be->state != state) {
		switch (be->state) {
		case XenbusStateInitialising:
			switch (state) {
			case XenbusStateInitWait:
			case XenbusStateConnected:
			case XenbusStateClosing:
				backend_switch_state(be, XenbusStateInitWait);
				break;
			case XenbusStateClosed:
				backend_switch_state(be, XenbusStateClosed);
				break;
			default:
				BUG();
			}
			break;
		case XenbusStateClosed:
			switch (state) {
			case XenbusStateInitWait:
			case XenbusStateConnected:
				backend_switch_state(be, XenbusStateInitWait);
				break;
			case XenbusStateClosing:
				backend_switch_state(be, XenbusStateClosing);
				break;
			default:
				BUG();
			}
			break;
		case XenbusStateInitWait:
			switch (state) {
			case XenbusStateConnected:
				backend_connect(be);
				backend_switch_state(be, XenbusStateConnected);
				break;
			case XenbusStateClosing:
			case XenbusStateClosed:
				backend_switch_state(be, XenbusStateClosing);
				break;
			default:
				BUG();
			}
			break;
		case XenbusStateConnected:
			switch (state) {
			case XenbusStateInitWait:
			case XenbusStateClosing:
			case XenbusStateClosed:
				backend_disconnect(be);
				backend_switch_state(be, XenbusStateClosing);
				break;
			default:
				BUG();
			}
			break;
		case XenbusStateClosing:
			switch (state) {
			case XenbusStateInitWait:
			case XenbusStateConnected:
			case XenbusStateClosed:
				backend_switch_state(be, XenbusStateClosed);
				break;
			default:
				BUG();
			}
			break;
		default:
			BUG();
		}
	}
}

static int vdummyback_remove(struct xenbus_device *dev)
{
	struct backend_info *be = dev_get_drvdata(&dev->dev);

	set_backend_state(be, XenbusStateClosed);

	if (be->vdummy) {
		kobject_uevent(&dev->dev.kobj, KOBJ_OFFLINE);
		vdummydev_free(be->vdummy);
		be->vdummy = NULL;
	}
	kfree(be);
	dev_set_drvdata(&dev->dev, NULL);
	return 0;
}

// The function is called on activation of the device
static int vdummyback_probe(struct xenbus_device *dev,
			const struct xenbus_device_id *id)
{
	int err;
	struct backend_info *be = kzalloc(sizeof(struct backend_info),
					  GFP_KERNEL);
	if (!be) {
		xenbus_dev_fatal(dev, -ENOMEM,
				 "allocating backend structure");
		return -ENOMEM;
	}

	be->dev = dev;
	dev_set_drvdata(&dev->dev, be);

	be->state = XenbusStateInitialising;
	err = xenbus_switch_state(dev, XenbusStateInitialising);
	if (err)
		goto fail;
	
	err = backend_create_vdummydev(be);
	if (err)
		goto fail;
	

	return 0;
fail:
	VDUMMY_DEBUG("failed\n");
	vdummyback_remove(dev);
	return err;
}



// The function is called on a state change of the frontend driver
static void vdummy_frontend_changed(struct xenbus_device *dev, enum xenbus_state frontend_state)
{
	
	struct backend_info *be = dev_get_drvdata(&dev->dev);

	VDUMMY_DEBUG("frontend changed: %s -> %s\n", dev->otherend, xenbus_strstate(frontend_state));

	be->frontend_state = frontend_state;
	
	switch (frontend_state) {
		case XenbusStateInitialising:
			set_backend_state(be, XenbusStateInitWait);
			break;

		case XenbusStateInitialised:
			break;

		case XenbusStateConnected:
			set_backend_state(be, XenbusStateConnected);
			break;

		case XenbusStateClosing:
			VDUMMY_LOG("Frontend is closing\n");
			set_backend_state(be, XenbusStateClosing);
			break;

		case XenbusStateClosed:
			set_backend_state(be, XenbusStateClosed);
			if (xenbus_dev_is_online(dev))
				break;
			/* fall through if not online */
		case XenbusStateUnknown:
			set_backend_state(be, XenbusStateClosed);
			device_unregister(&dev->dev);
			break;

		default:
			xenbus_dev_fatal(dev, -EINVAL, "saw state %s (%d) at frontend",
					xenbus_strstate(frontend_state), frontend_state);
			break;
	}
}


// This defines the name of the devices the driver reacts to
static const struct xenbus_device_id vdummyback_ids[] = {
	{ "vdummy" },
	{ "" }
};

// We set up the callback functions
static struct xenbus_driver vdummy_driver = {
	.ids  = vdummyback_ids,
	.probe = vdummyback_probe,
	.remove = vdummyback_remove,
	.otherend_changed = vdummy_frontend_changed,
};

int xenvdummy_xenbus_init(void)
{
	return xenbus_register_backend(&vdummy_driver);
}

void xenvdummy_xenbus_fini(void)
{
	return xenbus_unregister_driver(&vdummy_driver);
}
